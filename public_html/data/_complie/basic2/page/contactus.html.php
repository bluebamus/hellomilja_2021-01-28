<?php /* Template_ 2.2.8 2016/08/08 03:11:42 /home/brosiidea.com/www/eyoom/theme/basic2/page/contactus.html 000001439 */ ?>
<?php if (!defined("_GNUBOARD_")) exit; ?>
<div>
<div class="map-box margin-top-35">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12654.800189237718!2d126.95667203925568!3d37.53856744162723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357ca211f9b4c207%3A0x7fa72c903dfd9f22!2z7ISc7Jq47Yq567OE7IucIOyaqeyCsOq1rCDrsLHrspTroZwgMzI5!5e0!3m2!1sko!2skr!4v1470591948323" width=100% height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<div class="row">
<div class="col-sm-6">
<div class="headline margin-top-30 md-margin-bottom-30"><h4>Contacts</h4></div>
<ul class="list-unstyled contact-box-l margin-bottom-30">
<li><i class="fa fa-building"></i> 서울시 용산구 백범로 329 본관 RC1</li>
<li><i class="fa fa-envelope"></i> master@brosiidea.com</li>
<li><i class="fa fa-phone"></i> 010-3278-1324</li>
<li><i class="fa fa-fax"></i> 0504-167-1125</li>
</ul>
</div>
<div class="col-sm-6">
<div class="headline margin-top-30"><h4>Business Hours</h4></div>
<ul class="list-unstyled contact-box-r margin-bottom-30">
<li><strong>Monday ~ Friday :</strong> 9am to 6pm</li>
<!--<li><strong>Saturday :</strong> 10am to 3pm</li>-->
<li><strong>Mealtime :</strong> 12pm to 1pm</li>
<li><strong>Saturday, Sunday & Holidays:</strong> Closed</li>
</ul>
</div>
</div>
</div>