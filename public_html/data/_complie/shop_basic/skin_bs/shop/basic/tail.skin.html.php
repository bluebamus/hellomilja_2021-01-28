<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/tail.skin.html 000007798 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; // ?>
</div>
<?php if((defined('_INDEX_')&&$TPL_VAR["eyoom"]["use_main_side_layout"]=='y')||(!defined('_INDEX_')&&$TPL_VAR["eyoom"]["use_sub_side_layout"]=='y'&&$TPL_VAR["subinfo"]["sidemenu"]!='n')){?>
<?php if($TPL_VAR["eyoom"]["pos_side_layout"]=='right'){?>
<?php $this->print_("shop_side",$TPL_SCP,1);?>
<?php }?>
<?php }?>
<div class="clearfix"></div>
</div>	    </div>
<?php if($TPL_VAR["color"]=='white'){?>
<div class="footer footer-light">
<?php }elseif($TPL_VAR["color"]=='black'){?>
<div class="footer footer-dark">
<?php }?>
<div class="container">
<div class="row">
<div class="col-md-3 " style="text-align:center; ">
<div class="heading-footer"><h4>Certification Mark</h4></div>
<p class="margin-bottom-15">
<form name="KB_AUTHMARK_FORM" method="get">
<input type="hidden" name="page" value="B009111"/>
<input type="hidden" name="cc" value="b010807:b008491"/>
<input type="hidden" name="mHValue" value='4363e69e4b8bdb76b78a0c9d3ca18426201804041518905'/>
</form>
<div style="display:inline-block;">
<a href="#" onclick="javascript:onPopKBAuthMark();return false;">
<img src="http://img1.kbstar.com/img/escrow/escrowcmark.gif" border="0"/>
</a>
<img src="/eyoom/theme/shop_basic/skin_bs/shop/basic/img/1_bottom_icon.png" border="0"/>
<img src="/eyoom/theme/shop_basic/skin_bs/shop/basic/img/2_bottom_icon.png"  border="0"/>
</div>
<div style="display:inline-block;">
<a href="https://www.lotteglogis.com/home/personal/inquiry/track" target="_blank">
<img src="/eyoom/theme/shop_basic/skin_bs/shop/basic/img/3_bottom_icon.png"  border="0"/>
</a>
<img src="/eyoom/theme/shop_basic/skin_bs/shop/basic/img/4_bottom_icon.png"  border="0"/>
<img src="/eyoom/theme/shop_basic/skin_bs/shop/basic/img/cj.png"  border="0"/>
</div>
</div>
<div class="col-md-3 md-margin-bottom-40">
<div class="heading-footer"><h4>Customer Center</h4></div>
<ul class="list-unstyled footer-link-list">
<li class="font-size-25 margin-bottom-15"><strong><i class="fa fa-phone"></i> <?php echo $TPL_VAR["default"]["de_admin_company_tel"]?></strong></li>
<li class="font-size-12">업무시간 : <span class="pull-right">09:00 ~ 18:00</span></li>
<li class="font-size-12">점심시간 : <span class="pull-right">12:00 ~ 13:00</span></li>
<li class="font-size-12">업체휴무 : <span class="pull-right">토요일, 일요일, 공휴일</span></li>
</ul>
</div>
<div class="col-md-3 sm-margin-bottom-30">
<div class="heading-footer"><h4>Information</h4></div>
<ul class="list-unstyled footer-link-list">
<!--<li><a href="<?php echo G5_URL?>/page/?pid=aboutus">About Us</a></li>-->
<li><a href="<?php echo G5_URL?>/page/?pid=provision">이용약관</a></li>
<li><a href="<?php echo G5_URL?>/page/?pid=privacy">개인정보 취급방침</a></li>
<li><a href="<?php echo G5_URL?>/page/?pid=noemail">이메일 무단수집거부</a></li>
<!--<li><a href="<?php echo G5_URL?>/page/?pid=contactus">Contact Us</a></li>-->
</ul>
</div>
<div class="col-md-3">
<div class="heading-footer"><h4>Contact Us</h4></div>
<form name="frm1">
<input name="wrkr_no" type="hidden" value="<?php echo preg_replace('/-/','',$TPL_VAR["default"]["de_admin_company_saupja_no"])?>"/>
<address class="font-size-12 md-margin-bottom-40">
- 회사명 : <?php echo $TPL_VAR["default"]["de_admin_company_name"]?><br />
- 대표 : <?php echo $TPL_VAR["default"]["de_admin_company_owner"]?><br />
- <?php echo $TPL_VAR["default"]["de_admin_company_addr"]?><br />
- 전화 : <?php echo $TPL_VAR["default"]["de_admin_company_tel"]?><br />
- 사업자번호 : <?php echo $TPL_VAR["default"]["de_admin_company_saupja_no"]?> <?php if($TPL_VAR["default"]["de_admin_tongsin_no"]){?><input class="btn-e btn-e-dark btn-e-xs" type="button" value="정보확인" onclick="onopen();"/><?php }?><br />
- 통신판매업 : <?php echo $TPL_VAR["default"]["de_admin_tongsin_no"]?><br />
- 개인정보관리 : <?php echo $TPL_VAR["default"]["de_admin_info_name"]?><br />
- Email: <a href="mailto:<?php echo $TPL_VAR["default"]["de_admin_info_email"]?>"><?php echo $TPL_VAR["default"]["de_admin_info_email"]?></a>
</address>
</form>
</div>
</div>
</div>
<div class="copyright">
<div class="container">
<p class="text-center">Copyright &copy; <?php echo $TPL_VAR["config"]["cf_title"]?>. All Rights Reserved.</p>
</div>
</div>
</div>
</div></div>
<?php if($GLOBALS["is_admin"]&&!G5_IS_MOBILE&&!$_GET["wmode"]){?>
<?php $this->print_("misc_bs",$TPL_SCP,1);?>
<?php }?>
<form name="fitem_for_list" method="post" action="" onsubmit="return fitem_for_list_submit(this);">
<input type="hidden" name="url">
<input type="hidden" name="it_id">
</form>
<script>
function item_wish_for_list(it_id)
{
var f = document.fitem_for_list;
f.url.value = "<?php echo G5_SHOP_URL?>/wishupdate.php?it_id="+it_id;
f.it_id.value = it_id;
f.action = "<?php echo G5_SHOP_URL?>/wishupdate.php";
f.submit();
}
</script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/js/jquery.bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/js/jquery.sidebar.min.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/js/back-to-top.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/plugins/scrollbar/src/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/plugins/scrollbar/src/perfect-scrollbar.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/skin_bs/shop/basic/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/skin_bs/shop/basic/js/owl-carousel.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/js/app.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
App.init();
OwlCarousel.initOwlCarousel();
$(".img-thumb img").attr("width","100%");
$(".img-thumb img").removeAttr("height");
});
function onopen() {
var url =
"http://www.ftc.go.kr/info/bizinfo/communicationViewPopup.jsp?wrkr_no="+frm1.wrkr_no.value;
window.open(url, "communicationViewPopup", "width=750, height=700;");
}
jQuery(document).ready(function ($) {
"use strict";
$('.contentHolder').perfectScrollbar();
});
$(window).scroll(function(){
rd_width=document.documentElement.clientWidth;
rd_height=document.documentElement.clientHeight;
var scrollTop = $(document).scrollTop();
$("#scroll").text(scrollTop);
if(scrollTop>100){
$(".header .navbar.navbar-default").addClass("navbar-fixed");
}else{
$(".header .navbar.navbar-default").removeClass("navbar-fixed");
}
if(rd_width > 1310){
var contents = parseInt($(".basic-bs").css("height"));
var scrollTop = $(document).scrollTop();
var quick_height = $(".quick-menu").height();
if(scrollTop < 179){
scrollTop = 79;
} else {
scrollTop = scrollTop-100;
}
$(".quick-menu").stop();
$(".quick-menu").animate({"top" : scrollTop});
}
});
</script>
<script>
function onPopKBAuthMark()
{
window.open('','KB_AUTHMARK','height=604, width=648, status=yes, toolbar=no, menubar=no,location=no');
document.KB_AUTHMARK_FORM.action='http://escrow1.kbstar.com/quics';
document.KB_AUTHMARK_FORM.target='KB_AUTHMARK';
document.KB_AUTHMARK_FORM.submit();
}
</script>
<script type="text/javascript">
wcs_do();
</script>
<!--[if lt IE 9]>
<script src="/eyoom/theme/shop_basic/js/respond.js"></script>
<script src="/eyoom/theme/shop_basic/js/html5shiv.js"></script>
<![endif]-->
<?php $this->print_("tail_sub",$TPL_SCP,1);?>