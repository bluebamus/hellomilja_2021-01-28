<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/listtype.skin.html 000001048 */  $this->include_("eb_paging");?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<?php if(file_exists($GLOBALS["list_file"])){?>
<section class="margin-bottom-20">
<div class="headline">
<h5><strong><?php echo $TPL_VAR["g5"]["title"]?></strong></h5>
</div>
<?php echo $TPL_VAR["list"]->set_view('it_img',true)?>
<?php echo $TPL_VAR["list"]->set_view('it_id',false)?>
<?php echo $TPL_VAR["list"]->set_view('it_name',true)?>
<?php echo $TPL_VAR["list"]->set_view('it_cust_price',true)?>
<?php echo $TPL_VAR["list"]->set_view('it_price',true)?>
<?php echo $TPL_VAR["list"]->set_view('it_icon',true)?>
<?php echo $TPL_VAR["list"]->set_view('sns',true)?>
<?php echo $TPL_VAR["list"]->run()?>
</section>
<?php }else{?>
<div align="center"><?php echo $GLOBALS["skin"]?> 파일을 찾을 수 없습니다.<br>관리자에게 알려주시면 감사하겠습니다.</div>
<?php }?>
<?php echo eb_paging('basic')?>