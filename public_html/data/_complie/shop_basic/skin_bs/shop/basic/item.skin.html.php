<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/item.skin.html 000002503 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<style>
.shop-detail-view .headline {border-bottom:1px solid #000;border-top:0}
.shop-detail-view .headline h5 {border-top:0;border-bottom:1px solid #DE2600}
.shop-detail-view .headline i {color:#DE2600}
.shop-detail-view .headline span {color:#959595}
.shop-product-wrap .tab-e1 .nav-tabs a {font-size:12px}
.shop-product-wrap .tab-e1 .nav-tabs {border-bottom:1px solid #555}
.shop-product-wrap .tab-e1 .nav-tabs > .active > a,.shop-product-wrap .tab-e1 .nav-tabs > .active > a:hover,.shop-product-wrap .tab-e1 .nav-tabs > .active > a:focus {background:#555}
.shop-product-wrap .tab-e1 .nav-tabs > li > a:hover {background:#555}
@media (max-width: 767px){
.shop-product-wrap .tab-e1 .nav-tabs {border:1px solid #ddd;padding:7px;background:#fafafa}
}
</style>
<div class="shop-detail-view">
<?php if($TPL_VAR["shop"]->get_navigation()){?>
<div id="sct_location">
<a href='<?php echo G5_SHOP_URL?>/' class="sct_bg">Home</a>
<?php echo $TPL_VAR["shop"]->get_navigation()?>
</div>
<?php }?>
<?php if($TPL_VAR["shop"]->listcategory()){?>
<aside id="sct_ct_1" class="sct_ct">
<h6><strong><i class="fa fa-align-justify"></i> 현재 상품 분류와 관련된 분류</strong></h6>
<ul class="list-inline bg-light" style="margin-left:0">
<?php if(is_array($TPL_R1=$TPL_VAR["shop"]->listcategory())&&!empty($TPL_R1)){foreach($TPL_R1 as $TPL_V1){?>
<li class="font-size-12"><a href="<?php echo $TPL_V1["href"]?>"><?php echo $TPL_V1["ca_name"]?> (<?php echo $TPL_V1["cnt"]?>)</a></li>
<?php }}?>
</ul>
</aside>
<?php }?>
<div class="clearfix">
<div class="headline">
<h5><strong><i class="fa fa-ellipsis-v"></i> <span>카테고리 -</span> <?php echo $TPL_VAR["shop"]->get_navigation()?></strong></h5>
<?php if($GLOBALS["is_admin"]){?><a href="<?php echo G5_ADMIN_URL?>/shop_admin/itemform.php?w=u&amp;it_id=<?php echo $GLOBALS["it_id"]?>" class="btn-e btn-e-purple pull-right">상품 관리</a><?php }?>
</div>
</div>
<div id="sit_hhtml"><?php echo conv_content($TPL_VAR["it"]["it_head_html"], 1)?></div>
<?php if($GLOBALS["is_orderable"]){?>
<script src="/js/shop.js"></script>
<?php }?>
<div class="shop-product-wrap">
<?php $this->print_("item_form_bs",$TPL_SCP,1);?>
<?php $this->print_("item_info_bs",$TPL_SCP,1);?>
</div>
<div id="sit_thtml"><?php echo conv_content($TPL_VAR["it"]["it_tail_html"], 1)?></div>
</div>