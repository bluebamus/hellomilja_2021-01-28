<?php /* Template_ 2.2.8 2019/11/25 14:50:10 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/item_use.skin.html 000005515 */ 
$TPL__item_use_1=empty($GLOBALS["item_use"])||!is_array($GLOBALS["item_use"])?0:count($GLOBALS["item_use"]);?>
<?php if (!defined('_GNUBOARD_')) ?>
<style>
#shop-product-use {padding:10px 0}
#shop-product-use .panel {border:0;border-top:1px solid #d5d5d5;box-shadow:none}
#shop-product-use .panel:last-child {border-bottom:1px solid #d5d5d5}
#shop-product-use .panel-group .panel+.panel {margin-top:0}
#shop-product-use .panel-title {position:relative}
#shop-product-use .panel-title a {color:#000;background:#fcfcfc}
#shop-product-use .panel-title .product-use-img {width:45px;margin-right:10px}
#shop-product-use .panel-title .product-use-img img {display:block;width:100%;max-width:100%;height:auto}
#shop-product-use .panel-title .title-subj {font-size:12px;margin-bottom:5px;padding-right:30px}
#shop-product-use .panel-title .divide {color:#c5c5c5;margin-left:7px;margin-right:7px}
#shop-product-use .panel-title .indicator {font-size:11px;width:26px;height:16px;line-height:15px;background:#474A5E;border:1px solid #2E3340;text-align:center;color:#fff;position:absolute;top:11px;right:0}
#shop-product-use .panel-title .indicator.fa-angle-up {background:#FF2900;border:1px solid #DE2600}
#shop-product-use .panel-heading a {padding:10px 0 8px}
#shop-product-use .panel-body {padding:15px 0;border-top:1px dotted #d5d5d5}
#shop-product-use-list .panel-body img {display:block}
#shop-product-use .product-ratings {margin-bottom:0;margin-right:5px}
#shop-product-use .product-ratings li {font-size:12px}
#shop-product-use .product-ratings li .rating-selected {font-size:12px}
#shop-product-use .product-ratings li .rating {font-size:12px}
</style>
<section>
<div class="panel-group acc-v1" id="porduct-review">
<?php if($TPL__item_use_1){foreach($GLOBALS["item_use"] as $TPL_K1=>$TPL_V1){?>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">
<div class="pull-left product-use-img">
<a href="<?php echo $TPL_V1["it_href"]?>">
<?php echo get_itemuselist_thumbnail($TPL_V1["it_id"],$TPL_V1["is_content"], 200,'')?>
<span class="sound_only"><?php echo $TPL_V1["it_name"]?></span>
</a>
</div>
<a class="accordion-toggle" data-toggle="collapse" data-parent="#porduct-review" href="#review_<?php echo $TPL_K1?>">
<p class="title-subj"><strong><?php echo $TPL_V1["is_num"]?>. <?php echo $TPL_V1["is_subject"]?></strong></p>
<div class="pull-left">
<span class="font-size-12 color-grey">
<?php echo $TPL_V1["is_name"]?><span class="divide">|</span><?php echo $TPL_V1["is_time"]?>
</span>
</div>
<ul class="list-inline product-ratings pull-right">
<li>별 <?php echo $TPL_V1["is_star"]?>개&nbsp;</li>
<li><i class="rating<?php if($TPL_V1["is_star"]> 0){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["is_star"]> 1){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["is_star"]> 2){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["is_star"]> 3){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["is_star"]> 4){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
</ul>
<i class="indicator fa fa-angle-down pull-right"></i>
<div class="clearfix"></div>
</a>
</div>
</div>
<div id="review_<?php echo $TPL_K1?>" class="panel-collapse collapse">
<div class="panel-body">
<?php echo $TPL_V1["is_content"]?>
<?php if($GLOBALS["is_admin"]||$TPL_V1["mb_id"]==$TPL_VAR["member"]["mb_id"]){?>
<div class="text-right">
<a href="<?php echo $TPL_V1["link_edit"]?>" class="btn-e btn-e-purple btn-e-xs itemuse_form" onclick="return false;">수정</a>
<a href="<?php echo $TPL_V1["link_del"]?>" class="btn-e btn-e-dark btn-e-xs">삭제</a>
</div>
<?php }?>
</div>
</div>
</div>
<?php }}else{?>
<p class="text-center">사용후기가 없습니다.</p>
<?php }?>
</div>
</section>
<?php echo $GLOBALS["paging_itemuse"]?>
<div class="text-right margin-top-15">
<a href="<?php echo $GLOBALS["itemuse_form"]?>" class="btn-e btn-e-purple" target="_blank"><i class="fa fa-pencil"></i> 사용후기 쓰기</a>
<a href="<?php echo $GLOBALS["itemuse_list"]?>" class="btn-e btn-e-dark"><i class="fa fa-search-plus"></i> 더보기</a>
</div>
<script src="/js/viewimageresize.js"></script>
<script>
function toggleAngle(e) {
$(e.target)
.prev('.panel-heading')
.find("i.indicator")
.toggleClass('fa-angle-up fa-angle-down');
}
$('#porduct-review').on('hidden.bs.collapse', toggleAngle);
$('#porduct-review').on('shown.bs.collapse', toggleAngle);
$(function(){
$(".itemuse_form").click(function(){
window.open(this.href, "itemuse_form", "width=810,height=680,scrollbars=1");
return false;
});
$(".itemuse_delete").click(function(){
if (confirm("정말 삭제 하시겠습니까?\n\n삭제후에는 되돌릴수 없습니다.")) {
return true;
} else {
return false;
}
});
$(".sit_use_li_title").click(function(){
var $con = $(this).siblings(".sit_use_con");
if($con.is(":visible")) {
$con.slideUp();
} else {
$(".sit_use_con:visible").hide();
$con.slideDown(
function() {
// 이미지 리사이즈
$con.viewimageresize2();
}
);
}
});
$(".pg_page").click(function(){
$("#itemuse").load($(this).attr("href"));
return false;
});
});
$(window).on("load", function() {
$(".panel-body").viewimageresize2();
});
</script>