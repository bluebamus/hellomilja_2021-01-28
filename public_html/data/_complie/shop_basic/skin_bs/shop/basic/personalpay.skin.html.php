<?php /* Template_ 2.2.8 2017/10/23 00:04:03 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/personalpay.skin.html 000001550 */  $this->include_("eb_paging");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<style>
.shop-personal-pay .personal-img {border:1px solid #d5d5d5}
.shop-personal-pay .personal-pay {border:1px solid #d5d5d5;border-top:0;padding:8px 10px}
.shop-personal-pay .personal-price {border:1px solid #d5d5d5;background:#f8f8f8;border-top:0;padding:8px 10px;text-align:right;margin-bottom:30px}
.shop-personal-pay .personal-price strong {color:#ae0000}
</style>
<div class="shop-personal-pay">
<section>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<div class="col-sm-4">
<div class="personal-img"><a href="<?php echo $TPL_V1["href"]?>" class="sct_a"><img src="/eyoom/theme/shop_basic/skin_bs/shop/basic/img/personal.jpg" class="img-responsive" alt=""></a></div>
<div class="personal-pay"><a href="<?php echo $TPL_V1["href"]?>"><?php echo get_text($TPL_V1["pp_name"])?>님 개인결제</a></div>
<div class="personal-price"><strong><?php echo display_price($TPL_V1["pp_price"])?></strong></div>
</div>
<?php if($TPL_K1% 3== 0){?>
<div class="clearfix"></div>
<?php }?>
<?php }}else{?>
<div class="text-center">
<p><i class="fa fa-exclamation-circle"></i> 등록된 개인결제가 없습니다.</p>
</div>
<?php }?>
</section>
<div class="clearfix"></div>
<?php echo eb_paging('basic')?>
</div>