<?php /* Template_ 2.2.8 2017/03/06 16:59:42 /home/hellomilja.com/www/eyoom/theme/shop_basic/skin_bs/shop/basic/ordersendcostcoupon.skin.html 000001327 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div id="sc_coupon_frm">
<?php if($GLOBALS["count"]> 0){?>
<div class="table-list-eb">
<div class="table-responsive">
<table id="sod_list" class="table table-hover">
<thead>
<tr>
<th>쿠폰명</th>
<th>할인금액</th>
<th>적용</th>
</tr>
</thead>
<tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
<tr>
<td>
<input type="hidden" name="s_cp_id[]" value="<?php echo $TPL_V1["cp_id"]?>">
<input type="hidden" name="s_cp_prc[]" value="<?php echo $TPL_V1["dc"]?>">
<input type="hidden" name="s_cp_subj[]" value="<?php echo $TPL_V1["cp_subject"]?>">
<?php echo get_text($TPL_V1["cp_subject"])?>
</td>
<td><?php echo number_format($TPL_V1["dc"])?></td>
<td><button type="button" class="sc_cp_apply btn-e btn-e-red btn-e-xs">적용</button></td>
</tr>
<?php }}?>
</tbody>
</table>
</div>
</div>
<?php }else{?>
<div class="text-center">사용할 수 있는 쿠폰이 없습니다.</div>
<?php }?>
<div class="text-center">
<button type="button" id="sc_coupon_close" class="btn-e btn-e-dark">닫기</button>
</div>
</div>