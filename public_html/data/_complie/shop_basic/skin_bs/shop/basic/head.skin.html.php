<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/head.skin.html 000019671 */ 
$TPL_menu_1=empty($TPL_VAR["menu"])||!is_array($TPL_VAR["menu"])?0:count($TPL_VAR["menu"]);
$TPL__boxtoday_1=empty($GLOBALS["boxtoday"])||!is_array($GLOBALS["boxtoday"])?0:count($GLOBALS["boxtoday"]);?>
<?php if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/eyoom-form/css/eyoom-form.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/skin_bs/shop/basic/plugins/owl-carousel/owl-carousel/owl.carousel.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/skin_bs/shop/basic/shop_style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/custom.css" type="text/css" media="screen">',0);
?>
<?php
// ------------- 테마 디자인 설정 옵션 시작 ------------- //
/**
* Design Layout 종류 : wide or boxed
*/
$TPL_VAR["layout"] = 'wide';
/**
* Design Color 종류 : white or black
*/
$TPL_VAR["color"] = 'white';
/**
* Mega Menu 설정 : yes or no
*/
$TPL_VAR["is_megamenu"] = 'no';
/**
* Side Sticky 설정 : yes or no
*/
$TPL_VAR["is_side_sticky"] = 'no';
/**
* Header Slider 설정 : yes or no
*/
$TPL_VAR["is_slider"] = 'no';
/**
* Logo Type 종류 : image or text
*/
$TPL_VAR["logo"] = 'image';
/**
* Header Banner 종류 : image or text
*/
$TPL_VAR["hbanner"] = 'text';
/**
* Item View 종류 : zoom or slider
*/
$TPL_VAR["item_view"] = 'slider';
// ------------- 테마 디자인 설정 옵션 끝 ------------- //
?>
<div class="wrapper">
<?php if($TPL_VAR["layout"]=='wide'){?>
<div class="header-fixed basic-layout">
<?php }elseif($TPL_VAR["layout"]=='boxed'){?>
<div class="header-fixed boxed-layout container">
<?php }?>
<div class="header-topbar">
<div class="container">
<div class="row">
<div class="col-md-6">
<ul class="list-unstyled topbar-left">
<?php if(!G5_IS_MOBILE){?>
<li>
<a id="bookmarkme" href="javascript:void(0);" rel="sidebar" title="bookmark this page">북마크</a>
<script>
$(function() {
$("#bookmarkme").click(function() {
// Mozilla Firefox Bookmark
if ('sidebar' in window && 'addPanel' in window.sidebar) {
window.sidebar.addPanel(location.href,document.title,"");
} else if( /*@cc_on!@*/false) { // IE Favorite
window.external.AddFavorite(location.href,document.title);
} else { // webkit - safari/chrome
alert('단축키 ' + (navigator.userAgent.toLowerCase().indexOf('mac') != - 1 ? 'Command' : 'CTRL') + ' + D를 눌러 북마크에 추가하세요.');
}
});
});
</script>
</li>
<?php }?>
<?php if(G5_USE_SHOP){?>
<!--<li>
<a href="<?php echo G5_URL?>">커뮤니티</a>
</li>-->
<?php }?>
<?php if($GLOBALS["is_member"]){?>
<li class="btn-group">
<a href="#" data-toggle="dropdown">내메뉴</a>
<a href="#" data-toggle="dropdown">
<i class="fa fa-caret-down"></i>
<span class="sr-only">Toggle Dropdown</span>
</a>
<ul class="dropdown-menu" role="menu">
<li><a href="<?php echo G5_SHOP_URL?>/personalpay.php">개인결제</a></li>
<li><a href="<?php echo G5_SHOP_URL?>/mypage.php">쇼핑몰 마이페이지</a></li>
<li><a href="<?php echo G5_BBS_URL?>/member_confirm.php?url=<?php echo G5_BBS_URL?>/register_form.php">정보수정</a></li>
</ul>
</li>
<?php }?>
<li class="btn-group">
<a href="#" data-toggle="dropdown">더보기</a>
<a href="#" data-toggle="dropdown">
<i class="fa fa-caret-down"></i>
<span class="sr-only">Toggle Dropdown</span>
</a>
<ul class="dropdown-menu" role="menu">
<li><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=1">히트상품</a></li>
<li><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=2">추천상품</a></li>
<li><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=3">최신상품</a></li>
<li><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=4">인기상품</a></li>
<li><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=5">할인상품</a></li>
<div class="divider"></div>
<li><a href="<?php echo G5_BBS_URL?>/board.php?bo_table=faq">FAQ</a></li>
<li><a href="<?php echo G5_BBS_URL?>/board.php?bo_table=qa">1:1문의</a></li>
<li><a href="<?php echo G5_SHOP_URL?>/itemuselist.php">사용후기</a></li>
<?php if(!$GLOBALS["is_member"]){?>
<li><a href="<?php echo G5_SHOP_URL?>/orderinquiry.php">비회원주문조회</a></li>
<?php }?>
</ul>
</li>
</ul>
</div>
<div class="col-md-6">
<ul class="list-unstyled topbar-right">
<?php if($GLOBALS["is_member"]){?>
<?php if($GLOBALS["is_admin"]){?>
<li><a href="<?php echo G5_ADMIN_URL?>">관리자</a></li>
<?php }?>
<li class="tooltips" data-placement="bottom" data-toggle="tooltip" data-original-title="장바구니">
<a href="<?php echo G5_SHOP_URL?>/cart.php"><i class="fa fa-shopping-cart"></i></a>
</li>
<li class="tooltips" data-placement="bottom" data-toggle="tooltip" data-original-title="위시리스트">
<a href="<?php echo G5_SHOP_URL?>/wishlist.php"><i class="fa fa-heart"></i></a>
</li>
<li class="tooltips" data-placement="bottom" data-toggle="tooltip" data-original-title="주문/배송조회">
<a href="<?php echo G5_SHOP_URL?>/orderinquiry.php"><i class="fa fa-truck"></i></a>
</li>
<li><a href="<?php echo G5_BBS_URL?>/logout.php">로그아웃</a></li>
<?php if(!G5_IS_MOBILE){?>
<?php echo $TPL_VAR["latest"]->latest_memo('memo_latest','count=8||cut_subject=20||photo=y')?>
<?php echo $TPL_VAR["latest"]->latest_respond('respond_latest','count=8||cut_subject=20||photo=y')?>
<?php }?>
<?php }else{?>
<li><a href="<?php echo G5_BBS_URL?>/register.php">회원가입</a></li>
<li><a href="<?php echo G5_BBS_URL?>/login.php">로그인</a></li>
<?php }?>
</ul>
</div>
</div>
</div>
</div>
<div class="header-title">
<div class="container">
<div class="header-logo">
<?php if($TPL_VAR["logo"]=='text'){?>
<a class="navbar-brand" href="<?php echo G5_SHOP_URL?>"><?php echo $TPL_VAR["config"]["cf_title"]?></a>
<?php }elseif($TPL_VAR["logo"]=='image'){?>
<a class="navbar-brand" href="<?php echo G5_URL?>"><img src="/eyoom/theme/shop_basic/image/site_logo.png" class="img-responsive" alt="<?php echo $TPL_VAR["config"]["cf_title"]?> LOGO"></a>
<?php }?>
</div>
<?php if(!G5_IS_MOBILE){?>
<!--<div class="header-banner pull-right hidden-sm hidden-xs">
<div class="header-banner-box">
&lt;!&ndash;<?php if($TPL_VAR["hbanner"]=='text'){?>&ndash;&gt;
<div class="header-banner-txt">
<a href="#" target="_blank">Header Banner 468x60 이미지 사이즈</a>
</div>
&lt;!&ndash;<?php }elseif($TPL_VAR["hbanner"]=='image'){?>&ndash;&gt;
<div class="header-banner-img">
<a href="#" target="_blank"><img src="../../../image/이미지파일"></a>
</div>
&lt;!&ndash;<?php }?>&ndash;&gt;
</div>
</div>-->
<?php }?>
<div class="clearfix"></div>
</div>
</div>
<?php if($TPL_VAR["color"]=='white'){?>
<div class="header-nav nav-background-light header-sticky">
<?php }elseif($TPL_VAR["color"]=='black'){?>
<div class="header-nav nav-background-dark header-sticky">
<?php }?>
<div class="navbar mega-menu" role="navigation">
<div class="container">
<div class="menu-container">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-align-justify"></span>
</button>
<div class="nav-in-right">
<ul class="menu-icons-list">
<li class="menu-icons">
<i class="menu-icons-style search search-close search-btn fa fa-search"></i>
</li>
</ul>
</div>
</div>
<div class="menu-container">
<div class="search-open">
<form name="frmsearch1" action="<?php echo G5_SHOP_URL; ?>/search.php" onsubmit="return search_submit(this);">
<input type="hidden" name="sfl" value="wr_subject||wr_content">
<input type="hidden" name="sop" value="and">
<label for="sch_stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<input type="text" name="q" value="<?php echo stripslashes(get_text(get_search_string($q))); ?>" id="sch_str" placeholder="쇼핑몰검색" required class="form-control">
</form>
<script>
function search_submit(f) {
if (f.q.value.length < 2) {
alert("검색어는 두글자 이상 입력하십시오.");
f.q.select();
f.q.focus();
return false;
}
return true;
}
</script>
</div>
</div>
<div class="collapse navbar-collapse navbar-responsive-collapse">
<div class="menu-container">
<ul class="nav navbar-nav">
<!--<li class="<?php if(defined('_INDEX_')){?>active<?php }?> home-menu">-->
<!--<a href="<?php echo G5_SHOP_URL?>">HOME</a>-->
<!--</li>-->
<?php if($TPL_VAR["eyoom"]["use_eyoom_shopmenu"]=='n'){?>
<?php if($TPL_menu_1){foreach($TPL_VAR["menu"] as $TPL_V1){
$TPL_submenu_2=empty($TPL_V1["submenu"])||!is_array($TPL_V1["submenu"])?0:count($TPL_V1["submenu"]);?>
<li class="<?php if($TPL_V1["active"]){?>active<?php }?> dropdown">
<a href="<?php echo $TPL_V1["href"]?>" class="dropdown-toggle" <?php if(G5_IS_MOBILE){?>data-toggle="dropdown"<?php }else{?>data-hover="dropdown"<?php }?>>
<?php echo $TPL_V1["ca_name"]?>
</a>
<?php if($TPL_submenu_2){foreach($TPL_V1["submenu"] as $TPL_K2=>$TPL_V2){?>
<?php if($TPL_K2== 0){?>
<ul class="dropdown-menu">
<?php }?>
<li <?php if($TPL_V2["active"]){?>class="active"<?php }?>>
<a href="<?php echo $TPL_V2["href"]?>"><?php echo $TPL_V2["ca_name"]?></a>
</li>
<?php if($TPL_K2==$TPL_V1["cnt"]- 1){?>
</ul>
<?php }?>
<?php }}?>
</li>
<?php }}?>
<?php }elseif($TPL_VAR["eyoom"]["use_eyoom_shopmenu"]=='y'){?>
<?php if($TPL_menu_1){foreach($TPL_VAR["menu"] as $TPL_V1){
$TPL_submenu_2=empty($TPL_V1["submenu"])||!is_array($TPL_V1["submenu"])?0:count($TPL_V1["submenu"]);?>
<li class="<?php if($TPL_V1["active"]){?>active<?php }?> <?php if($TPL_V1["submenu"]){?>dropdown<?php }?>">
<!--lim-fix 2017/10/22-->
<!--<a href="<?php echo $TPL_V1["me_link"]?>" target="_<?php echo $TPL_V1["me_target"]?>" class="dropdown-toggle" <?php if(G5_IS_MOBILE&&$TPL_V1["submenu"]){?>data-toggle="dropdown"<?php }else{?>data-hover="dropdown"<?php }?> style="<?php if($TPL_V1["me_name"]=="SALE"){?> color:pink <?php }else{?>  <?php }?>">-->
<a href="<?php echo $TPL_V1["me_link"]?>" target="_<?php echo $TPL_V1["me_target"]?>" class="dropdown-toggle" <?php if(G5_IS_MOBILE&&$TPL_V1["submenu"]){?>data-toggle="dropdown"<?php }else{?>data-hover="dropdown"<?php }?> ">
<?php echo $TPL_V1["me_name"]?>&nbsp;<?php if($TPL_V1["new"]){?>&nbsp; <i class="fa fa-check-circle color-red"></i><?php }?> <?php if($TPL_V1["me_icon"]){?><i class="fa <?php echo $TPL_V1["me_icon"]?>"></i> <?php }?> &nbsp;
</a>
<?php if($TPL_submenu_2){$TPL_I2=-1;foreach($TPL_V1["submenu"] as $TPL_V2){$TPL_I2++;
$TPL_subsub_3=empty($TPL_V2["subsub"])||!is_array($TPL_V2["subsub"])?0:count($TPL_V2["subsub"]);?>
<?php if($TPL_I2== 0){?>
<ul class="dropdown-menu">
<?php }?>
<li class="dropdown-submenu <?php if($TPL_V2["active"]){?>active<?php }?>">
<a href="<?php echo $TPL_V2["me_link"]?>" target="_<?php echo $TPL_V2["me_target"]?>"><?php echo $TPL_V2["me_name"]?>&nbsp;&nbsp;<?php if($TPL_V2["new"]){?>&nbsp;<i class="fa fa-check-circle color-red"></i><?php }?><?php if($TPL_V2["me_icon"]){?><i class="fa <?php echo $TPL_V2["me_icon"]?>"></i> <?php }?> <?php if($TPL_V2["sub"]=='on'){?><i class="fa fa-angle-right sub-caret hidden-sm hidden-xs"></i><i class="fa fa-angle-down sub-caret hidden-md hidden-lg"></i><?php }?></a>
<?php if($TPL_subsub_3){$TPL_I3=-1;foreach($TPL_V2["subsub"] as $TPL_V3){$TPL_I3++;?>
<?php if($TPL_I3== 0){?>
<ul class="dropdown-menu <?php if($TPL_V3["active"]){?>active<?php }?>">
<?php }?>
<li class="dropdown-submenu">
<a href="<?php echo $TPL_V3["me_link"]?>" target="_<?php echo $TPL_V3["me_target"]?>"><?php if($TPL_V3["me_icon"]){?><i class="fa <?php echo $TPL_V3["me_icon"]?>"></i> <?php }?><?php echo $TPL_V3["me_name"]?><?php if($TPL_V3["new"]){?>&nbsp;<i class="fa fa-check-circle color-red"></i><?php }?><?php if($TPL_V3["sub"]=='on'){?><i class="fa fa-angle-right sub-caret hidden-sm hidden-xs"></i><i class="fa fa-angle-down sub-caret hidden-md hidden-lg"></i><?php }?></a>
</li>
<?php if($TPL_I3==$TPL_subsub_3- 1){?>
</ul>
<?php }?>
<?php }}?>
</li>
<?php if($TPL_I2==$TPL_submenu_2- 1){?>
</ul>
<?php }?>
<?php }}?>
</li>
<?php }}?>
<?php }?>
<?php if($TPL_VAR["is_megamenu"]=='yes'){?>
<li class="dropdown mega-menu-area">
<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
전체메뉴
</a>
<ul class="dropdown-menu">
<li>
<div class="mega-menu-content disable-icons">
<div class="container">
<div class="row mega-height">
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
<div class="col-md-2 mega-height-in">
<ul class="list-unstyled mega-height-list">
<li><h3>전체메뉴 그룹명</h3></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
<li><a href="#"><i class="fa fa-genderless"></i> 게시판명 설정</a></li>
</ul>
</div>
</div>
</div>
</div>
</li>
</ul>
</li>
<?php }?>
</ul>
</div>
</div>	            </div>
</div>
</div>
<div class="header-sticky-space"></div>
<?php if(defined('_SHOP_')){?>
<div class="container" style="position:relative;top:0;">
<div class="quick-menu">
<ul class="list-unstyled">
<li>
<a href="<?php echo G5_SHOP_URL?>/cart.php">
<i class="fa fa-shopping-cart"></i>
<span>장바구니</span>
</a>
</li>
<li>
<a href="<?php echo G5_SHOP_URL?>/wishlist.php">
<i class="fa fa-heart"></i>
<span>위시리스트</span>
</a>
</li>
<li>
<a href="<?php echo G5_SHOP_URL?>/orderinquiry.php">
<i class="fa fa-truck"></i>
<span>주문/배송</span>
</a>
</li>
<li class="current-view">
<div class="quick-carousel">
<h4>최근 본 상품</h4>
<div id="quickCarousel" class="carousel slide carousel-e1 item-carousel">
<div class="carousel-inner">
<?php if($TPL__boxtoday_1){$TPL_I1=-1;foreach($GLOBALS["boxtoday"] as $TPL_V1){$TPL_I1++;?>
<div class="item <?php if($TPL_I1== 0){?>active<?php }?>">
<?php echo $TPL_V1["img"]?>
</div>
<?php }}else{?>
<div class="item active"><p>최근 본 상품이<br>없습니다.</p></div>
<?php }?>
</div>
<div class="carousel-arrow clearfix">
<a class="left carousel-control pull-left" href="#quickCarousel" data-slide="prev">이전</a>
<a class="right carousel-control pull-right" href="#quickCarousel" data-slide="next">다음</a>
</div>
</div>
</div>
</li>
</ul>
</div>
</div>
<?php }?>
<div class="basic-body container">
<!--<div class="basic-body container">-->
<div class="row">
<?php if((defined('_INDEX_')&&$TPL_VAR["eyoom"]["use_main_side_layout"]=='y')||(!defined('_INDEX_')&&$TPL_VAR["eyoom"]["use_sub_side_layout"]=='y'&&$TPL_VAR["subinfo"]["sidemenu"]!='n')){?>
<?php if($TPL_VAR["eyoom"]["pos_side_layout"]=='left'){?>
<?php $this->print_("side_bs",$TPL_SCP,1);?>
<?php }?>
<div class="basic-body-main <?php if($TPL_VAR["eyoom"]["pos_side_layout"]=='left'){?>right<?php }else{?>left<?php }?>-main col-md-9">
<?php }else{?>
<div class="basic-body-main col-md-12">
<?php }?>
<?php if(defined('_INDEX_')||defined('_EYOOM_MYPAGE_')){?>
<?php if($TPL_VAR["is_slider"]=='yes'){?>
<div class="header-slider">
<div id="owl-header-slider" class="owl-carousel owl-theme">
<div class="item header-slider-bg">
<img src="/eyoom/theme/shop_basic/image/header_slider/slider_1.jpg" class="img-responsive">
<div class="container">
<h1>Welcome to <?php echo $TPL_VAR["config"]["cf_title"]?></h1>
<h2><?php echo $TPL_VAR["config"]["cf_title"]?> 을 방문해 주셔서 감사합니다.</h2>
<p>Lorem ipsum dulor sit amet.<br>Consectetur adipisicing elft. Harum dolore, sequi ex quam sunt delectus veniam aut tempore illum, dulor nemo quae nulla.<br>Nam perferendis enim quisquam, culpa.</p>
</div>
</div>
<div class="item header-slider-bg">
<img src="/eyoom/theme/shop_basic/image/header_slider/slider_2.jpg" class="img-responsive">
<div class="container">
<h1>Have a good time</h1>
<p><br>Lorem ipsum dulor sit amet.<br>Consectetur adipisicing elft. Harum dolore, sequi ex quam sunt delectus veniam aut tempore illum, dulor nemo quae nulla.<br>Nam perferendis enim quisquam, culpa.</p>
<div class="text-center margin-top-10">
<a href="<?php echo G5_URL?>/page/?pid=aboutus" class="btn-e btn-e-yellow btn-e-lg">About Us</a>
</div>
</div>
</div>
</div>
</div>
<?php }?>
<?php }else{?>
<div class="board-title">
<div class="container">
<h3><i class="fa fa-map-marker"></i> <?php echo $TPL_VAR["subinfo"]["title"]?></h3>
</div>
</div>
<?php }?>