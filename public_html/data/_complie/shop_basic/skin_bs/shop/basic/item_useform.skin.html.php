<?php /* Template_ 2.2.8 2017/10/23 00:04:03 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/item_useform.skin.html 000004184 */ ?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/eyoom-form/css/eyoom-form.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/skin_bs/shop/basic/shop_style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/custom.css" type="text/css" media="screen">',0);
?>
<style>
.shop-product-use-form {padding:15px}
.shop-product-use-form fieldset {padding:0}
.shop-product-use-form .btn {font-size:14px}
.shop-product-use-form .eyoom-form .rating label:hover {color:#f25858}
.shop-product-use-form .eyoom-form .rating input:checked ~ label {color:#e33334}
.shop-product-use-form .tbl_frm01 textarea,.frm_input {border:1px solid #eee;background:#f8f8f8}
/* Smart Editor Style */
.cke_sc {margin-bottom:10px}
.btn_cke_sc {padding:0 10px}
.cke_sc_def {padding:10px;margin-bottom:10px;margin-top:10px}
.cke_sc_def button {padding:3px 15px;background:#53535a;color:#fff;border:none}
</style>
<div class="shop-product-use-form">
<form name="fitemuse" method="post" action="./itemuseformupdate.php" onsubmit="return fitemuse_submit(this);" autocomplete="off" class="eyoom-form">
<input type="hidden" name="w" value="<?php echo $GLOBALS["w"]?>">
<input type="hidden" name="it_id" value="<?php echo $GLOBALS["it_id"]?>">
<input type="hidden" name="is_id" value="<?php echo $GLOBALS["is_id"]?>">
<div class="tbl_frm01 tbl_wrap">
<fieldset>
<div class="row">
<div class="col col-12">
<div class="headline"><h6><strong>사용후기 쓰기</strong></h6></div>
</div>
<section class="col col-6">
<label for="is_subject" class="sound_only">제목<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-prepend fa fa-edit"></i>
<input type="text" name="is_subject" value="<?php echo get_text($TPL_VAR["use"]["is_subject"])?>" id="is_subject" required class="form-control" minlength="2" maxlength="250">
<b class="tooltip tooltip-top-left">제목을 입력 해 주세요.</b>
</label>
</section>
<div class="col col-12">
<div class="margin-hr-10"></div>
</div>
<section class="col col-12">
<?php echo $GLOBALS["editor_html"]?>
</section>
<div class="col col-12">
<div class="margin-hr-10"></div>
</div>
<section class="col col-4 rating">
<input type="radio" name="is_score" value="5" id="is_score5">
<label for="is_score5"><i class="fa fa-star"></i></label>
<input type="radio" name="is_score" value="4" id="is_score4">
<label for="is_score4"><i class="fa fa-star"></i></label>
<input type="radio" name="is_score" value="3" id="is_score3">
<label for="is_score3"><i class="fa fa-star"></i></label>
<input type="radio" name="is_score" value="2" id="is_score2">
<label for="is_score2"><i class="fa fa-star"></i></label>
<input type="radio" name="is_score" value="1" id="is_score1">
<label for="is_score1"><i class="fa fa-star"></i></label>
평점선택
<p class="note"><strong>Note:</strong> 솔직한 평점 부탁드려요.^^</p>
</section>
<div class="clearfix"></div>
</div>
</fieldset>
</div>
<div class="margin-hr-10"></div>
<div class="text-center margin-top-15 margin-bottom-15">
<input type="submit" value="작성완료" class="btn-e btn-e-red">
<button type="button" onclick="self.close();" class="btn-e btn-e-dark">닫기</button>
</div>
</form>
</div>
<script type="text/javascript">
function fitemuse_submit(f)
{
<?php echo $GLOBALS["editor_js"]?>
return true;
}
</script>