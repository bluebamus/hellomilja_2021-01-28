<?php /* Template_ 2.2.8 2017/10/23 00:04:03 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/cart_option.skin.html 000008661 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="shop-option">
<form name="foption" method="post" action="<?php echo G5_SHOP_URL?>/cartupdate.php" onsubmit="return formcheck(this);">
<input type="hidden" name="act" value="optionmod">
<input type="hidden" name="it_id[]" value="<?php echo $TPL_VAR["it"]["it_id"]?>">
<input type="hidden" id="it_price" value="<?php echo $TPL_VAR["row2"]["ct_price"]?>">
<input type="hidden" name="ct_send_cost" value="<?php echo $TPL_VAR["row2"]["ct_send_cost"]?>">
<input type="hidden" name="sw_direct">
<?php if($GLOBALS["option_1"]){?>
<section class="tbl_wrap tbl_head02">
<h3>선택옵션</h3>
<table class="sit_ov_tbl">
<colgroup>
<col class="grid_3">
<col>
</colgroup>
<tbody>
<?php echo $GLOBALS["option_1"]?>
</tbody>
</table>
</section>
<?php }?>
<?php if($GLOBALS["option_2"]){?>
<section class="tbl_wrap tbl_head02">
<h3>추가옵션</h3>
<table class="sit_ov_tbl">
<colgroup>
<col class="grid_3">
<col>
</colgroup>
<tbody>
<?php echo $GLOBALS["option_2"]?>
</tbody>
</table>
</section>
<?php }?>
<div id="sit_sel_option">
<ul id="sit_opt_added">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<li class="sit_<?php echo $TPL_V1["cls"]?>_list">
<input type="hidden" name="io_type[<?php echo $TPL_VAR["it"]["it_id"]?>][]" value="<?php echo $TPL_V1["io_type"]?>">
<input type="hidden" name="io_id[<?php echo $TPL_VAR["it"]["it_id"]?>][]" value="<?php echo $TPL_V1["io_id"]?>">
<input type="hidden" name="io_value[<?php echo $TPL_VAR["it"]["it_id"]?>][]" value="<?php echo $TPL_V1["ct_option"]?>">
<input type="hidden" class="io_price" value="<?php echo $TPL_V1["io_price"]?>">
<input type="hidden" class="io_stock" value="<?php echo $TPL_V1["it_stock_qty"]?>">
<span class="sit_opt_subj"><?php echo $TPL_V1["ct_option"]?></span>
<span class="sit_opt_prc"><?php if($TPL_V1["io_price"]< 0){?>(<?php echo number_format($TPL_V1["io_price"])?>)<?php }else{?>(+<?php echo number_format($TPL_V1["io_price"])?>)<?php }?></span>
<div>
<label for="ct_qty_<?php echo $TPL_K1?>" class="sound_only">수량</label>
<input type="text" name="ct_qty[<?php echo $TPL_VAR["it"]["it_id"]?>][]" value="<?php echo $TPL_V1["ct_qty"]?>" id="ct_qty_<?php echo $TPL_K1?>" class="frm_input" size="5">
<button type="button" class="sit_qty_plus btn_frmline">증가</button>
<button type="button" class="sit_qty_minus btn_frmline">감소</button>
<button type="button" class="btn_frmline">삭제</button>
</div>
</li>
<?php }}?>
</ul>
</div>
<div id="sit_tot_price"></div>
<div class="text-center">
<input type="submit" value="선택사항적용" class="btn-e btn-e-purple">
<button type="button" id="mod_option_close" class="btn-e btn-e-dark">닫기</button>
</div>
</form>
</div>
<?php if(!G5_IS_MOBILE){?>
<style>
#sod_bsk_list #mod_option_frm {z-index:10000;position:absolute;top:0;left:0;padding:10px;width:100%;max-width:500px;height:auto !important;height:500px;max-height:500px;border:1px solid #000;background:#fbfbfb;overflow-y:scroll;overflow-x:none}
#sod_bsk_list #mod_option_frm .tbl_wrap {margin-bottom:15px}
#sod_bsk_list #mod_option_frm .tbl_wrap h3 {font-weight:bold;text-align:left;font-size:13px;color:#007AFF;margin-bottom:10px;border-bottom:1px dotted #d5d5d5}
#sod_bsk_list #mod_option_frm .tbl_wrap tbody {border-bottom:0}
#sod_bsk_list #mod_option_frm .sit_ov_tbl {background:none}
#sod_bsk_list #mod_option_frm .sit_ov_tbl th {background:none}
#sod_bsk_list #mod_option_frm #sit_opt_added {margin-bottom:10px;border:1px solid #d5d5d5;border-bottom:0}
#sod_bsk_list #mod_option_frm #sit_opt_added li {padding:10px;border-bottom:1px solid #d5d5d5}
#sod_bsk_list #mod_option_frm #sit_opt_added .sit_opt_subj {font-size:12px;color:#7347CF}
#sod_bsk_list #mod_option_frm #sit_opt_added .sit_opt_prc {font-size:12px;color:#000}
#sod_bsk_list #mod_option_frm .frm_input {height:16px;line-height:16px;border:1px solid #000;padding:0 5px;margin-right:5px}
#sod_bsk_list #mod_option_frm button.btn_frmline {font-size:12px;height:18px;line-height:18px;padding:0 8px;background:#2E3340}
#sod_bsk_list #mod_option_frm button.btn_frmline:hover {background:#DE2600}
#sod_bsk_list #mod_option_frm #sit_tot_price {font-size:15px;font-weight:bold;color:#ae0000;margin-bottom:15px;text-align:right}
@media (max-width: 767px) {
#sod_bsk_list #mod_option_frm {max-width:300px}
.table-responsive {overflow-y:auto}
}
</style>
<?php }?>
<?php if(G5_IS_MOBILE){?>
<style>
/* 영카트 모바일 기본 CSS 수정 */
#sod_bsk_list #mod_option_frm {z-index:1;position:relative;top:0;left:0;padding:10px;width:280px;max-width:280px;height:auto !important;height:500px;max-height:500px;border:1px solid #bacdf8;background:#fff;overflow-y:scroll;overflow-x:none;margin:10px 0}
#sod_bsk_list #mod_option_frm form {border:inherit;background:inherit;padding:inherit;max-height:inherit;overflow-y:inherit}
#sod_bsk_list #mod_option_frm #sit_tot_price {font-size:15px;font-weight:bold;color:#ae0000;margin-bottom:15px;text-align:right}
#sod_bsk_list #mod_option_frm #sit_tot_price span {position:relative}
#sod_bsk_list #mod_option_frm .tbl_wrap {margin-bottom:15px}
#sod_bsk_list #mod_option_frm .tbl_wrap h3 {font-weight:bold;text-align:left;font-size:13px;color:#007AFF;margin-bottom:10px;border-bottom:1px dotted #d5d5d5}
#sod_bsk_list #mod_option_frm .tbl_wrap tbody {border-bottom:0}
#sod_bsk_list #mod_option_frm .sit_ov_tbl {background:none}
#sod_bsk_list #mod_option_frm .sit_ov_tbl th {background:none}
#sod_bsk_list #mod_option_frm #sit_opt_added {margin-bottom:10px;border:0;border-top:1px solid #b5b5b5}
#sod_bsk_list #mod_option_frm #sit_opt_added li {padding:10px 0 15px;border-bottom:1px solid #b5b5b5}
#sod_bsk_list #mod_option_frm #sit_opt_added .sit_opt_subj {font-size:12px;color:#7347CF}
#sod_bsk_list #mod_option_frm #sit_opt_added .sit_opt_prc {font-size:12px;color:#000}
#sit_opt_added .sit_opt_list span, #sit_opt_added .sit_spl_list span {line-height:24px;font-weight:bold}
#sit_opt_added li div {height:28px;margin:5px 0 0 10px;text-align:right;position:relative;width:160px;float:right}
#sit_opt_added li div .frm_input {width:46px;border:1px solid #d5d5d5;height:28px;padding:0 !important;line-height:28px;text-align:center;background:#fff;position:absolute;top:0;left:40px
}
#sit_opt_added li div .sit_qty_minus {overflow:hidden;position:absolute;top:0;left:0;background:none;background-color:#f2f2f2;text-indent:0;color:#f2f2f2;border:1px solid #d5d5d5;width:40px;height:28px}
#sit_opt_added li div .sit_qty_minus:after {content:"\f068";color:#5a5a5a;font-family:FontAwesome;font-size:12px;position:absolute;top:50%;left:50%;margin-top:-10px;margin-left:-4px}
#sit_opt_added li div .sit_qty_plus {overflow:hidden;position:absolute;top:0;left:81px;background:none;background-color:#f2f2f2;text-indent:0;color:#f2f2f2;border:1px solid #d5d5d5;width:40px;height:28px}
#sit_opt_added li div .sit_qty_plus:after {content:"\f067";color:#5a5a5a;font-family:FontAwesome;font-size:12px;position:absolute;top:50%;left:50%;margin-top:-10px;margin-left:-4px}
</style>
<?php }?>
<script>
// 셀렉트 이윰폼 적용
jQuery(document).ready(function(){
$(".shop-option .it_option, .shop-option .it_supply").wrap('<label class="select" />');
$(".shop-option .it_option, .shop-option .it_supply").after('<i></i>');
});
function formcheck(f)
{
var val, io_type, result = true;
var sum_qty = 0;
var min_qty = parseInt(<?php echo $TPL_VAR["it"]["it_buy_min_qty"]?>);
var max_qty = parseInt(<?php echo $TPL_VAR["it"]["it_buy_max_qty"]?>);
var $el_type = $("input[name^=io_type]");
$("input[name^=ct_qty]").each(function(index) {
val = $(this).val();
if(val.length < 1) {
alert("수량을 입력해 주십시오.");
result = false;
return false;
}
if(val.replace(/[0-9]/g, "").length > 0) {
alert("수량은 숫자로 입력해 주십시오.");
result = false;
return false;
}
if(parseInt(val.replace(/[^0-9]/g, "")) < 1) {
alert("수량은 1이상 입력해 주십시오.");
result = false;
return false;
}
io_type = $el_type.eq(index).val();
if(io_type == "0")
sum_qty += parseInt(val);
});
if(!result) {
return false;
}
if(min_qty > 0 && sum_qty < min_qty) {
alert("선택옵션 개수 총합 "+number_format(String(min_qty))+"개 이상 주문해 주십시오.");
return false;
}
if(max_qty > 0 && sum_qty > max_qty) {
alert("선택옵션 개수 총합 "+number_format(String(max_qty))+"개 이하로 주문해 주십시오.");
return false;
}
return true;
}
</script>