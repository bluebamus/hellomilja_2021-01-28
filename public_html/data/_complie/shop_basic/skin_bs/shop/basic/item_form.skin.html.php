<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/item_form.skin.html 000027964 */ 
$TPL__thumb_big_1=empty($GLOBALS["thumb_big"])||!is_array($GLOBALS["thumb_big"])?0:count($GLOBALS["thumb_big"]);
$TPL__thumb_info_1=empty($GLOBALS["thumb_info"])||!is_array($GLOBALS["thumb_info"])?0:count($GLOBALS["thumb_info"]);
$TPL_bigimg_1=empty($TPL_VAR["bigimg"])||!is_array($TPL_VAR["bigimg"])?0:count($TPL_VAR["bigimg"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/skin_bs/shop/basic/plugins/fotorama/fotorama.css" type="text/css" media="screen">',0);
?>
<style>
/*----- Shop Detail Product -----*/
.shop-product {padding:10px 0}
.shop-product .color-red {color:#DE2600}
.shop-product h2 {float:left;color:#687074;font-size:26px;text-transform:uppercase}
.shop-product .list-inline {margin:0}
.shop-different-product {padding:10px 0}
.shop-different-product .btn {font-size:11px;color:#000}
.shop-product .sct_here {color:crimson}
.shop-product .sct_here:before {content:"\f105";font-family:FontAwesome;padding-right:5px;color:#888}
.shop-product .sct_bg {padding-right:5px;color:#888;background:none}
.shop-product-img {position:relative;overflow:hidden}
/*Shop Product Img*/
.zoomLens {cursor:pointer !important}
.product-img-big {margin:0 0 1px}
.product-img-big a {display:none}
.product-img-big a.visible {display:block}
.product-img-big img {width:100%;display:block;max-width:100%;height:auto}
.product-img-big i {width:100%;background:#e5e5e5;text-align:center;color:#757575;height:408px;line-height:408px;font-size:70px}
.product-img-big .fotorama__thumb-border {border-color:#00ff00}
@media (max-width: 500px) {
.product-img-big i {height:300px;line-height:300px}
}
.product-thumb-wrap {position:relative;overflow:hidden;max-height:240px}
.product-thumb-wrap .owl-navi a.owl-btn {position:relative;overflow:hidden;color:#fff;width:20px;height:100%;font-size:16px;cursor:pointer;line-height:100%;text-align:center;display:inline-block;opacity:0.6;background:#000;border-top:1px solid #fff;border-bottom:1px solid #fff;visibility:hidden}
.product-thumb-wrap:hover .owl-navi a.owl-btn {visibility:visible}
.product-thumb-wrap .owl-navi a.owl-btn:hover {color:#fff;opacity:0.8;background:#DE2600;-webkit-transition:all 0.2s ease-in-out;-moz-transition:all 0.2s ease-in-out;-o-transition:all 0.2s ease-in-out;transition:all 0.2s ease-in-out}
.product-thumb-wrap .owl-navi a.owl-btn i {position:absolute;top:50%;left:8px;margin-top:-9px}
.product-thumb-wrap .owl-navi a.owl-btn.prev-thumb {position:absolute;top:0;left:0;z-index:1}
.product-thumb-wrap .owl-navi a.owl-btn.next-thumb {position:absolute;top:0;right:0;z-index:1}
.product-thumb:after {display:block;visibility:hidden;clear:both;content:""}
.product-thumb .thumb-img-p {position:relative;overflow:hidden;float:left;width:100%}
.product-thumb .thumb-img-i {padding:1px}
/*Shop Product Img Modal*/
.shop-img-modal .modal-header {padding:15px;border-bottom:1px dotted #d5d5d5}
.shop-img-modal .modal-title {color:#000}
.shop-img-modal .modal-header .close {margin-top:0}
.shop-img-modal .modal-body {padding:15px}
.shop-img-modal .fotorama__thumb-border {border-color:#00ff00}
@media (min-width: 768px) {
.shop-img-modal .modal-dialog {width:620px}
}
/*Shp Product Title*/
.shop-product .shop-product-heading {overflow:hidden;margin-bottom:0}
.shop-product .shop-product-heading h3 {color:#000}
/*Shp Product Social*/
.shop-product .shop-product-social {text-align:right}
.shop-product .shop-product-social li {display:inline-block;margin-right:10px}
.shop-product .shop-product-social li i {color:#aaa;font-size:16px;display:inline-block}
.shop-product .shop-product-social li i:hover {color:#e33334;text-decoration:none}
/*Product Ratings*/
.shop-product .product-review-list {margin-left:20px}
.shop-product .product-review-list,.shop-product .product-review-list a {color:#999;font-size:14px}
.shop-product .product-review-list a:hover {color:#18ba9b}
.shop-product .shop-product-prices li:first-child {padding-left:0}
/*Shop Product Prices*/
.shop-product .shop-product-prices li {font-size:18px}
.shop-product .shop-product-prices .prices {color:#ae0000;font-weight:bold}
.shop-product .line-through {color:#b5b5b5;text-decoration:line-through}
.shop-product #sit_tot_price {font-weight:bold;font-size:20px;margin-top:4px;color:#000}
.shop-product .product-ratings  {margin-bottom:10px}
/*Badge*/
.shop-product .time-day-left {top:-6px;color:#fff;font-size:12px;padding:3px 8px;margin-left:40px;position:relative}
/*Shop Product Description*/
.shop-product .shop-intro-box {position:relative;font-size:12px;color:#000;border-top:1px solid #ddd;padding-top:10px}
.shop-product .shop-description-box {position:relative;padding:10px 0;background:#fff;border-width:1px 0 0;border-style:solid;border-color:#ddd;margin:0}
.shop-product section.shop-product-description {color:#000;font-size:12px;border-bottom:1px dotted #e5e5e5;padding-bottom:6px;margin-bottom:6px;clear:both}
.shop-product section.shop-product-description:last-child {border-bottom:0;padding-bottom:0;margin-bottom:0}
.shop-product .sit_ov_tbl label {font-size:12px;color:#000}
.shop-product .shop-soldout {position:relative;font-size:12px;color:#DE2600;border-top:1px solid #ddd;padding-top:10px}
/*Shop Product Select Opition*/
.shop-product .sit_ov_tbl tr {line-height:36px}
.shop-product .select {position:absolute;right:0;margin-top:-15px}
.shop-product .select select {min-width:200px}
/*Product Quantity*/
.shop-product .shop-product-quantity {position:relative;border-top:1px solid #ddd;padding-top:10px}
.shop-product .product-quantity {float:left;border:1px solid #ccc}
.shop-product .quantity-button {color:#f2f2f2;width:46px;height:32px;padding:5px;border:none;outline:none;cursor:pointer;font-size:12px;background:#f2f2f2;text-align:center;font-weight:normal;white-space:nowrap;display:inline-block;background-image:none;position:relative}
.shop-product .quantity-field {width:46px;height:32px;outline:none;margin:0 -4px;font-size:12px;text-align:center;border-width:0 1px;border-style:solid;border-color:#ccc;background:#fff;font-weight:bold}
.shop-product .quantity-button i  {position:absolute;top:10px;left:18px;color:#6a6a6a}
.shop-product .frm_input {height:24px;line-height:24px;border:1px solid #000;padding:0 5px;margin-right:5px}
.shop-product button.btn_frmline {font-size:12px;padding:0 8px;background:#2E3340}
.shop-product button.btn_frmline:hover {background:#DE2600}
.shop-product #sit_opt_added li {padding:10px 0}
.shop-product #sit_opt_added button {margin-left:4px}
/*product select*/
.shop-product #sit_opt_added {border-width:1px 0 0;border-style:solid;border-color:#ddd;font-size:12px}
.shop-product #sit_opt_added li {border-top:1px dotted #ddd;border-bottom:0 none}
.shop-product #sit_opt_added li:first-child {border:0 none}
.shop-product #sit_opt_added .sit_opt_list span, .shop-product #sit_opt_added .sit_spl_list span {color:#007AFF}
/*product btn*/
.shop-product .shop-product-button {position:relative;border-top:1px solid #ddd;padding:10px 0 0}
.shop-product .shop-product-button .btn-purchase {height:42px;line-height: 2px;vertical-align:middle;font-weight:bold;background:#007AFF;border:1px solid #006ee1;color:#fff;width:34%;float:left;margin-right:1%}
.shop-product .shop-product-button .btn-purchase:hover {background:#0061D6;border:1px solid #0049a0}
.shop-product .shop-product-button .btn-cart {height:42px;line-height:42px;vertical-align:middle;font-weight:bold;background:#2E3340;border:1px solid #242630;color:#fff;width:34%;float:left;margin-right:1%}
.shop-product .shop-product-button .btn-cart:hover {background:#242630;border:1px solid #1C1C26}
.shop-product .shop-product-button a {display:inline-block;padding:0;text-align:center;vertical-align:middle;text-decoration:none;line-height:42px;height:42px;border:1px solid #a5a5a5;color:#474A5E;font-size:1.2em;font-weight:bold;background:#e5e5e5}
.shop-product .shop-product-button a.btn-wish {float:left;width:14.5%}
.shop-product .shop-product-button a.btn-recommend {float:left;width:14.5%;margin-left:1%}
.shop-product .shop-product-button a:hover {background:#d5d5d5;border-color:#959595}
</style>
<?php if(G5_IS_MOBILE){?>
<style>
/* 영카트 모바일 기본 CSS 수정 */
.shop-product #sit_tot_price {text-align:left;margin:0}
.shop-product #sit_tot_price span {position:relative;margin-right:10px;top:0;left:0}
.shop-product .sit_ov_tbl tbody th {height:35px}
.shop-product .it_option, .shop-product .it_supply {width:inherit;padding-right:10px;min-width:80px}
a.btn-item {height:32px}
#sit_opt_added .sit_opt_list span, #sit_opt_added .sit_spl_list span {line-height:24px;color:#007AFF;font-weight:bold}
#sit_opt_added li div {height:32px;margin:5px 0 0 10px;text-align:right;position:relative;width:160px;float:right}
#sit_opt_added li div .frm_input {width:46px;border:1px solid #d5d5d5;height:32px;padding:0 !important;line-height:32px;text-align:center;background:#fff;position:absolute;top:0;left:40px
}
#sit_opt_added li div .sit_qty_minus {overflow:hidden;position:absolute;top:0;left:0;background:none;background-color:#f2f2f2;text-indent:0;color:#f2f2f2;border:1px solid #d5d5d5;width:40px;height:32px}
#sit_opt_added li div .sit_qty_minus:after {content:"\f068";color:#5a5a5a;font-family:FontAwesome;font-size:12px;position:absolute;top:50%;left:50%;margin-top:-10px;margin-left:-4px}
#sit_opt_added li div .sit_qty_plus {overflow:hidden;position:absolute;top:0;left:81px;background:none;background-color:#f2f2f2;text-indent:0;color:#f2f2f2;border:1px solid #d5d5d5;width:40px;height:32px}
#sit_opt_added li div .sit_qty_plus:after {content:"\f067";color:#5a5a5a;font-family:FontAwesome;font-size:12px;position:absolute;top:50%;left:50%;margin-top:-10px;margin-left:-4px}
#sit_opt_added li div .sit_opt_del {background:none;position:absolute;top:3px;right:0;text-indent:0;width:24px;height:24px;padding:0;background-color:#2E3340;color:#2E3340;border-radius:24px !important}
#sit_opt_added li div .sit_opt_del:after {content:"\f00d";color:#fff;font-family:FontAwesome;font-size:12px; position:absolute;top:50%;left:50%;margin-top:-11px;margin-left:-5px}
</style>
<?php }?>
<form name="fitem" method="post" action="<?php echo $GLOBALS["action_url"]?>" onsubmit="return fitem_submit(this);">
<input type="hidden" name="it_id[]" value="<?php echo $GLOBALS["it_id"]?>">
<input type="hidden" name="sw_direct">
<input type="hidden" name="url">
<div class="shop-product">
<div class="row">
<div class="col-sm-6 md-margin-bottom-20">
<?php if($TPL_VAR["item_view"]=='zoom'){?>
<div class="shop-product-img">
<div class="product-img-big">
<?php if($TPL__thumb_big_1){foreach($GLOBALS["thumb_big"] as $TPL_V1){?>
<a href="javascript:;" <?php if(!G5_IS_MOBILE){?>id="elevaterzoom_item" data-toggle="modal" data-target=".shop-img-modal"<?php }?>><?php echo $TPL_V1["img"]?></a>
<?php }}else{?>
<i class="fa fa-picture-o"></i>
<?php }?>
</div>
<?php if($GLOBALS["thumb_total_count"]> 1){?>
<div class="product-thumb-wrap">
<div class="owl-navi">
<a class="owl-btn prev-thumb"><i class="fa fa-angle-left"></i></a>
<a class="owl-btn next-thumb"><i class="fa fa-angle-right"></i></a>
</div>
<section class="product-thumb owl-carousel">
<?php if($TPL__thumb_info_1){foreach($GLOBALS["thumb_info"] as $TPL_V1){?>
<div class="thumb-img-p">
<div class="thumb-img-i">
<a href="#" <?php if(!G5_IS_MOBILE){?>data-toggle="modal" data-target=".shop-img-modal"<?php }?> class="popup-item-image img-thumb">
<?php echo $TPL_V1["img"]?><span class="sound_only"> <?php echo $TPL_V1["cnt"]?>번째 이미지 새창</span>
</a>
</div>
</div>
<?php }}?>
<div class="clearfix"></div>
</section>
</div>
<?php }?>
</div>
<?php }elseif($TPL_VAR["item_view"]=='slider'){?>
<div class="shop-product-img">
<div class="product-img-big fotorama" data-nav="thumbs" data-max-width="100%" data-loop="true" data-allowfullscreen="true">
<?php if($TPL__thumb_big_1){foreach($GLOBALS["thumb_big"] as $TPL_V1){?>
<?php echo $TPL_V1["img"]?>
<?php }}else{?>
<i class="fa fa-picture-o"></i>
<?php }?>
</div>
</div>
<?php }?>
</div>
<div class="col-sm-6">
<div class="shop-product-heading">
<h3><strong><?php echo stripslashes($TPL_VAR["it"]["it_name"])?></strong></h3>
<ul class="list-inline product-ratings">
<li><i class="rating<?php if($GLOBALS["star_score"]> 0){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($GLOBALS["star_score"]> 1){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($GLOBALS["star_score"]> 2){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($GLOBALS["star_score"]> 3){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($GLOBALS["star_score"]> 4){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li class="ratings-average">평점 별 <?php echo $GLOBALS["star_score"]?>개</li>
</ul>
<div class="shop-description-box1">
<?php if(!$TPL_VAR["it"]["it_use"]){?>
<ul class="list-inline shop-product-prices pull-left">
<li class="color-red"><strong>판매중지</strong></li>
</ul>
<?php }elseif($TPL_VAR["it"]["it_tel_inq"]){?>
<ul class="list-inline shop-product-prices pull-left">
<li class="color-red"><strong>전화문의</strong></li>
</ul>
<?php }else{?>
<ul class="list-inline shop-product-prices pull-left">
<li class="prices">₩ <span><?php echo display_price($GLOBALS["sell_price"])?></span><input type="hidden" id="it_price" value="<?php echo $GLOBALS["sell_price"]?>"></li>
<?php if($TPL_VAR["it"]["it_cust_price"]){?>
<li class="line-through">₩ <span><?php echo display_price($TPL_VAR["it"]["it_cust_price"])?></span></li>
<?php }?>
</ul>
<?php }?>
<!-- <ul class="social-icons pull-right">
<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $GLOBALS["sns_url"]?>&amp;p=<?php echo $GLOBALS["sns_title"]?>" target="_blank" data-original-title="Facebook" class="social_facebook"></a></li>
<li><a href="https://twitter.com/share?url=<?php echo $GLOBALS["sns_url"]?>&amp;text=<?php echo $GLOBALS["sns_title"]?>" target="_blank" data-original-title="Twitter" class="social_twitter"></a></li>
<li><a href="https://plus.google.com/share?url=<?php echo $GLOBALS["sns_url"]?>" target="_blank" data-original-title="Google Plus" class="social_google"></a></li>
</ul>-->
</div>
<div class="clearfix"></div>
<?php if($TPL_VAR["it"]["it_basic"]){?>
<div class="shop-intro-box">
<p><i class="fa fa-info-circle"></i> <?php echo $TPL_VAR["it"]["it_basic"]?></p>
</div>
<?php }?>
</div>
<div class="shop-description-box">
<?php if($TPL_VAR["it"]["it_maker"]){?>
<section class="shop-product-description">- 제조사<strong class="pull-right"><?php echo $TPL_VAR["it"]["it_maker"]?></strong></section>
<?php }?>
<?php if($TPL_VAR["it"]["it_origin"]){?>
<section class="shop-product-description">- 원산지<strong class="pull-right"><?php echo $TPL_VAR["it"]["it_origin"]?></strong></section>
<?php }?>
<?php if($TPL_VAR["it"]["it_brand"]){?>
<section class="shop-product-description">- 브랜드<strong class="pull-right"><?php echo $TPL_VAR["it"]["it_brand"]?></strong></section>
<?php }?>
<?php if($TPL_VAR["it"]["it_model"]){?>
<section class="shop-product-description">- 모델<strong class="pull-right"><?php echo $TPL_VAR["it"]["it_model"]?></strong></section>
<?php }?>
<?php if( 0){?>
<section class="shop-product-description">- 재고수량<strong class="pull-right"><?php echo number_format(get_it_stock_qty($GLOBALS["it_id"]))?> 개</strong></section>
<?php }?>
<?php if($TPL_VAR["config"]["cf_use_point"]){?>
<section class="shop-product-description">- 포인트<strong class="pull-right"><?php echo $GLOBALS["point_calc"]?></strong></section>
<?php }?>
<section class="shop-product-description">- <?php echo $GLOBALS["ct_send_cost_label"]?><strong class="pull-right"><?php echo $GLOBALS["sc_method"]?></strong></section>
<?php if($TPL_VAR["it"]["it_buy_min_qty"]){?>
<section class="shop-product-description">- 최소구매수량<strong class="pull-right"><?php echo number_format($TPL_VAR["it"]["it_buy_min_qty"])?> 개</strong></section>
<?php }?>
<?php if($TPL_VAR["it"]["it_buy_max_qty"]){?>
<section class="shop-product-description">- 최대구매수량<strong class="pull-right"><?php echo number_format($TPL_VAR["it"]["it_buy_max_qty"])?> 개</strong></section>
<?php }?>
</div>
<?php if($GLOBALS["option_item"]){?>
<section class="shop-description-box">
<p class="color-red font-size-12"><strong>선택옵션</strong></p>
<table class="sit_ov_tbl eyoom-form">
<tbody>
<?php echo $GLOBALS["option_item"]?>
</tbody>
</table>
</section>
<?php }?>
<?php if($GLOBALS["supply_item"]){?>
<section class="shop-description-box">
<p class="color-red font-size-12"><strong>추가옵션</strong></p>
<table class="sit_ov_tbl eyoom-form">
<tbody>
<?php echo $GLOBALS["supply_item"]?>
</tbody>
</table>
</section>
<?php }?>
<?php if($GLOBALS["is_orderable"]){?>
<section id="sit_sel_option">
<?php if(!$GLOBALS["option_item"]){?>
<div class="shop-product-quantity">
<ul class="list-unstyled">
<li class="sit_opt_list">
<input type="hidden" name="io_type[<?php echo $GLOBALS["it_id"]?>][]" value="0">
<input type="hidden" name="io_id[<?php echo $GLOBALS["it_id"]?>][]" value="">
<input type="hidden" name="io_value[<?php echo $GLOBALS["it_id"]?>][]" value="<?php echo $TPL_VAR["it"]["it_name"]?>">
<input type="hidden" class="io_price" value="0">
<input type="hidden" class="io_stock" value="<?php echo $TPL_VAR["it"]["it_stock_qty"]?>">
<div class="clearfix"></div>
<div class="product-quantity">
<button type="button" class="quantity-button">감소<i class="fa fa-minus"></i></button>
<label for="ct_qty_<?php echo $GLOBALS["i"]?>" class="sound_only">수량</label>
<input type="text" name="ct_qty[<?php echo $GLOBALS["it_id"]?>][]" value="<?php echo $TPL_VAR["it"]["it_buy_min_qty_sel"]?>" id="ct_qty_<?php echo $GLOBALS["i"]?>" class="quantity-field" size="5">
<button type="button" class="quantity-button">증가<i class="fa fa-plus"></i></button>
</div>
<div class="clearfix"></div>
</li>
</ul>
</div>
<div class="shop-product-button">
<?php if($GLOBALS["is_orderable"]){?>
<input type="submit" onclick="document.pressed=this.value;" value="바로구매" class="btn-purchase">
<input type="submit" onclick="document.pressed=this.value;" value="장바구니" class="btn-cart">
<a href="javascript:item_wish(document.fitem, '<?php echo $TPL_VAR["it"]["it_id"]?>');" class="btn-wish tooltips" data-placement="top" data-toggle="tooltip" data-original-title="위시리스트"><i class="fa fa-heart"></i></a>
<a href="javascript:popup_item_recommend('<?php echo $TPL_VAR["it"]["it_id"]?>');" class="btn-recommend tooltips" data-placement="top" data-toggle="tooltip" data-original-title="추천"><i class="fa fa-envelope"></i></a>
<?php if($GLOBALS["naverpay_button_js"]){?>
<div class="itemform-naverpay"><?php echo $GLOBALS["naverpay_request_js"]?><?php echo $GLOBALS["naverpay_button_js"]?></div>
<?php }?>
<div class="clearfix"></div>
<?php }?>
</div>
<script>
$(function() {
price_calculate();
});
</script>
<?php }?>
</section>
<?php }?>
<?php if($GLOBALS["is_soldout"]){?>
<div class="shop-soldout"><i class="fa fa-exclamation-circle"></i> 상품의 재고가 부족하여 구매할 수 없습니다.</div>
<?php }?>
<div class="clearfix"></div>
<?php if($GLOBALS["option_item"]||$GLOBALS["supply_item"]){?>
<div class="shop-product-button margin-bottom-10">
<?php if($GLOBALS["is_orderable"]){?>
<input type="submit" onclick="document.pressed=this.value;" value="바로구매" class="btn-purchase">
<input type="submit" onclick="document.pressed=this.value;" value="장바구니" class="btn-cart">
<a href="javascript:item_wish(document.fitem, '<?php echo $TPL_VAR["it"]["it_id"]?>');" class="btn-wish tooltips" data-placement="top" data-toggle="tooltip" data-original-title="위시리스트"><i class="fa fa-heart"></i></a>
<a href="javascript:popup_item_recommend('<?php echo $TPL_VAR["it"]["it_id"]?>');" class="btn-recommend tooltips" data-placement="top" data-toggle="tooltip" data-original-title="추천"><i class="fa fa-envelope"></i></a>
<?php if($GLOBALS["naverpay_button_js"]){?>
<div class="itemform-naverpay"><?php echo $GLOBALS["naverpay_request_js"]?><?php echo $GLOBALS["naverpay_button_js"]?></div>
<?php }?>
<div class="clearfix"></div>
<?php }?>
</div>
<?php }?>
<div class="margin-divide-10"></div>
<section class="margin-bottom-10">
<div id="sit_tot_price" class="pull-left"></div>
<div class="pull-right">
<?php if(!$GLOBALS["is_orderable"]&&$TPL_VAR["it"]["it_soldout"]&&$TPL_VAR["it"]["it_stock_sms"]){?>
<a href="javascript:popup_stocksms('<?php echo $TPL_VAR["it"]["it_id"]?>');" class="btn-e btn-e-dark">재입고알림</a>
<?php }?>
</div>
<div class="clearfix"></div>
</section>
<div class="shop-different-product">
<?php if($GLOBALS["prev_href"]||$GLOBALS["next_href"]){?>
<a href="<?php echo $GLOBALS["prev_href"]?>" class="btn btn-default pull-left"><i class="fa fa-arrow-left"> 이전상품</i><span class="sound_only"><?php echo $GLOBALS["prev_title"]?></span></a>
<a href="<?php echo $GLOBALS["next_href"]?>" class="btn btn-default pull-right">다음상품 <i class="fa fa-arrow-right"></i><span class="sound_only"><?php echo $GLOBALS["next_title"]?></span></a>
<?php }else{?>
<span class="sound_only">이 분류에 등록된 다른 상품이 없습니다.</span>
<?php }?>
<div class="clearfix"></div>
</div>
</div>
<script>
// 상품보관
function item_wish(f, it_id)
{
f.url.value = "<?php echo G5_SHOP_URL?>/wishupdate.php?it_id="+it_id;
f.action = "<?php echo G5_SHOP_URL?>/wishupdate.php";
f.submit();
}
// 추천메일
function popup_item_recommend(it_id)
{
if (!g5_is_member)
{
if (confirm("회원만 추천하실 수 있습니다."))
document.location.href = "<?php echo G5_BBS_URL?>/login.php?url=<?php echo $GLOBALS["encoded_url"]?>";
}
else
{
url = "./itemrecommend.php?it_id=" + it_id;
opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
popup_window(url, "itemrecommend", opt);
}
}
// 재입고SMS 알림
function popup_stocksms(it_id) {
url = "<?php echo G5_SHOP_URL?>/itemstocksms.php?it_id=" + it_id;
opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
popup_window(url, "itemstocksms", opt);
}
</script>
</div></div>
</form>
<div class="modal fade shop-img-modal" tabindex="-1" role="dialog" aria-labelledby="shopImgModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
<h4 id="shopImgModalLabel" class="modal-title"><strong><?php echo stripslashes($TPL_VAR["it"]["it_name"])?></strong></h4>
</div>
<div class="modal-body">
<div class="fotorama" data-nav="thumbs" data-max-width="100%" data-loop="true">
<?php if($TPL_bigimg_1){foreach($TPL_VAR["bigimg"] as $TPL_K1=>$TPL_V1){?>
<img src="<?php echo $TPL_V1["imageurl"]?>" class="full-width img-responsive" alt="<?php echo $TPL_V1["it_name"]?>" id="largeimage_<?php echo $TPL_K1?>">
<?php }}?>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="/eyoom/theme/shop_basic/skin_bs/shop/basic/plugins/fotorama/fotorama.js"></script>
<?php if($TPL_VAR["item_view"]=='zoom'){?>
<?php if(!G5_IS_MOBILE){?>
<script type="text/javascript" src="/eyoom/theme/shop_basic/skin_bs/shop/basic/plugins/jquery-elevateZoom/jquery.elevateZoom.min.js"></script>
<?php }?>
<script>
jQuery(document).ready(function(){
<?php if(!G5_IS_MOBILE){?>//PC버전의 경우에만 줌적용
$('#elevaterzoom_item img').elevateZoom({
zoomWindowWidth: 408,
zoomWindowHeight: 408,
borderSize: 0,
borderColour: "#000"
});
<?php }?>
$(function(){
// 상품이미지 첫번째 링크
$(".product-img-big a:first").addClass("visible");
// 상품이미지 미리보기 (썸네일에 마우스 오버시)
$(".shop-product-img .img-thumb").bind("mouseover focus", function(){
var idx = $(".shop-product-img .img-thumb").index($(this));
$(".product-img-big a.visible").removeClass("visible");
$(".product-img-big a:eq("+idx+")").addClass("visible");
});
});
});
</script>
<?php }?>
<script>
// 셀렉트 이윰폼 적용
jQuery(document).ready(function(){
$(".shop-description-box .it_option, .shop-description-box .it_supply").wrap('<label class="select" />');
$(".shop-description-box .it_option, .shop-description-box .it_supply").after('<i></i>');
});
// 상품사진 썸네일 캐러셀 적용
jQuery(document).ready(function() {
var owl = jQuery(".product-thumb");
owl.owlCarousel({
items : [5],
itemsDesktop : [1199,5],
itemsMobile : [1199,5],
slideSpeed: 400,
pagination: false
});
jQuery(".next-thumb").click(function(){
owl.trigger('owl.next');
})
jQuery(".prev-thumb").click(function(){
owl.trigger('owl.prev');
})
});
function fsubmit_check(f)
{
// 판매가격이 0 보다 작다면
if (document.getElementById("it_price").value < 0) {
alert("전화로 문의해 주시면 감사하겠습니다.");
return false;
}
if($(".sit_opt_list").size() < 1) {
alert("상품의 선택옵션을 선택해 주십시오.");
return false;
}
var val, io_type, result = true;
var sum_qty = 0;
var min_qty = parseInt(<?php echo $TPL_VAR["it"]["it_buy_min_qty"]?>);
var max_qty = parseInt(<?php echo $TPL_VAR["it"]["it_buy_max_qty"]?>);
var $el_type = $("input[name^=io_type]");
$("input[name^=ct_qty]").each(function(index) {
val = $(this).val();
if(val.length < 1) {
alert("수량을 입력해 주십시오.");
result = false;
return false;
}
if(val.replace(/[0-9]/g, "").length > 0) {
alert("수량은 숫자로 입력해 주십시오.");
result = false;
return false;
}
if(parseInt(val.replace(/[^0-9]/g, "")) < 1) {
alert("수량은 1이상 입력해 주십시오.");
result = false;
return false;
}
io_type = $el_type.eq(index).val();
if(io_type == "0")
sum_qty += parseInt(val);
});
if(!result) {
return false;
}
if(min_qty > 0 && sum_qty < min_qty) {
alert("선택옵션 개수 총합 "+number_format(String(min_qty))+"개 이상 주문해 주십시오.");
return false;
}
if(max_qty > 0 && sum_qty > max_qty) {
alert("선택옵션 개수 총합 "+number_format(String(max_qty))+"개 이하로 주문해 주십시오.");
return false;
}
return true;
}
// 바로구매, 장바구니 폼 전송
function fitem_submit(f)
{
f.action = "<?php echo $GLOBALS["action_url"]?>";
f.target = "";
if (document.pressed == "장바구니") {
f.sw_direct.value = 0;
} else { // 바로구매
f.sw_direct.value = 1;
}
// 판매가격이 0 보다 작다면
if (document.getElementById("it_price").value < 0) {
alert("전화로 문의해 주시면 감사하겠습니다.");
return false;
}
if($(".sit_opt_list").size() < 1) {
alert("상품의 선택옵션을 선택해 주십시오.");
return false;
}
var val, io_type, result = true;
var sum_qty = 0;
var min_qty = parseInt(<?php echo $TPL_VAR["it"]["it_buy_min_qty"]?>);
var max_qty = parseInt(<?php echo $TPL_VAR["it"]["it_buy_max_qty"]?>);
var $el_type = $("input[name^=io_type]");
$("input[name^=ct_qty]").each(function(index) {
val = $(this).val();
if(val.length < 1) {
alert("수량을 입력해 주십시오.");
result = false;
return false;
}
if(val.replace(/[0-9]/g, "").length > 0) {
alert("수량은 숫자로 입력해 주십시오.");
result = false;
return false;
}
if(parseInt(val.replace(/[^0-9]/g, "")) < 1) {
alert("수량은 1이상 입력해 주십시오.");
result = false;
return false;
}
io_type = $el_type.eq(index).val();
if(io_type == "0")
sum_qty += parseInt(val);
});
if(!result) {
return false;
}
if(min_qty > 0 && sum_qty < min_qty) {
alert("선택옵션 개수 총합 "+number_format(String(min_qty))+"개 이상 주문해 주십시오.");
return false;
}
if(max_qty > 0 && sum_qty > max_qty) {
alert("선택옵션 개수 총합 "+number_format(String(max_qty))+"개 이하로 주문해 주십시오.");
return false;
}
return true;
}
</script>