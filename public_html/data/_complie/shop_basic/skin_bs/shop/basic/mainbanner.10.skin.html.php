<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/mainbanner.10.skin.html 000003140 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="shop-mainbanner-10">
<div class="shop-mainbanner-wrap">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_K1== 0){?>
<div class="owl-mainbanner-10 owl-carousel">
<?php }?>
<div class="item">
<?php if(($TPL_V1["bn_url_0"]=='#')||($TPL_V1["bn_url"]&&$TPL_V1["bn_url"]!='http://')){?>
<a href="<?php echo $TPL_V1["bn_href"]?>" <?php echo $TPL_V1["bn_new_win"]?>>
<?php }?>
<?php if($TPL_V1["bn_alt"]){?>
<h5><?php echo $TPL_V1["bn_alt"]?></h5>
<?php }?>
<img src="<?php echo G5_DATA_URL?>/banner/<?php echo $TPL_V1["bn_id"]?>" <?php echo $TPL_V1["bn_border"]?>>
<?php if($TPL_V1["bn_href"]){?>
</a>
<?php }?>
</div>
<?php }}?>
<?php if($TPL_VAR["count"]> 0){?>
</div>
<?php }?>
</div>
</div>
<style>
.shop-mainbanner-10 .shop-mainbanner-wrap {position:relative;overflow:hidden;margin-bottom:30px;width:100%}
.shop-mainbanner-10 .item img {width:100%;display:block;max-width:100%;height:auto}
.shop-mainbanner-10 .item h5 {position:absolute;top:50%;left:0;right:0;text-align:center;padding:15px;width:60%;margin:-25px auto;font-size:14px;font-weight:bold; color:#000;background:rgba(255, 255, 255, 0.7)}
@media (max-width: 767px) {
.shop-mainbanner-10 .item h5 {top:40px;width:70%;font-size:12px;padding:7px 12px}
}
.shop-mainbanner-10 .owl-buttons .owl-prev {color:#000;width:24px;height:100%;font-size:16px;cursor:pointer;text-align:center;display:inline-block;opacity:0.8;background:#fff;position:absolute;top:0;left:0;z-index:1;visibility:hidden}
.shop-mainbanner-10 .owl-buttons .owl-next {color:#000;width:24px;height:100%;font-size:16px;cursor:pointer;text-align:center;display:inline-block;opacity:0.8;background:#fff;position:absolute;top:0;right:0;z-index:1;visibility:hidden}
.shop-mainbanner-10 .owl-buttons .owl-prev i, .shop-mainbanner-10 .owl-buttons .owl-next i {position:absolute;top:50%;left:10px;margin-top:-8px}
.shop-mainbanner-10 .owl-buttons .owl-prev:hover, .shop-mainbanner-10 .owl-buttons .owl-next:hover {color:#FF0000;opacity:0.8;background:#171C29;-webkit-transition:all 0.2s ease-in-out;-moz-transition:all 0.2s ease-in-out;-o-transition:all 0.2s ease-in-out;transition:all 0.2s ease-in-out}
.shop-mainbanner-10 .shop-mainbanner-wrap:hover .owl-buttons .owl-prev, .shop-mainbanner-10 .shop-mainbanner-wrap:hover .owl-buttons .owl-next {visibility:visible}
.shop-mainbanner-10 .owl-pagination {position:absolute;width:100%;bottom:5px;text-align:center}
.shop-mainbanner-10 .owl-pagination .owl-page {background:#e5e5e5;width:12px;height:8px;border-radius:8px !important;display:inline-block;margin:0 5px;transition:all 0.3s ease-in-out}
.shop-mainbanner-10 .owl-pagination .owl-page.active {background:#FF2900;width:24px}
.shop-mainbanner-10 #bar {width:0;max-width:100%;height:2px;background:#FF2900}
.shop-mainbanner-10 #progressBar {width:100%;background:#1C1C26}
</style>