<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/item_uselist.skin.html 000006208 */  $this->include_("eb_paging");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가 ?>
<style>
.shop-product-use-list .panel {border:0;border-top:1px solid #d5d5d5;box-shadow:none}
.shop-product-use-list .panel:last-child {border-bottom:1px solid #d5d5d5}
.shop-product-use-list .panel-group .panel+.panel {margin-top:0}
.shop-product-use-list .panel-title {position:relative}
.shop-product-use-list .panel-title a {color:#000;background:#fcfcfc}
.shop-product-use-list .panel-title .product-use-img {width:45px;margin-right:10px}
.shop-product-use-list .panel-title .product-use-img img {display:block;width:100%;max-width:100%;height:auto}
.shop-product-use-list .panel-title .title-subj {font-size:12px;margin-bottom:5px;padding-right:30px}
.shop-product-use-list .panel-title .divide {color:#c5c5c5;margin-left:7px;margin-right:7px}
.shop-product-use-list .panel-title .indicator {font-size:11px;width:26px;height:16px;line-height:15px;background:#474A5E;border:1px solid #2E3340;text-align:center;color:#fff;position:absolute;top:11px;right:0}
.shop-product-use-list .panel-title .indicator.fa-angle-up {background:#FF2900;border:1px solid #DE2600}
.shop-product-use-list .panel-heading a {padding:10px 0 5px}
.shop-product-use-list .panel-body {padding:15px 0;border-top:1px dotted #d5d5d5}
.shop-product-use-list .panel-body img {display:block}
.shop-product-use-list .product-ratings {margin-bottom:0;margin-right:5px}
.shop-product-use-list .product-ratings li {font-size:12px}
.shop-product-use-list .product-ratings li .rating-selected {font-size:12px}
.shop-product-use-list .product-ratings li .rating {font-size:12px}
</style>
<fieldset class="margin-bottom-20">
<form method="get" action="<?php echo $_SERVER['PHP_SELF']?>" class="eyoom-form">
<div class="row">
<section class="col col-12" style="text-align: center;margin: 30px 0px 30px 0px;">
<div class="font-size-11"><strong>Note:</strong> 타이틀 이미지를 클릭하시면 해당상품으로 이동됩니다.</div>
</section>
</div>
<div class="row" >
<section class="col col-2">
</section>
<section class="col col-3">
<label for="sfl" class="sound_only">검색항목<strong class="sound_only"> 필수</strong></label>
<lavel class="select">
<select name="sfl" id="sfl" required class="form-control">
<option value="">선택</option>
<option value="b.it_name"   <?php echo get_selected($GLOBALS["sfl"],"b.it_name")?>>상품명</option>
<option value="a.it_id"     <?php echo get_selected($GLOBALS["sfl"],"a.it_id")?>>상품코드</option>
<option value="a.is_subject"<?php echo get_selected($GLOBALS["sfl"],"a.is_subject")?>>후기제목</option>
<option value="a.is_content"<?php echo get_selected($GLOBALS["sfl"],"a.is_content")?>>후기내용</option>
<option value="a.is_name"   <?php echo get_selected($GLOBALS["sfl"],"a.is_name")?>>작성자명</option>
<option value="a.mb_id"     <?php echo get_selected($GLOBALS["sfl"],"a.mb_id")?>>작성자아이디</option>
</select>
</lavel>
</section>
<section class="col col-3 input-group">
<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<lavel class="input"><input type="text" name="stx" value="<?php echo $GLOBALS["stx"]?>" id="stx" required class="form-control"></lavel>
<span class="input-group-btn">
<button type="submit" value="검색" class="btn btn-default btn-e-group">검색</button>
</span>
</section>
<section class="col col-3">
<a href="<?php echo $_SERVER['PHP_SELF']?>" class="btn-e btn-e-dark btn-e-group">전체보기</a>
</section>
<section class="col col-1">
</section>
</div>
</form>
</fieldset>
<section class="shop-product-use-list">
<div class="panel-group acc-v1" id="porduct-review">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">
<div class="pull-left product-use-img">
<a href="<?php echo $TPL_V1["it_href"]?>">
<?php echo get_itemuselist_thumbnail($TPL_V1["it_id"],$TPL_V1["is_content"], 200,'')?>
<span class="sound_only"><?php echo $TPL_V1["it_name"]?></span>
</a>
</div>
<a class="accordion-toggle" data-toggle="collapse" data-parent="#porduct-review" href="#review_<?php echo $TPL_K1?>">
<p class="title-subj"><strong><?php echo $TPL_V1["is_subject"]?></strong></p>
<div class="pull-left">
<span class="font-size-12 color-grey">
<?php echo $TPL_V1["is_name"]?><span class="divide">|</span><?php echo substr($TPL_V1["is_time"], 0, 10)?>
</span>
</div>
<ul class="list-inline product-ratings pull-right">
<li>별 <?php echo $TPL_V1["star"]?>개&nbsp;</li>
<li><i class="rating<?php if($TPL_V1["star"]> 0){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star"]> 1){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star"]> 2){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star"]> 3){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star"]> 4){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
</ul>
<i class="indicator fa fa-angle-down pull-right"></i>
<div class="clearfix"></div>
</a>
</div>
</div>
<div id="review_<?php echo $TPL_K1?>" class="panel-collapse collapse">
<div class="panel-body">
<?php echo $TPL_V1["is_content"]?>
</div>
</div>
</div>
<?php }}else{?>
<p class="text-center">자료가 없습니다.</p>
<?php }?>
</div>
</section>
<?php echo eb_paging('basic')?>
<script src="/js/viewimageresize.js"></script>
<script>
function toggleAngle(e) {
$(e.target)
.prev('.panel-heading')
.find("i.indicator")
.toggleClass('fa-angle-up fa-angle-down');
}
$('#porduct-review').on('hidden.bs.collapse', toggleAngle);
$('#porduct-review').on('shown.bs.collapse', toggleAngle);
$(window).on("load", function() {
$(".panel-body").viewimageresize();
});
</script>