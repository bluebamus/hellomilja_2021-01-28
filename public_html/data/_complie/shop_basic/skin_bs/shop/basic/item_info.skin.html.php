<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/item_info.skin.html 000005404 */ 
$TPL__open_goods_info_1=empty($GLOBALS["open_goods_info"])||!is_array($GLOBALS["open_goods_info"])?0:count($GLOBALS["open_goods_info"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<style>
.shop-product-tab {position:relative;margin-bottom:20px;margin-top:15px}
.shop-product-tab .nav-tabs > li > a {margin-right:0}
.shop-product-tab .tab-e2 .nav-tabs li {width:20%}
.shop-product-tab .tab-e2 .nav-tabs li a {padding:7px 15px;background:#f8f8f8;border:solid 1px #d5d5d5;border-bottom:none;border-right:none;font-weight:bold;color:#000;text-align:center}
.shop-product-tab .tab-e2 .nav-tabs li.active a {background:#2E3340;color:#fff;padding:8px 15px 7px;border:0}
.shop-product-tab .tab-e2 .nav-tabs li.last {border-right:1px solid #d5d5d5}
.shop-product-tab .tab-e2 .nav-tabs li.active.last {border-right:1px solid #2E3340}
.shop-product-tab .tab-e2 .tab-content {position:relative;overflow:hidden;margin-top:1px;margin-bottom:0;padding:0;border:0;border-top:1px solid #2E3340}
.shop-product-tab .tab-e2 .tab-content .tab-pane {margin-top:10px}
#shop-product-info {position:relative;padding:10px 0 40px}
#shop-product-info .headline {border-bottom:1px solid #000;border-top:0}
#shop-product-info .headline h5 {border-top:0;border-bottom:1px solid #DE2600}
#shop-product-info .headline i {color:#DE2600}
#shop-product-info .table-list-eb {border-left:1px solid #ddd;border-right:1px solid #ddd;font-size:12px}
#shop-product-info .table-list-eb th {padding:8px 12px}
#shop-product-info .table-list-eb td {border-left:1px solid #ddd;padding:8px 12px;background:#fafafa}
#shop-product-rel {position:relative;padding:10px 0 0}
@media (max-width: 767px){
.shop-product-tab .tab-e2 .nav-tabs li a {font-size:11px;padding:7px 3px}
#shop-product-info .table-list-eb {border-left:0;border-right:0}
}
#shop-product-dvr {position:relative;padding:10px 0}
#shop-product-ex {position:relative;padding:10px 0}
</style>
<div class="shop-product-tab">
<div class="tab-e2">
<ul class="nav nav-tabs">
<li class="active"><a href="#shop-product-info" >상품정보</a></li>
<li><a href="#shop-product-use" >사용후기</a></li>
<li><a href="#shop-product-qa" >상품문의</a></li>
<li><a href="#shop-product-dvr" >배송정보</a></li>
<li class="last"><a href="#shop-product-ex" >교환/반품</a></li>
</ul>
<div class="headline"><h5><strong><i class="fa fa-info-circle"></i> 상품정보</strong></h5></div>
<?php if( 0){?>
<?php $this->print_("pg_anchor_inf",$TPL_SCP,1);?>
<?php }?>
<?php if($TPL_VAR["it"]["it_basic"]){?>
<div class="alert alert-warning padding-all-10 font-size-12">
<?php echo $TPL_VAR["it"]["it_basic"]?>
</div>
<?php }?>
<?php if($TPL_VAR["it"]["it_explan"]){?>
<div id="shop-product-explain">
<?php echo conv_content($TPL_VAR["it"]["it_explan"], 1)?>
</div>
<?php }?>
<?php if($TPL_VAR["it"]["it_info_value"]){?>
<div class="headline margin-top-30 margin-bottom-20"><h5><strong><i class="fa fa-exclamation-circle"></i> 상품 정보 고시</strong></h5></div>
<?php if(G5_IS_MOBILE){?>
<p class="text-right font-size-11 margin-bottom-5 color-grey">Note! 좌우 스크롤 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>
<div class="table-list-eb">
<div class="table-responsive">
<table class="table table-hover margin-bottom-0">
<tbody>
<?php if($TPL__open_goods_info_1){foreach($GLOBALS["open_goods_info"] as $TPL_V1){?>
<tr>
<th><?php echo $TPL_V1["ii_title"]?></th>
<td><?php echo $TPL_V1["ii_value"]?></td>
</tr>
<?php }}?>
</tbody>
</table>
</div>
</div>
<?php }elseif($GLOBALS["is_admin"]){?>
<p><i class="fa fa-exclamation-circle"></i> 상품 정보 고시 정보가 올바르게 저장되지 않았습니다.<br>config.php 파일의 G5_ESCAPE_FUNCTION 설정을 addslashes 로<br>변경하신 후 관리자 &gt; 상품정보 수정에서 상품 정보를 다시 저장해주세요.</p>
<?php }?>
<br><br><br>
<div class="headline"><h5><strong><i class="fa fa-info-circle"></i><a name="shop-product-use">사용후기</a></strong></h5></div>
<div id="itemuse">
<?php $this->print_("item_use_bs",$TPL_SCP,1);?>
</div>
<div class="headline"><h5><strong><i class="fa fa-info-circle"></i><a name="shop-product-qa">상품문의</a></strong></h5></div>
<div id="itemqa">
<?php $this->print_("item_qa_bs",$TPL_SCP,1);?>
</div>
<div class="headline"><h5><strong><i class="fa fa-info-circle"></i><a name="shop-product-dvr">배송정보</a></strong></h5></div>
<?php if($TPL_VAR["default"]["de_baesong_content"]){?>
<?php echo conv_content($TPL_VAR["default"]["de_baesong_content"], 1)?>
<?php }?>
<br><br><br>
<div class="headline"><h5><strong><i class="fa fa-info-circle"></i><a name="shop-product-ex">교환/반품</a></strong></h5></div>
<?php if($TPL_VAR["default"]["de_change_content"]){?>
<?php echo conv_content($TPL_VAR["default"]["de_change_content"], 1)?>
<?php }?>
</div>
</div>
<?php if($TPL_VAR["default"]["de_rel_list_use"]){?>
<div class="margin-hr-10"></div>
<section id="shop-product-rel">
<div class="headline"><h5><strong><i class="fa fa-puzzle-piece"></i> 관련상품</strong></h5></div>
<div class="sct_wrap">
<?php echo $TPL_VAR["list"]->run()?>
</div>
</section>
<?php }?>
<script src="/js/viewimageresize.js"></script>
<script>
$(window).on("load", function() {
$("#shop-product-explain").viewimageresize2();
});
</script>