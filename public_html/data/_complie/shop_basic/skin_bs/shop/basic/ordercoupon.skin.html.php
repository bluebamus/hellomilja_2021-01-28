<?php /* Template_ 2.2.8 2017/10/23 00:04:03 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/ordercoupon.skin.html 000001391 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div id="od_coupon_frm" class="margin-top-10">
<?php if($GLOBALS["count"]> 0){?>
<div class="table-list-eb">
<div class="table-responsive">
<table id="sod_list" class="table table-bordered">
<thead>
<tr>
<th>쿠폰명</th>
<th>할인금액</th>
<th>적용</th>
</tr>
</thead>
<tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
<tr>
<td>
<input type="hidden" name="o_cp_id[]" value="<?php echo $TPL_V1["cp_id"]?>">
<input type="hidden" name="o_cp_prc[]" value="<?php echo $TPL_V1["dc"]?>">
<input type="hidden" name="o_cp_subj[]" value="<?php echo $TPL_V1["cp_subject"]?>">
<?php echo get_text($TPL_V1["cp_subject"])?>
</td>
<td><?php echo number_format($TPL_V1["dc"])?></td>
<td><button type="button" class="od_cp_apply btn-e btn-e-red btn-e-xs">적용</button></td>
</tr>
<?php }}?>
</tbody>
</table>
</div>
</div>
<?php }else{?>
<div class="text-center">사용할 수 있는 쿠폰이 없습니다.</div>
<?php }?>
<div class="text-center margin-top-10">
<button type="button" id="od_coupon_close" class="btn-e btn-e-default btn-e-xs margin-bottom-5">닫기</button>
</div>
</div>