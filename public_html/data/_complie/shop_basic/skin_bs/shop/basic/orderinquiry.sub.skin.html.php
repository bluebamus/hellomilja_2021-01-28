<?php /* Template_ 2.2.8 2017/10/23 00:04:03 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/orderinquiry.sub.skin.html 000002630 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
if (!defined("_ORDERINQUIRY_")) exit; // 개별 페이지 접근 불가
?>
<div class="shop-orderinquiry-sub">
<?php if(!$GLOBALS["limit"]){?>
<p><strong>총 <?php echo $GLOBALS["cnt"]?> 건</strong></p>
<?php }?>
<?php if(G5_IS_MOBILE){?>
<p class="text-right font-size-11 margin-bottom-5 color-grey">Note! 좌우 스크롤 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>
<div class="table-list-eb">
<div class="table-responsive">
<table class="table table-bordered">
<thead>
<tr>
<th>주문서번호</th>
<th>주문일시</th>
<th>상품수</th>
<th>주문금액</th>
<th>입금액</th>
<th>미입금액</th>
<th>상태</th>
</tr>
</thead>
<tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<tr>
<td>
<input type="hidden" name="ct_id[<?php echo $TPL_K1?>]" value="<?php echo $TPL_V1["ct_id"]?>">
<a href="<?php echo $TPL_V1["href"]?>"><?php echo $TPL_V1["od_id"]?></a>
</td>
<td><?php echo substr($TPL_V1["od_time"], 2, 14)?> (<?php echo get_yoil($TPL_V1["od_time"])?>)</td>
<td><?php echo $TPL_V1["od_cart_count"]?></td>
<td><?php echo display_price($TPL_V1["od_price"])?></td>
<td><?php echo display_price($TPL_V1["od_receipt_price"])?></td>
<td><?php echo display_price($TPL_V1["od_misu"])?></td>
<td><?php echo $TPL_V1["od_status"]?></td>
</tr>
<?php }}else{?>
<tr><td colspan="7" class="text-center">주문 내역이 없습니다.</td></tr>
<?php }?>
</tbody>
</table>
</div>
</div>
</div>
<style>
.shop-orderinquiry-sub .table-bordered>thead>tr>th, .shop-orderinquiry-sub .table-bordered>tbody>tr>th, .shop-orderinquiry-sub .table-bordered>tfoot>tr>th, .shop-orderinquiry-sub .table-bordered>thead>tr>td, .shop-orderinquiry-sub .table-bordered>tbody>tr>td, .shop-orderinquiry-sub .table-bordered>tfoot>tr>td {border:1px solid #bacdf8}
.shop-orderinquiry-sub .table-list-eb {color:#000}
.shop-orderinquiry-sub .table > thead > tr > th {border:1px solid #bacdf8;vertical-align:middle}
.shop-orderinquiry-sub .table-list-eb .table tbody > tr > td {border:1px solid #bacdf8;vertical-align:middle}
.shop-orderinquiry-sub .table-list-eb .table tbody > tr > th {background:#e7efff}
.shop-orderinquiry-sub .table-list-eb thead {border-top:1px solid #bacdf8;background:#e7efff}
.shop-orderinquiry-sub .table-list-eb .table tbody > tr > td {border-top:1px solid #bacdf8}
</style>