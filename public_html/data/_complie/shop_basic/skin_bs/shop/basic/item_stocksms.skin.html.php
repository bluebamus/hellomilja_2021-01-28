<?php /* Template_ 2.2.8 2017/10/23 00:04:03 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/item_stocksms.skin.html 000004196 */ ?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/eyoom-form/css/eyoom-form.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/scrollbar/src/perfect-scrollbar.css" id="style_color" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/skin_bs/shop/basic/shop_style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/custom.css" type="text/css" media="screen">',0);
?>
<style>
.shop-item-stocksms {padding:15px}
.shop-item-stocksms fieldset {padding:0}
.shop-item-stocksms .input-group-btn .btn {font-size:14px}
.shop-item-stocksms .panel {padding:10px;border:1px solid #eee;box-shadow:none;margin-bottom:10px}
.shop-item-stocksms .contentHolder {height:120px}
</style>
<div class="shop-item-stocksms">
<div class="headline"><h6><strong><?php echo $TPL_VAR["g5"]["title"]?></strong></h6></div>
<form name="fstocksms" method="post" action="<?php echo G5_HTTPS_SHOP_URL?>/itemstocksmsupdate.php" onsubmit="return fstocksms_submit(this);"  autocomplete="off" class="eyoom-form">
<input type="hidden" name="it_id" value="<?php echo $GLOBALS["it_id"]?>">
<div class="tbl_frm01 tbl_wrap">
<fieldset>
<div class="row">
<section class="col col-12">
<label class="label">상품</label>
<h4><?php echo $TPL_VAR["it"]["it_name"]?></h4>
</section>
<div class="col col-12">
<div class="margin-hr-10"></div>
</div>
<section class="col col-12">
<label for="ss_hp" class="label">휴대폰번호<strong class="sound_only"> 필수</strong></label>
<label class="input">
<i class="icon-prepend fa fa-tablet"></i>
<input type="text" name="ss_hp" value="<?php echo $TPL_VAR["member"]["mb_hp"]?>" id="ss_hp" required class="form-control">
<b class="tooltip tooltip-top-left">휴대폰번호를 입력 해 주세요.</b>
</label>
<div class="note"><strong>Note: </strong> " - " 없이 연결하여 입력 해 주세요.</div>
</section>
<div class="col col-12">
<div class="margin-hr-10"></div>
</div>
<section class="col col-12">
<label class="label margin-left-5">개인정보처리방침안내</label>
<div class="panel panel-basic-bs no-bg">
<div id="scrollbar" class="panel-body contentHolder">
<?php echo get_text($TPL_VAR["config"]["cf_privacy"])?>
</div>
</div>
<label class="checkbox" for="agree">
<input type="checkbox" name="agree" value="1" id="agree"><i></i><span class="font-size-12">개인정보처리방침안내의 내용에 동의합니다.</span>
</label>
</section>
<div class="clearfix"></div>
</div>
</fieldset>
</div>
<div class="text-center margin-bottom-20">
<input type="submit" value="확인" class="btn-e btn-e-red">
<button type="button" onclick="window.close();" class="btn-e btn-e-dark">닫기</button>
</div>
</form>
</div>
<script type="text/javascript" src="/eyoom/theme/shop_basic/plugins/scrollbar/src/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/eyoom/theme/shop_basic/plugins/scrollbar/src/perfect-scrollbar.js"></script>
<script>
function fstocksms_submit(f)
{
if(!f.agree.checked) {
alert("개인정보처리방침안내에 동의해 주십시오.");
return false;
}
if(confirm("재입고SMS 알림 요청을 등록하시겠습니까?")) {
return true;
} else {
window.close();
return false;
}
}
jQuery(document).ready(function ($) {
"use strict";
$('.contentHolder').perfectScrollbar();
});
</script>