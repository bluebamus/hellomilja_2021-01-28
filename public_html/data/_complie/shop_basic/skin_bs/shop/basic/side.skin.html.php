<?php /* Template_ 2.2.8 2016/08/26 11:34:32 /home/hellomilja.com/www/eyoom/theme/shop_basic/skin_bs/shop/basic/side.skin.html 000003146 */  $this->include_("eb_outlogin","eb_poll","eb_popular","eb_visit");?>
<?php if (!defined('_GNUBOARD_')) exit; // ?>
<div class="basic-body-side <?php if($TPL_VAR["eyoom"]["pos_side_layout"]=='left'){?>left<?php }else{?>right<?php }?>-side col-md-3">
<div class="margin-bottom-20">
<?php echo $TPL_VAR["latest"]->latest_eyoom('notice_balloon','bo_table=게시판id||count=5||cut_subject=50')?>
</div>
<?php if(!G5_IS_MOBILE){?>
<div class="margin-bottom-20">
<?php if($TPL_VAR["eyoom"]["use_gnu_outlogin"]=='y'){?><?php echo outlogin('basic')?><?php }else{?><?php echo eb_outlogin($TPL_VAR["eyoom"]["outlogin_skin"])?><?php }?>
</div>
<?php if(!defined('_INDEX_')){?>
<div class="margin-bottom-20">
<?php echo $TPL_VAR["latest"]->latest_item('item_carousel',"title=히트상품||count=6||cut_name=50||width=400||type=1")?>
</div>
<div class="margin-bottom-20">
<?php echo $TPL_VAR["latest"]->latest_item('item_carousel',"title=최신상품||count=6||cut_name=50||width=400||type=3")?>
</div>
<div class="margin-bottom-20">
<?php echo $TPL_VAR["latest"]->latest_item('item_carousel',"title=할인상품||count=6||cut_name=50||width=400||type=5")?>
</div>
<?php }?>
<div class="margin-bottom-20">
<div class="side-tab margin-bottom-20">
<div class="tab-e2">
<ul class="nav nav-tabs">
<li class="active"><a href="#side-tn-1" data-toggle="tab">새글</a></li>
<li class="last"><a href="#side-tn-2" data-toggle="tab">새댓글</a></li>
</ul>
<div class="tab-content">
<div class="tab-pane fade active in" id="side-tn-1">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_newpost('tab_newpost','count=5||cut_subject=30||photo=y')?>
</div>
</div>
<div class="tab-pane fade in" id="side-tn-2">
<div class="tab-content-wrap">
<?php echo $TPL_VAR["latest"]->latest_newpost('tab_newcomment','count=5||cut_subject=30||photo=y')?>
</div>
</div>
</div>
</div>
</div>
</div>
<?php if(defined('_INDEX_')){?>
<?php if( 1){?>
<div class="margin-bottom-20">
<?php if($TPL_VAR["eyoom"]["use_gnu_poll"]=='y'){?><?php echo poll('basic')?><?php }else{?><?php echo eb_poll($TPL_VAR["eyoom"]["poll_skin"])?><?php }?>
</div>
<?php }?>
<?php if( 1){?>
<div class="margin-bottom-20">
<?php echo $TPL_VAR["latest"]->latest_rankset('basic','10')?>
</div>
<?php }?>
<?php if( 1){?>
<div class="margin-bottom-20">
<?php if($TPL_VAR["eyoom"]["use_gnu_popular"]=='y'){?><?php echo popular('basic')?><?php }else{?><?php echo eb_popular($TPL_VAR["eyoom"]["popular_skin"])?><?php }?>
</div>
<?php }?>
<?php }?>
<?php if($GLOBALS["is_admin"]){?>
<div class="margin-bottom-20">
<?php if($TPL_VAR["eyoom"]["use_gnu_visit"]=='y'){?><?php echo visit('basic')?><?php }else{?><?php echo eb_visit($TPL_VAR["eyoom"]["visit_skin"])?><?php }?>
</div>
<?php }?>
<?php }?>
</div>
<?php if($TPL_VAR["is_side_sticky"]=='yes'){?>
<script type="text/javascript" src="/eyoom/theme/shop_basic/js/theia-sticky-sidebar.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
App.initSideSticky();
});
</script>
<?php }?>