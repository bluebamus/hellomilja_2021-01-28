<?php /* Template_ 2.2.8 2017/10/23 00:04:03 /home1/bluebamus1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/coupon.skin.html 000002343 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/skin_bs/shop/basic/shop_style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/css/custom.css" type="text/css" media="screen">',0);
?>
<div class="shop-coupon">
<div class="headline"><h6><strong><?php echo $TPL_VAR["g5"]["title"]?></strong></h6></div>
<?php if(G5_IS_MOBILE){?>
<p class="text-right font-size-11 margin-bottom-5 color-grey">Note! 좌우 스크롤 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>
<div class="table-list-eb">
<div class="table-responsive">
<table id="sod_list" class="table table-bordered">
<thead>
<tr>
<th>쿠폰명</th>
<th>적용대상</th>
<th>할인금액</th>
<th>사용기한</th>
</tr>
</thead>
<tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
<tr>
<td><?php echo $TPL_V1["cp_subject"]?></td>
<td><?php echo $TPL_V1["cp_target"]?></td>
<td><?php echo $TPL_V1["cp_price"]?></td>
<td><?php echo substr($TPL_V1["cp_start"], 0, 10)?> ~ <?php echo substr($TPL_V1["cp_end"], 0, 10)?></td>
</tr>
<?php }}else{?>
<tr><td colspan="4" class="text-center">사용할 수 있는 쿠폰이 없습니다.</td></tr>
<?php }?>
</tbody>
</table>
</div>
</div>
<div class="text-center"><button type="button" onclick="window.close();" class="btn-e btn-e-dark">창닫기</button></div>
</div>
<style>
.shop-coupon {padding:15px}
.shop-coupon .table-list-eb {color:#000}
.shop-coupon .table-list-eb .table tbody > tr > td {text-align:center}
</style>