<?php /* Template_ 2.2.8 2019/11/25 14:50:10 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/item_qa.skin.html 000004687 */ 
$TPL__item_qa_1=empty($GLOBALS["item_qa"])||!is_array($GLOBALS["item_qa"])?0:count($GLOBALS["item_qa"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<style>
#shop-product-qa {padding:10px 0}
#shop-product-qa .panel {border:0;border-top:1px solid #d5d5d5;box-shadow:none}
#shop-product-qa .panel:last-child {border-bottom:1px solid #d5d5d5}
#shop-product-qa .panel-group .panel+.panel {margin-top:0}
#shop-product-qa .panel-title {position:relative}
#shop-product-qa .panel-title a {color:#000;background:#fcfcfc}
#shop-product-qa .panel-title .title-subj {font-size:12px;margin-bottom:5px;padding-right:30px}
#shop-product-qa .panel-title .divide {color:#c5c5c5;margin-left:7px;margin-right:7px}
#shop-product-qa .panel-title .indicator {font-size:11px;width:26px;height:16px;line-height:15px;background:#474A5E;border:1px solid #2E3340;text-align:center;color:#fff;position:absolute;top:11px;right:0}
#shop-product-qa .panel-title .indicator.fa-angle-up {background:#FF2900;border:1px solid #DE2600}
#shop-product-qa .panel-heading a {padding:10px 0 8px}
#shop-product-qa .panel-body {padding:15px 0;border-top:1px dotted #d5d5d5}
#shop-product-qa .sit_qaa_done {color:#FF2900}
</style>
<section>
<div class="panel-group acc-v1" id="porduct-qa">
<?php if($TPL__item_qa_1){foreach($GLOBALS["item_qa"] as $TPL_K1=>$TPL_V1){?>
<div class="panel panel-default">
<div class="panel-heading">
<div class="panel-title">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#porduct-qa" href="#product_qa_<?php echo $TPL_K1?>">
<p class="title-subj"><strong><?php echo $GLOBALS["iq_num"]?>. <?php echo $TPL_V1["iq_subject"]?></strong></p>
<div class="pull-left">
<span class="font-size-12 color-grey">
<?php echo $TPL_V1["iq_name"]?><span class="divide">|</span><?php echo $TPL_V1["iq_time"]?>
</span>
</div>
<span class="list-unstyled font-size-12 pull-right">
상태: <strong class="<?php echo $TPL_V1["iq_style"]?>"><?php echo $TPL_V1["iq_stats"]?></strong>
</span>
<i class="indicator fa fa-angle-down pull-right"></i>
<div class="clearfix"></div>
</a>
</div>
</div>
<div id="product_qa_<?php echo $TPL_K1?>" class="panel-collapse collapse">
<div class="panel-body">
<p><strong class="color-black margin-right-5">문의</strong> <span class="color-black">-----</span> <span class="font-size-12"><?php echo $TPL_V1["iq_question"]?></span></p>
<div class="margin-hr-10"></div>
<p><strong class="color-red margin-right-5">답변</strong> <span class="color-red">-----</span> <span class="font-size-12 display-block margin-top-10"><?php if(!$TPL_V1["is_secret"]){?><?php echo $TPL_V1["iq_answer"]?><?php }else{?>비밀글로 보호된 답변입니다.<?php }?></span></p>
<?php if($GLOBALS["is_admin"]||($TPL_V1["mb_id"]==$TPL_VAR["member"]["mb_id"]&&!$TPL_V1["is_answer"])){?>
<div class="text-right">
<a href="<?php echo $TPL_V1["link_edit"]?>" class="btn-e btn-e-purple btn-e-xs itemqa_form" onclick="return false;">수정</a>
<a href="<?php echo $TPL_V1["link_del"]?>" class="btn-e btn-e-dark btn-e-xs">삭제</a>
</div>
<?php }?>
</div>
</div>
</div>
<?php }}else{?>
<p class="text-center">상품문의가 없습니다.</p>
<?php }?>
</div>
</section>
<?php echo $GLOBALS["paging_itemqa"]?>
<div class="text-right margin-top-15">
<a href="<?php echo $GLOBALS["itemqa_form"]?>" class="btn-e btn-e-purple" target="_blank"><i class="fa fa-pencil"></i> 상품문의 쓰기</a>
<a href="<?php echo $GLOBALS["itemqa_list"]?>" class="btn-e btn-e-dark"><i class="fa fa-search-plus"></i> 더보기</a>
</div>
<script src="/js/viewimageresize.js"></script>
<script>
function toggleAngle(e) {
$(e.target)
.prev('.panel-heading')
.find("i.indicator")
.toggleClass('fa-angle-up fa-angle-down');
}
$('#porduct-qa').on('hidden.bs.collapse', toggleAngle);
$('#porduct-qa').on('shown.bs.collapse', toggleAngle);
$(function(){
$(".itemqa_form").click(function(){
window.open(this.href, "itemqa_form", "width=810,height=680,scrollbars=1");
return false;
});
$(".itemqa_delete").click(function(){
return confirm("정말 삭제 하시겠습니까?\n\n삭제후에는 되돌릴수 없습니다.");
});
$(".sit_qa_li_title").click(function(){
var $con = $(this).siblings(".sit_qa_con");
if($con.is(":visible")) {
$con.slideUp();
} else {
$(".sit_qa_con:visible").hide();
$con.slideDown(
function() {
// 이미지 리사이즈
$con.viewimageresize2();
}
);
}
});
$(".qa_page").click(function(){
$("#itemqa").load($(this).attr("href"));
return false;
});
});
$(window).on("load", function() {
$(".panel-body").viewimageresize2();
});
</script>
</script>