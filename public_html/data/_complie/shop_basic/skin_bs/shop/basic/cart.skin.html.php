<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/cart.skin.html 000007507 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="shop-cart">
<form name="frmcartlist" id="sod_bsk_list" method="post" action="<?php echo $GLOBALS["cart_action_url"]?>" class="eyoom-form">
<?php if(G5_IS_MOBILE){?>
<p class="text-right font-size-11 margin-bottom-5 color-grey">Note! 좌우 스크롤 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>
<div class="table-list-eb margin-bottom-20">
<div class="table-responsive">
<table class="table table-bordered">
<thead>
<tr>
<th>상품이미지</th>
<th>상품명</th>
<th>총수량</th>
<th>판매가</th>
<th>소계</th>
<th>포인트</th>
<th>배송비</th>
<th>
<label for="ct_all" class="sound_only">상품 전체</label>
<label class="checkbox">
<input type="checkbox" name="ct_all" value="1" id="ct_all" checked="checked"><i></i>
</label>
</th>
</tr>
</thead>
<tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<tr>
<td class="sod-img"><a href="./item.php?it_id=<?php echo $TPL_V1["it_id"]?>"><?php echo $TPL_V1["image"]?></a></td>
<td>
<input type="hidden" name="it_id[<?php echo $TPL_K1?>]" value="<?php echo $TPL_V1["it_id"]?>">
<input type="hidden" name="it_name[<?php echo $TPL_K1?>]" value="<?php echo get_text($TPL_V1["row_it_name"])?>">
<div class="text-left"><a href="./item.php?it_id=<?php echo $TPL_V1["it_id"]?>"><strong><?php echo stripslashes($TPL_V1["it_name"])?></strong></a></div>
<?php if($TPL_V1["it_options"]){?>
<div class="sod-opt"><?php echo $TPL_V1["it_options"]?></div>
<div class="text-left"><button type="button" class="btn-e btn-e-dark btn-e-xs mod_options">선택사항수정</button></div>
<?php }?>
</td>
<td><?php echo number_format($TPL_V1["sum_qty"])?></td>
<td><?php echo number_format($TPL_V1["ct_price"])?></td>
<td><span id="sell_price_<?php echo $TPL_K1?>"><?php echo number_format($TPL_V1["sell_price"])?></span></td>
<td><?php echo number_format($TPL_V1["point"])?></td>
<td><?php echo $TPL_V1["ct_send_cost"]?></td>
<td>
<label for="ct_chk_<?php echo $TPL_K1?>" class="sound_only">상품</label>
<label class="checkbox">
<input type="checkbox" name="ct_chk[<?php echo $TPL_K1?>]" value="1" id="ct_chk_<?php echo $TPL_K1?>" checked="checked"><i></i>
</label>
</td>
</tr>
<?php }}?>
<?php if($GLOBALS["count"]== 0){?>
<tr><td colspan="8" class="text-center">장바구니에 담긴 상품이 없습니다.</td></tr>
<?php }?>
</tr>
</tbody>
</table>
</div>
</div>
<?php if($GLOBALS["tot_price"]> 0||$GLOBALS["send_cost"]> 0){?>
<div class="sod-bsk-tot">
<?php if($GLOBALS["send_cost"]> 0){?>
<p class="pull-left">배송비</p>
<p class="pull-right color-white"><strong><?php echo number_format($GLOBALS["send_cost"])?> 원</strong></p>
<div class="clearfix"></div>
<?php }?>
<?php if($GLOBALS["tot_price"]> 0){?>
<div class="sod-bsk-tot-in">
<span class="pull-left color-black"><strong>총계 가격 / 포인트</strong></span>
<span class="pull-right color-red-t"><strong><?php echo number_format($GLOBALS["tot_price"])?> 원 / <?php echo number_format($GLOBALS["tot_point"])?> 점</strong></span>
<div class="clearfix"></div>
</div>
<?php }?>
</div>
<?php }?>
<div class="text-center">
<?php if($GLOBALS["count"]== 0){?>
<a href="<?php echo G5_SHOP_URL?>/" class="btn-e btn-e-green">쇼핑 계속하기</a>
<?php }else{?>
<input type="hidden" name="url" value="./orderform.php">
<input type="hidden" name="records" value="<?php echo $GLOBALS["count"]?>">
<input type="hidden" name="act" value="">
<a href="<?php echo G5_SHOP_URL?>/list.php?ca_id=<?php echo $GLOBALS["continue_ca_id"]?>" class="btn-e btn-e-default">쇼핑 계속하기</a>
<button type="button" onclick="return form_check('buy');" class="btn-e btn-e-red">주문하기</button>
<button type="button" onclick="return form_check('seldelete');" class="btn-e btn-e-dark">선택삭제</button>
<button type="button" onclick="return form_check('alldelete');" class="btn-e btn-e-dark">비우기</button>
<?php if($GLOBALS["naverpay_button_js"]){?>
<div class="cart-naverpay"><?php echo $GLOBALS["naverpay_request_js"]?><?php echo $GLOBALS["naverpay_button_js"]?></div>
<?php }?>
<?php }?>
</div>
</form>
</div>
<style>
.shop-cart .margin-hrd-5 {height:1px;border-top:1px dotted #536999;margin:5px 0;clear:both}
.shop-cart .color-red-t {color:#ae0000}
.shop-cart .eyoom-form .radio i,.shop-cart .eyoom-form .checkbox i {top:2px}
.shop-cart .table-list-eb .sod-img {width:80px}
.shop-cart .table-list-eb td img {display:block;width:100%;max-width:100%;height:auto}
.shop-cart .sod-opt {color:#777;font-size:12px;padding:8px;text-align:left;display:block;border:dotted 1px #c5c5c5;background:#fbfbfb;margin-top:7px;margin-bottom:10px}
.shop-cart .sod-opt ul {margin:0;padding:0;list-style:none}
.shop-cart .sod-bsk-tot {background:#334773;border:1px solid #1F263B;color:#fff;padding:10px;margin-bottom:30px}
.shop-cart .sod-bsk-tot p {padding:0 10px;margin-bottom:5px;color:#c5cced}
.shop-cart .sod-bsk-tot-in {border:1px solid #1F263B;background:#e1e5fb;padding:8px 10px}
.shop-cart .table-bordered>thead>tr>th, .shop-cart .table-bordered>tbody>tr>th, .shop-cart .table-bordered>tfoot>tr>th, .shop-cart .table-bordered>thead>tr>td, .shop-cart .table-bordered>tbody>tr>td, .shop-cart .table-bordered>tfoot>tr>td {border:1px solid #bacdf8}
.shop-cart .table-list-eb {color:#000}
.shop-cart .table-list-eb .table tbody > tr > td {text-align:center}
.shop-cart .table-list-eb .table tbody > tr > th {background:#e7efff}
.shop-cart .table-list-eb thead {border-top:1px solid #bacdf8;background:#e7efff}
.shop-cart .table-list-eb .table tbody > tr > td {border-top:1px solid #bacdf8}
</style>
<script src="/js/shop.js"></script>
<script>
$(function() {
var close_btn_idx;
// 선택사항수정
$(".mod_options").click(function() {
var it_id = $(this).closest("tr").find("input[name^=it_id]").val();
var $this = $(this);
close_btn_idx = $(".mod_options").index($(this));
$.post(
"./cartoption.php",
{ it_id: it_id },
function(data) {
$("#mod_option_frm").remove();
$this.after("<div id=\"mod_option_frm\"></div>");
$("#mod_option_frm").html(data);
price_calculate();
}
);
});
// 모두선택
$("input[name=ct_all]").click(function() {
if($(this).is(":checked"))
$("input[name^=ct_chk]").attr("checked", true);
else
$("input[name^=ct_chk]").attr("checked", false);
});
// 옵션수정 닫기
$(document).on("click", "#mod_option_close", function() {
$("#mod_option_frm").remove();
$(".mod_options").eq(close_btn_idx).focus();
});
$("#win_mask").click(function () {
$("#mod_option_frm").remove();
$(".mod_options").eq(close_btn_idx).focus();
});
});
function fsubmit_check(f) {
if($("input[name^=ct_chk]:checked").size() < 1) {
alert("구매하실 상품을 하나이상 선택해 주십시오.");
return false;
}
return true;
}
function form_check(act) {
var f = document.frmcartlist;
var cnt = f.records.value;
if (act == "buy")
{
if($("input[name^=ct_chk]:checked").size() < 1) {
alert("주문하실 상품을 하나이상 선택해 주십시오.");
return false;
}
f.act.value = act;
f.submit();
}
else if (act == "alldelete")
{
f.act.value = act;
f.submit();
}
else if (act == "seldelete")
{
if($("input[name^=ct_chk]:checked").size() < 1) {
alert("삭제하실 상품을 하나이상 선택해 주십시오.");
return false;
}
f.act.value = act;
f.submit();
}
return true;
}
</script>