<?php /* Template_ 2.2.8 2019/11/25 14:50:10 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/list.skin.html 000004201 */  $this->include_("eb_paging");?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="shop-list">
<div class="shop-list-navi">
<?php if( 0){?>
<div class="pull-left list-navi">
<a href="<?php echo G5_SHOP_URL?>/">Home</a>
<?php echo $TPL_VAR["shop"]->get_navigation()?>
</div>
<?php }?>
<?php if($GLOBALS["is_admin"]){?>
<div class="pull-right">
<a href="<?php echo G5_ADMIN_URL?>/shop_admin/categoryform.php?w=u&amp;ca_id=<?php echo $GLOBALS["ca_id"]?>" class="btn-e btn-e-purple">분류 관리</a>
</div>
<?php }?>
<div class="clearfix"></div>
</div>
<div id="sct_hhtml" class="margin-bottom-20"><?php echo conv_content($TPL_VAR["ca"]["ca_head_html"], 1)?></div>
<?php if($TPL_VAR["shop"]->listcategory()){?>
<aside class="sct-ct margin-bottom-20">
<h5><strong>관련 분류</strong></h5>
<ul class="list-unstyled">
<?php if(is_array($TPL_R1=$TPL_VAR["shop"]->listcategory())&&!empty($TPL_R1)){foreach($TPL_R1 as $TPL_V1){?>
<li><a href="<?php echo $TPL_V1["href"]?>"><span class="label label-dark"><?php echo $TPL_V1["ca_name"]?> (<?php echo $TPL_V1["cnt"]?>)</span></a></li>
<?php }}?>
</ul>
<div class="clearfix"></div>
</aside>
<?php }?>
<section class="tab-e2 margin-bottom-20">
<ul class="nav nav-tabs">
<li <?php if($_GET["sort"]=='it_update_time'&&$_GET["sortodr"]=='desc'){?>class="active"<?php }elseif(!$_GET["sort"]){?>class="active"<?php }?>>
<a href="<?php echo $GLOBALS["sct_sort_href"]?>it_update_time&amp;sortodr=desc">최근등록순</a>
</li>
<li <?php if($_GET["sort"]=='it_sum_qty'&&$_GET["sortodr"]=='desc'){?>class="active"<?php }?>>
<a href="<?php echo $GLOBALS["sct_sort_href"]?>it_sum_qty&amp;sortodr=desc">판매많은순</a>
</li>
<li <?php if($_GET["sort"]=='it_price'&&$_GET["sortodr"]=='asc'){?>class="active"<?php }?>>
<a href="<?php echo $GLOBALS["sct_sort_href"]?>it_price&amp;sortodr=asc">낮은가격순</a>
</li>
<li <?php if($_GET["sort"]=='it_price'&&$_GET["sortodr"]=='desc'){?>class="active"<?php }?>>
<a href="<?php echo $GLOBALS["sct_sort_href"]?>it_price&amp;sortodr=desc">높은가격순</a>
</li>
<li <?php if($_GET["sort"]=='it_use_avg'&&$_GET["sortodr"]=='desc'){?>class="active"<?php }?>>
<a href="<?php echo $GLOBALS["sct_sort_href"]?>it_use_avg&amp;sortodr=desc">평점높은순</a>
</li>
<li <?php if($_GET["sort"]=='it_use_cnt'&&$_GET["sortodr"]=='desc'){?>class="active"<?php }?>>
<a href="<?php echo $GLOBALS["sct_sort_href"]?>it_use_cnt&amp;sortodr=desc">후기많은순</a>
</li>
</ul>
<div class="tab-bottom-line"></div>
</section>
<?php echo $GLOBALS["list_package"]?>
<?php echo eb_paging('basic')?>
<div id="sct_thtml"><?php echo conv_content($TPL_VAR["ca"]["ca_tail_html"], 1)?></div>
</div>
<style>
.shop-list .shop-list-navi {margin-bottom:20px}
.shop-list .sct_here {color:crimson}
.shop-list .sct_here:before {content:"\f105";font-family:FontAwesome;padding:0 5px;color:#888}
.shop-list .sct_bg:before {content:"\f105";font-family:FontAwesome;padding:0 5px;color:#888}
.shop-list .sct-ct {border:1px solid #d5d5d5;border-left:1px solid #FF2900;background:#fafafa;padding:10px}
.shop-list .sct-ct h5 {font-size:14px;font-weight:bold;padding-bottom:5px;margin-top:0;border-bottom:1px dotted #d5d5d5;color:#DE2600}
.shop-list .sct-ct li {float:left;padding:2px}
.shop-list .tab-e2 .nav-tabs li a {background:#f5f5f5}
.shop-list .tab-e2 .nav-tabs li a:hover {background:#e5e5e5;color:#000}
.shop-list .tab-e2 .nav-tabs li.active a {border:1px solid #000;border-top:1px solid #FF2900;background:#fff;z-index:1}
.shop-list .tab-e2 .tab-bottom-line {position:relative;height:1px;background:#000}
@media (max-width: 767px){
.shop-list .tab-e2 .nav-tabs {border:1px solid #b5b5b5;padding:10px 10px 5px;background:#fafafa}
.shop-list .tab-e2 .nav-tabs li a {background:#e5e5e5;color:#000;font-size:11px;padding:3px 7px;margin-bottom:5px}
.shop-list .tab-e2 .nav-tabs li.active a {background:#1C1C26;color:#fff;border:0}
.shop-list .tab-e2 .tab-bottom-line {display:none}
}
</style>
<script>
var itemlist_ca_id = "<?php echo $GLOBALS["ca_id"]?>";
</script>
<script src="/js/shop.list.js"></script>