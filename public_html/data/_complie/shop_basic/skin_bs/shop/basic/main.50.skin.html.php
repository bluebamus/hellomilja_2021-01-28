<?php /* Template_ 2.2.8 2017/04/05 21:55:07 /home/hellomilja.com/www/eyoom/theme/shop_basic/skin_bs/shop/basic/main.50.skin.html 000003838 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; // ?>
<div class="product-main-50">
<ul class="list-unstyled clearfix">
<?php if($TPL_list_1){$TPL_I1=-1;foreach($TPL_VAR["list"] as $TPL_V1){$TPL_I1++;?>
<li>
<div class="product-main-50-img">
<?php if($TPL_VAR["href"]){?>
<a href="<?php echo $TPL_V1["href"]?>">
<?php }?>
<?php echo $TPL_V1["it_image"]?>
<?php if($TPL_VAR["href"]){?>
</a>
<?php }?>
<div class="product-main-50-content">
<?php if($TPL_VAR["href"]){?>
<h3>
<a href="<?php echo $TPL_V1["href"]?>">
<?php }?>
<?php if($TPL_VAR["view_it_name"]){?><?php echo stripslashes($TPL_V1["it_name"])?><?php }?>
<?php if($TPL_VAR["href"]){?>
</a>
</h3>
<?php }?>
<?php if( 0){?>
<?php if($TPL_VAR["view_it_basic"]&&$TPL_V1["it_basic"]){?>
<p><?php echo stripslashes($TPL_V1["it_basic"])?></p>
<?php }?>
<?php }?>
<?php if($TPL_VAR["view_it_cust_price"]||$TPL_VAR["view_it_price"]){?>
<div class="product-main-50-price">
<?php if($TPL_VAR["view_it_price"]){?>
<span class="e3-price">₩ <?php echo $TPL_V1["it_tel_inq"]?></span>
<?php }?>
<?php if($TPL_VAR["view_it_cust_price"]&&$TPL_V1["it_cust_price"]){?>
<span class="e3-price line-through">₩ <?php echo $TPL_V1["it_cust_price"]?></span>
<?php }?>
</div>
<?php }?>
</div>
</div>
</li>
<?php if($TPL_I1== 4){?>
<div class="clearfix visible-lg"></div>
<?php }?>
<?php }}else{?>
<li class="no-item">
<p class="text-center">등록된 상품이 없습니다.</p>
</li>
<?php }?>
</ul>
</div>
<style>
.product-main-50 ul {margin:0 -3px}
.product-main-50 > ul > li {width:50%;float:left;padding:0 3px;margin-bottom:10px}
.product-main-50 > ul > li:last-child {display:none}
.product-main-50-img {text-align:center;position:relative;overflow:hidden}
.product-main-50-img a img {max-width:100%;display:inline-block;height:auto}
.product-main-50-icons ul li a {color:#ccc}
.product-main-50-icons ul li a:hover {color:#fff}
.product-main-50-content {padding:0 5px;background:rgba(256,256,256,0.9)}
.product-main-50-content h3 {text-align:center;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;font-weight:bold}
.product-main-50-content h3 a {display:inline-block;border-bottom:1px solid #ae0000;font-size:14px}
.product-main-50-content p {margin:0;text-align:center;font-size:12px;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden}
.product-main-50-price {text-align:center}
.product-main-50-price span {color:#aaa;font-size:12px;margin:0 3px}
.product-main-50-price span:first-child {font-weight:bold;color:#ae0000;font-size:14px}
.product-main-50 .no-item {width:100% !important}
.product-main-50 .no-item p{text-align:center;padding:10px 0;border:1px solid #ddd}
@media (min-width:768px){
.product-main-50 > ul {margin:0 -5px}
.product-main-50 > ul > li {padding:0 5px}
.product-main-50-content {position:absolute;bottom:-95px;width:100%;height:95px;-webkit-transition:all 0.3s ease-in-out;-moz-transition:all 0.3s ease-in-out;transition:all 0.3s ease-in-out}
.product-main-50 > ul > li:hover .product-main-50-content {bottom:0}
}
@media (min-width: 1200px){
.product-main-50 > ul {margin:0 0 20px}
.product-main-50 > ul > li {width:25%;float:left;padding:0;margin:0;border-style:solid;border-color:#fff;border-width:1px}
.product-main-50 > ul > li:last-child {display:block}
.product-main-50-content {height:75px}
.product-main-50-content p {display:none}
.product-main-50 > ul > li:first-child {width:50%}
.product-main-50 > ul > li:first-child .product-main-50-content {height:75px}
.product-main-50 > ul > li:first-child .product-main-50-content p {display:block}
}
</style>