<?php /* Template_ 2.2.8 2019/11/25 14:50:11 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/shop/basic/index.skin.html 000008958 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가 ?>
<script src="https://snapwidget.com/js/snapwidget.js"></script>
<div id="main_banner">
<?php echo eyoom_display_banner('메인','mainbanner.10.skin.php')?>
</div>
<!--<div class="main-banner-slider margin-bottom-30 hidden-xs">
<div class="owl-navi">
<a class="owl-btn prev-main-banner"><i class="fa fa-angle-left"></i></a>
<a class="owl-btn next-main-banner"><i class="fa fa-angle-right"></i></a>
</div>
<div class="owl-slider-main-banner">
<article class="item">
&lt;!&ndash;&ndash;&gt;
<a href="#">
<img src="/eyoom/theme/shop_basic/image/banner_slider/banner_slider_1.jpg">
<div class="banner-slider-text">
<h1><?php echo $TPL_VAR["config"]["cf_title"]?> OPEN!</h1>
<p>Lorem ipsum dulor sit amet, consectetur sequi ex quam sunt delectus adipisicing elft.<br>Veniam aut tempore illum, dulor nemo quae nulla.</p>
</div>
</a>
&lt;!&ndash;&ndash;&gt;
</article>
<article class="item">
&lt;!&ndash;&ndash;&gt;
<a href="#">
<img src="/eyoom/theme/shop_basic/image/banner_slider/banner_slider_2.jpg">
<div class="banner-slider-text">
<h1><?php echo $TPL_VAR["config"]["cf_title"]?> EVENT!</h1>
<p>Lorem ipsum dulor sit amet, consectetur sequi ex quam sunt delectus adipisicing elft.<br>Veniam aut tempore illum, dulor nemo quae nulla.</p>
</div>
</a>
&lt;!&ndash;&ndash;&gt;
</article>
</div>
</div>-->
<?php if($TPL_VAR["default"]["de_type3_list_use"]){?>
<section class="margin-bottom-30">
<div class="headline">
<!--<h5><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=3"><strong>최신상품</strong></a></h5>-->
<a href="<?php echo G5_SHOP_URL?>/listtype.php?type=3"><img src="/eyoom/theme/shop_basic/image/main_split_line/new.png"></a>
<!--<a href="<?php echo G5_SHOP_URL?>/listtype.php?type=3" style="float:right;"> more&nbsp;&nbsp; <i class="fa  fa-plus-square-o"></i></a>-->
</div>
<?php echo $TPL_VAR["new_goods"]->set_view('it_id',false)?>
<?php echo $TPL_VAR["new_goods"]->set_view('it_name',true)?>
<?php echo $TPL_VAR["new_goods"]->set_view('it_basic',true)?>
<?php echo $TPL_VAR["new_goods"]->set_view('it_cust_price',true)?>
<?php echo $TPL_VAR["new_goods"]->set_view('it_price',true)?>
<?php echo $TPL_VAR["new_goods"]->set_view('it_icon',true)?>
<?php echo $TPL_VAR["new_goods"]->set_view('sns',true)?>
<?php echo $TPL_VAR["new_goods"]->run()?>
</section>
<?php }?>
<div class="main-banner-slider1 margin-bottom-30 hidden-xs">
<div class="owl-navi">
<a class="owl-btn prev-main-banner"><i class="fa fa-angle-left"></i></a>
<a class="owl-btn next-main-banner"><i class="fa fa-angle-right"></i></a>
</div>
<div class="owl-slider-main-banner">
<article class="item">
<img src="/eyoom/theme/shop_basic/image/main_banner/2-1.png">
<!--<a href="#">
<img src="/eyoom/theme/shop_basic/image/main_banner/2-1.png">
</a>-->
</article>
<article class="item">
<img src="/eyoom/theme/shop_basic/image/main_banner/2-2.png">
<!--<a href="#">
<img src="/eyoom/theme/shop_basic/image/main_banner/2-2.png">
</a>-->
</article>
</div>
</div>
<?php if($TPL_VAR["default"]["de_type4_list_use"]){?>
<section class="margin-bottom-10">
<div class="headline" >
<!--<h5><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=4"><strong>인기상품</strong></a></h5>-->
<a href="<?php echo G5_SHOP_URL?>/listtype.php?type=4"><img src="/eyoom/theme/shop_basic/image/main_split_line/best.png"></a>
<!--<a href="<?php echo G5_SHOP_URL?>/listtype.php?type=4" style="float:right;"> more&nbsp;&nbsp; <i class="fa  fa-plus-square-o"></i></a>-->
</div>
<?php echo $TPL_VAR["popular_goods"]->set_view('it_id',false)?>
<?php echo $TPL_VAR["popular_goods"]->set_view('it_name',true)?>
<?php echo $TPL_VAR["popular_goods"]->set_view('it_basic',true)?>
<?php echo $TPL_VAR["popular_goods"]->set_view('it_cust_price',true)?>
<?php echo $TPL_VAR["popular_goods"]->set_view('it_price',true)?>
<?php echo $TPL_VAR["popular_goods"]->set_view('it_icon',true)?>
<?php echo $TPL_VAR["popular_goods"]->set_view('sns',true)?>
<?php echo $TPL_VAR["popular_goods"]->run()?>
</section>
<?php }?>
<div class="main-banner-slider2 margin-bottom-30 hidden-xs">
<div class="owl-navi">
<a class="owl-btn prev-main-banner"><i class="fa fa-angle-left"></i></a>
<a class="owl-btn next-main-banner"><i class="fa fa-angle-right"></i></a>
</div>
<div class="owl-slider-main-banner">
<article class="item">
<img src="/eyoom/theme/shop_basic/image/main_banner/3-1.png">
<!--<a href="#">
<img src="/eyoom/theme/shop_basic/image/main_banner/3-1.png">
</a>-->
</article>
<article class="item">
<img src="/eyoom/theme/shop_basic/image/main_banner/3-2.png">
<!--<a href="#">
<img src="/eyoom/theme/shop_basic/image/main_banner/3-2.png">
</a>-->
</article>
</div>
</div>
<?php if($TPL_VAR["default"]["de_type2_list_use"]){?>
<section class="margin-bottom-10">
<div class="headline">
<!--<h5><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=2"><strong>추천상품</strong></a></h5>-->
<a href="<?php echo G5_SHOP_URL?>/listtype.php?type=2"><img src="/eyoom/theme/shop_basic/image/main_split_line/hot.png"></a>
<!--<a href="<?php echo G5_SHOP_URL?>/listtype.php?type=2" style="float:right;"> more&nbsp;&nbsp; <i class="fa  fa-plus-square-o"></i></a>-->
</div>
<?php echo $TPL_VAR["recommend_goods"]->set_view('it_id',false)?>
<?php echo $TPL_VAR["recommend_goods"]->set_view('it_name',true)?>
<?php echo $TPL_VAR["recommend_goods"]->set_view('it_basic',true)?>
<?php echo $TPL_VAR["recommend_goods"]->set_view('it_cust_price',true)?>
<?php echo $TPL_VAR["recommend_goods"]->set_view('it_price',true)?>
<?php echo $TPL_VAR["recommend_goods"]->set_view('it_icon',true)?>
<?php echo $TPL_VAR["recommend_goods"]->set_view('sns',true)?>
<?php echo $TPL_VAR["recommend_goods"]->run()?>
</section>
<?php }?>
<?php if($TPL_VAR["default"]["de_type1_list_use"]){?>
<section class="margin-bottom-10">
<div class="headline">
<h5><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=1"><strong>히트상품</strong></a></h5>
<a href="<?php echo G5_SHOP_URL?>/listtype.php?type=1" style="float:right;"> more&nbsp;&nbsp; <i class="fa  fa-plus-square-o"></i></a>
</div>
<?php echo $TPL_VAR["hit_goods"]->set_view('it_img',true)?>
<?php echo $TPL_VAR["hit_goods"]->set_view('it_id',false)?>
<?php echo $TPL_VAR["hit_goods"]->set_view('it_name',true)?>
<?php echo $TPL_VAR["hit_goods"]->set_view('it_basic',true)?>
<?php echo $TPL_VAR["hit_goods"]->set_view('it_cust_price',true)?>
<?php echo $TPL_VAR["hit_goods"]->set_view('it_price',true)?>
<?php echo $TPL_VAR["hit_goods"]->set_view('it_icon',true)?>
<?php echo $TPL_VAR["hit_goods"]->set_view('sns',true)?>
<?php echo $TPL_VAR["hit_goods"]->run()?>
</section>
<?php }?>
<?php if( 0){?>
<!--<div class="shop-main-banner hidden-xs margin-bottom-30">
<div class="row">
<div class="col-xs-4">
<a href="#">
<div class="banner-img">
<img src="/eyoom/theme/shop_basic/skin_bs/shop/basic/img/banner_sample.jpg">
<h3>SHOES<small>SALE EVENT</small></h3>
</div>
</a>
</div>
<div class="col-xs-4">
<a href="#">
<div class="banner-text">
<h3>CLOTH<small>SALE EVENT</small></h3>
<i class="fa fa-arrow-right"></i>
</div>
</a>
</div>
<div class="col-xs-4">
<a href="#">
<div class="banner-text">
<h3>WATCH<small>SALE EVENT</small></h3>
<i class="fa fa-arrow-right"></i>
</div>
</a>
</div>
</div>
</div>-->
<?php }?>
<?php if($TPL_VAR["default"]["de_type5_list_use"]){?>
<section class="margin-bottom-10">
<div class="headline">
<h5><a href="<?php echo G5_SHOP_URL?>/listtype.php?type=5"><strong>할인상품</strong></a></h5>
<a href="<?php echo G5_SHOP_URL?>/listtype.php?type=5" style="float:right;"> more&nbsp;&nbsp; <i class="fa  fa-plus-square-o"></i></a>
</div>
<?php echo $TPL_VAR["sale_goods"]->set_view('it_id',false)?>
<?php echo $TPL_VAR["sale_goods"]->set_view('it_name',true)?>
<?php echo $TPL_VAR["sale_goods"]->set_view('it_basic',true)?>
<?php echo $TPL_VAR["sale_goods"]->set_view('it_cust_price',true)?>
<?php echo $TPL_VAR["sale_goods"]->set_view('it_price',true)?>
<?php echo $TPL_VAR["sale_goods"]->set_view('it_icon',true)?>
<?php echo $TPL_VAR["sale_goods"]->set_view('sns',true)?>
<?php echo $TPL_VAR["sale_goods"]->run()?>
</section>
<?php }?>
<section class="margin-bottom-10">
<div class="headline" style="text-align: center;">
<img src="/eyoom/theme/shop_basic/image/main_split_line/insta.png">
</div>
<iframe src="https://snapwidget.com/embed/361386" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
</section>
<!--
<section class="margin-bottom-10">
<div class="headline">
<h5><a href="<?php echo G5_SHOP_URL?>/itemuselist.php"><strong>최신 후기</strong></a></h5>
</div>
<?php echo $TPL_VAR["latest"]->latest_eyoom('blog','title=||bo_table=blog||count=3||cut_subject=50||img_view=y||img_width=200||content=y||cut_content=50')?>
</section>-->