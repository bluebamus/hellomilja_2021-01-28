<?php /* Template_ 2.2.8 2016/06/13 13:34:24 /home/hellomilja.com/www/eyoom/theme/shop_basic/skin_bs/latest/item_slider/latest.skin.html 000005779 */
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit; // ?>
<div class="headline">
<h6><strong><?php echo $TPL_VAR["title"]?></strong></h6>
</div>
<div class="product-community-main">
<div class="shop-owl-navi">
<a class="owl-btn prev-s"><i class="fa fa-angle-left"></i></a>
<a class="owl-btn next-s"><i class="fa fa-angle-right"></i></a>
</div>
<div class="owl-slider-wrap">
<div class="owl-slider-shop owl-carousel">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
<div class="item-main-40">
<div class="product-img">
<a href="<?php echo $TPL_V1["href"]?>"><?php echo $TPL_V1["img"]?></a>
</div>
<div class="product-description">
<div class="product-description-in">
<h4 class="product-name ellipsis">
<a href="<?php echo $TPL_V1["href"]?>"><?php echo $TPL_V1["it_name"]?></a>
</h4>
<div class="product-price">
<span class="title-price">₩ <?php echo number_format($TPL_V1["it_price"])?></span>
<?php if($TPL_V1["it_cust_price"]){?>
<span class="title-price line-through">₩ <?php echo number_format($TPL_V1["it_cust_price"])?></span>
<?php }?>
</div>
</div>
</div>
<div class="product-description-bottom">
<a class="pull-left font-size-12" href="<?php echo G5_SHOP_URL?>/itemuselist.php?sfl=a.it_id&stx=<?php echo $TPL_V1["it_id"]?>">리뷰보기</a>
<ul class="list-inline product-ratings pull-right">
<li><i class="rating<?php if($TPL_V1["star_score"]> 0){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star_score"]> 1){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star_score"]> 2){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star_score"]> 3){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
<li><i class="rating<?php if($TPL_V1["star_score"]> 4){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
</ul>
<div class="clearfix"></div>
</div>
</div>
<?php }}else{?>
<p class="text-center">등록된 상품이 없습니다.</p>
<?php }?>
</div>
</div>
</div>
<style>
.product-community-main {position:relative;overflow:hidden}
.product-community-main .shop-owl-navi a.owl-btn {color:#fff;width:24px;height:100px;font-size:16px;cursor:pointer;line-height:100px;text-align:center;display:inline-block;opacity:0.6;background:#171C29;visibility:hidden}
.product-community-main .shop-owl-navi a.owl-btn:hover {color:#fff;opacity:0.8;background:#FF2900;-webkit-transition:all 0.2s ease-in-out;-moz-transition:all 0.2s ease-in-out;-o-transition:all 0.2s ease-in-out;transition:all 0.2s ease-in-out}
.product-community-main .shop-owl-navi a.owl-btn.prev-s {position:absolute;top:50%;left:0;z-index:1;margin-top:-50px}
.product-community-main .shop-owl-navi a.owl-btn.next-s {position:absolute;top:50%;right:0;z-index:1;margin-top:-50px}
.product-community-main:hover .shop-owl-navi a.owl-btn {visibility:visible}
.product-community-main .owl-slider-wrap {position:relative;margin-left:-7px;margin-right:-7px}
.product-community-main .item-main-40 {position:relative;border:1px solid #d5d5d5;-webkit-transition:all 0.2s ease-in-out;-moz-transition:all 0.2s ease-in-out;-o-transition:all 0.2s ease-in-out;transition:all 0.2s ease-in-out;margin:0 7px}
.product-community-main .item-e1:hover {border:1px solid #0085DA}
.product-community-main .product-img {position:relative;padding:10px}
.product-community-main .item-e1:hover .product-img {background:#f8f8f8}
.product-community-main .product-img img {width:100%;display:block;max-width:100%;height:auto}
.product-community-main .product-sns {left:0;right:0;top:50%;z-index:1;width:100%;color:#555;border:none;padding:7px 0;font-size:12px;margin-top:-20px;text-align:center;position:absolute;visibility:hidden;text-transform:uppercase;background:#fff;-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=95)";filter:alpha(opacity=95);opacity:0.95}
.product-community-main .item-e1:hover .product-description-in {background:#f8f8f8}
.product-community-main .product-description .product-description-in {position:relative;overflow:hidden;padding:0 10px 10px}
.product-community-main .product-description .product-name {margin:0;margin-bottom:5px}
.product-community-main .product-description .product-name a {font-size:14px;font-weight:bold;color:#000}
.product-community-main .product-description .product-name a:hover {color:#DE2600}
.product-community-main .product-description .title-price {font-size:14px;font-weight:bold;color:#ae0000}
.product-community-main .product-description .line-through {color:#b5b5b5;text-decoration:line-through;margin-left:7px;font-weight:normal}
.product-community-main .product-description .type {color:#757575;display:block;font-size:12px;margin-top:8px}
.product-community-main .product-description-bottom {position:relative;overflow:hidden;padding:7px 10px;border-top:1px solid #d5d5d5}
.product-community-main .product-ratings {margin:0;padding:0}
.product-community-main .product-ratings li {padding:0;margin-right:-3px}
.product-community-main .product-ratings li .rating {color:#959595;line-height:normal;font-size:14px}
.product-community-main .product-ratings li .rating-selected {color:#FF2900;font-size:14px}
.product-community-main .product-ratings li.ratings-average {margin-left:8px;color:#757575;font-size:13px}
</style>
<?php if(G5_IS_MOBILE){?>
<style>
.product-community-main .owl-slider-wrap {margin-left:-5px;margin-right:-5px}
.product-community-main .item-main-40 {margin:0 5px 20px}
</style>
<?php }?>