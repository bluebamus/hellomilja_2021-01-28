<?php /* Template_ 2.2.8 2019/11/25 14:50:12 /home1/hellomilja1/public_html/eyoom/theme/shop_basic/skin_bs/latest/blog/latest.skin.html 000003504 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/shop_basic/skin_bs/latest/blog/style.css" type="text/css" media="screen">',0);
?>
<div class="blog-headline">
<h4><?php echo $TPL_VAR["title"]?></h4>
</div>
<div class="latest-blog row">
<?php if($TPL_loop_1){$TPL_I1=-1;foreach($TPL_VAR["loop"] as $TPL_V1){$TPL_I1++;?>
<div class="col-sm-4">
<div class="blog-box">
<div class="blog-img">
<?php if($TPL_V1["image"]){?>
<a href="<?php echo $TPL_V1["href"]?>"><img class="img-responsive" src="<?php echo $TPL_V1["image"]?>" alt=""></a>
<?php }else{?>
<a href="<?php echo $TPL_V1["href"]?>"><img class="img-responsive" src="/eyoom/theme/shop_basic/image/sample/no_image.jpg" alt=""></a>
<?php }?>
</div>
<div class="blog-content">
<h5><a href="<?php echo $TPL_V1["href"]?>"><?php echo $TPL_V1["wr_subject"]?></a></h5>
<ul class="list-inline">
<li><i class="fa fa-user"></i> <?php echo $TPL_V1["mb_nick"]?></li>
<li><i class="fa fa-clock-o"></i> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d H:i',$TPL_V1["datetime"])?></li>
<li><span class="color-red"><?php if($TPL_V1["wr_comment"]){?><i class="fa fa-comments"> <?php echo $TPL_V1["wr_comment"]?></i><?php }?></span></li>
<div class="clearfix"></div>
</ul>
<?php if($TPL_VAR["content"]=='y'){?>
<p><?php echo $TPL_V1["wr_content"]?></p>
<?php }?>
</div>
</div>
<?php if($TPL_I1> 0&&$TPL_I1%$TPL_VAR["cols"]== 0){?>
<div class="clearfix"></div>
<?php }?>
</div>
<?php }}else{?>
<div class="col-sm-4">
<div class="blog-box">
<div class="blog-img">
<img class="img-responsive" src="/eyoom/theme/shop_basic/image/sample/sample_02.jpg" alt="">
</div>
<div class="blog-content">
<h5><a href="#">Curabitur sem urna, consequat ve</a></h5>
<ul class="list-inline">
<li><i class="fa fa-user"></i> eyoom</li>
<li><i class="fa fa-clock-o"></i> 5일전</li>
<li><span class="color-red"><i class="fa fa-comments"></i> 3</span></li>
<div class="clearfix"></div>
</ul>
<p>Duis enim nulla, luctus eu, dapibus lacinia, venenatis id, quam.</p>
</div>
</div>
</div>
<div class="col-sm-4">
<div class="blog-box">
<div class="blog-img">
<img class="img-responsive" src="/eyoom/theme/shop_basic/image/sample/sample_01.jpg" alt="">
</div>
<div class="blog-content">
<h5><a href="#">Curabitur sem urna, consequat ve</a></h5>
<ul class="list-inline">
<li><i class="fa fa-user"></i> eyoom</li>
<li><i class="fa fa-clock-o"></i> 5일전</li>
<li><span class="color-red"><i class="fa fa-comments"></i> 3</span></li>
<div class="clearfix"></div>
</ul>
<p>Duis enim nulla, luctus eu, dapibus lacinia, venenatis id, quam.</p>
</div>
</div>
</div>
<div class="col-sm-4">
<div class="blog-box">
<div class="blog-img">
<img class="img-responsive" src="/eyoom/theme/shop_basic/image/sample/sample_03.jpg" alt="">
</div>
<div class="blog-content">
<h5><a href="#">Curabitur sem urna, consequat ve</a></h5>
<ul class="list-inline">
<li><i class="fa fa-user"></i> eyoom</li>
<li><i class="fa fa-clock-o"></i> 5일전</li>
<li><span class="color-red"><i class="fa fa-comments"></i> 3</span></li>
<div class="clearfix"></div>
</ul>
<p>Duis enim nulla, luctus eu, dapibus lacinia, venenatis id, quam.</p>
</div>
</div>
</div>
<?php }?>
</div>
<div style="border-bottom: 1px solid #333;margin-top:5px;"></div>