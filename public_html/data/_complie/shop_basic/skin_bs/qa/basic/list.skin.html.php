<?php /* Template_ 2.2.8 2016/10/27 23:04:53 /home/hellomilja.com/www/eyoom/theme/shop_basic/skin_bs/qa/basic/list.skin.html 000007898 */  $this->include_("eb_paging");
$TPL__category_tab_1=empty($GLOBALS["category_tab"])||!is_array($GLOBALS["category_tab"])?0:count($GLOBALS["category_tab"]);
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>
<div class="board-list">
<!-- 게시판 페이지 정보 및 버튼 시작 -->
<div class="board-info margin-bottom-10">
<div class="pull-left margin-top-5 font-size-12 color-grey">
<span>Total <?php echo number_format($GLOBALS["total_count"])?>건</span> <?php echo $GLOBALS["page"]?> 페이지
</div>
<?php if($GLOBALS["admin_href"]||$TPL_VAR["write_href"]){?>
<div class="pull-right">
<?php if($GLOBALS["admin_href"]){?><a href="<?php echo $GLOBALS["admin_href"]?>" class="btn-e btn-e-dark margin-right-5" type="button">관리자</a><?php }?>
<?php if($GLOBALS["write_href"]){?><a href="<?php echo $GLOBALS["write_href"]?>" class="btn-e btn-e-red" type="button">문의등록</a><?php }?>
</div>
<?php }?>
<div class="clearfix"></div>
</div>
<!-- 게시판 페이지 정보 및 버튼 끝 -->
<div class="tab-e2">
<?php if($GLOBALS["category_option"]){?>
<ul class="nav nav-tabs">
<li <?php if($GLOBALS["sca"]==''){?>class="active"<?php }?>><a href="<?php echo $GLOBALS["category_href"]?>">전체</a></li>
<?php if($TPL__category_tab_1){foreach($GLOBALS["category_tab"] as $TPL_V1){?>
<li <?php if($TPL_V1["category"]==$GLOBALS["sca"]){?>class="active"<?php }?>>
<a href="<?php echo $TPL_V1["href"]?>">
<?php if($TPL_V1["category"]==$GLOBALS["sca"]){?><span class="sound_only">열린 분류 </span><?php }?>
<?php echo $TPL_V1["category"]?>
</a>
</li>
<?php }}?>
</ul>
<?php }?>
<div class="tab-content">
<form name="fqalist" id="fqalist" action="./qadelete.php" onsubmit="return fqalist_submit(this);" method="post" class="eyoom-form">
<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
<input type="hidden" name="sca" value="<?php echo $GLOBALS["sca"]?>">
<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
<div class="table-list-eb margin-bottom-20">
<div class="board-list-body">
<table class="table table-hover">
<thead>
<tr>
<th class="hidden-xs">번호</th>
<?php if($GLOBALS["is_checkbox"]){?>
<th>
<label for="chkall" class="sound_only">현재 페이지 게시물 전체</label>
<label class="checkbox">
<input type="checkbox" id="chkall" onclick="if (this.checked) all_checked(true); else all_checked(false);"><i></i>
</label>
</th>
<?php }?>
<th>제목</th>
<th class="hidden-xs">글쓴이</th>
<th class="hidden-xs">상태</th>
<th class="hidden-xs">등록일</th>
</tr>
</thead>
<tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<tr>
<td class="text-center hidden-xs"><?php echo $TPL_V1["num"]?></td>
<?php if($GLOBALS["is_checkbox"]){?>
<td>
<label for="chk_qa_id_<?php echo $TPL_K1?>" class="sound_only"><?php echo $TPL_V1["subject"]?></label>
<label class="checkbox">
<input type="checkbox" name="chk_qa_id[]" value="<?php echo $TPL_V1["qa_id"]?>" id="chk_qa_id_<?php echo $TPL_K1?>"><i></i>
</label>
</td>
<?php }?>
<td class="td-width">
<div class="td-subject ellipsis">
<span class="color-grey">[<?php echo $TPL_V1["category"]?>]</span>&nbsp;
<a href="<?php echo $TPL_V1["view_href"]?>">
<?php echo $TPL_V1["subject"]?>
</a>
<?php if($TPL_V1["icon_file"]){?>
<i class="fa fa-floppy-o margin-left-5 color-grey"></i>
<?php }?>
</div>
</td>
<td class="hidden-xs"><?php echo $TPL_V1["name"]?></td>
<td class="<?php if($TPL_V1["qa_status"]){?>txt_done<?php }else{?>txt_rdy<?php }?> text-center hidden-xs"><?php if($TPL_V1["qa_status"]){?>답변완료<?php }else{?>답변대기<?php }?></td>
<td class="text-center hidden-xs"><?php echo $TPL_V1["date"]?></td>
</tr>
<tr class="td-mobile visible-xs">						        <td colspan="<?php echo $TPL_VAR["colspan"]?>">
<span><i class="fa fa-user"></i> <?php echo $TPL_V1["name"]?></span>
<span><i class="fa fa-clock-o"></i> <?php echo $TPL_V1["date"]?></span>
<span class="<?php if($TPL_V1["qa_status"]){?>txt_done<?php }else{?>txt_rdy<?php }?>"><?php if($TPL_V1["qa_status"]){?>답변완료<?php }else{?>답변대기<?php }?></span>
</td>
</tr>
<?php }}else{?>
<tr><td colspan="<?php echo $TPL_VAR["colspan"]?>" class="text-center">게시물이 없습니다.</td></tr>
<?php }?>
</tbody>
</table>
</div>
</div>
<div class="pull-left">
<?php if($GLOBALS["is_checkbox"]){?>
<input type="submit" name="btn_submit" value="선택삭제" onclick="document.pressed=this.value" class="btn-e btn-e-light-grey">
<?php }?>
</div>
<div class="pull-right">
<?php if($GLOBALS["list_href"]){?><a href="<?php echo $GLOBALS["list_href"]?>" class="btn-e btn-e-default">목록</a><?php }?>
<?php if($GLOBALS["write_href"]){?><a href="<?php echo $GLOBALS["write_href"]?>" class="btn-e btn-e-red margin-left-5">문의등록</a><?php }?>
</div>
<div class="clearfix"></div>
</form>
</div>
</div>
</div>
<?php if($GLOBALS["is_checkbox"]){?>
<noscript>
<p>자바스크립트를 사용하지 않는 경우<br>별도의 확인 절차 없이 바로 선택삭제 처리하므로 주의하시기 바랍니다.</p>
</noscript>
<?php }?>
<!-- 페이지 -->
<?php echo eb_paging('basic')?>
<div class="row margin-bottom-20">
<div class="col-sm-4 col-sm-offset-4">
<form name="fsearch" method="get" class="eyoom-form">
<input type="hidden" name="sca" value="<?php echo $GLOBALS["sca"]?>">
<div class="input-group">
<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
<label class="input">
<input type="text" name="stx" value="<?php echo stripslashes($GLOBALS["stx"])?>" required id="stx" class="form-control" size="15" maxlength="15" placeholder="1:1검색">
</label>
<span class="input-group-btn">
<button class="btn btn-default btn-e-group" type="submit" value="검색">검색</button>
</span>
</div>
</form>
</div>
</div>
<style>
.board-list .eyoom-form .radio i,.board-list .eyoom-form .checkbox i {top:2px}
.board-list .bo_current {color:#ff2a00}
.board-list .txt_done {color:#aaa}
.board-list .txt_rdy {color:#000;font-weight:bold}
.board-list .sch_word {color:#ff2a00}
.board-list .tab-e2 .nav-tabs li.active a {border:1px solid #000;border-top:1px solid #ff2a00}
.board-list .tab-e2 .tab-content {padding:0;border:0}
.table-list-eb .table thead > tr > th {border-bottom:1px solid #000}
.table-list-eb .table tbody > tr > td {padding:8px 5px}
.table-list-eb .table-hover>tbody>tr:hover>td, .table-hover>tbody>tr:hover>th {background:#fafafa}
.table-list-eb thead {border-top:1px solid #000;border-bottom:1px solid #000;background:#fff}
.table-list-eb th {color:#000;font-weight:bold;white-space:nowrap}
.table-list-eb .td-subject {width:300px}
@media (max-width: 1199px) {
.table-list-eb .td-subject {width:260px}
}
@media (max-width: 767px) {
.table-list-eb .td-width {width:inherit}
.table-list-eb .td-subject {width:280px}
}
.table-list-eb .td-mobile td {border-top:1px solid #f0f0f0;padding:4px 5px !important;font-size:10px;color:#999;background:#fafafa}
.table-list-eb .td-mobile td span {margin-right:5px}
</style>
<?php if($GLOBALS["is_checkbox"]){?>
<script>
function all_checked(sw) {
var f = document.fqalist;
for (var i=0; i<f.length; i++) {
if (f.elements[i].name == "chk_qa_id[]")
f.elements[i].checked = sw;
}
}
function fqalist_submit(f) {
var chk_count = 0;
for (var i=0; i<f.length; i++) {
if (f.elements[i].name == "chk_qa_id[]" && f.elements[i].checked)
chk_count++;
}
if (!chk_count) {
alert(document.pressed + "할 게시물을 하나 이상 선택하세요.");
return false;
}
if(document.pressed == "선택삭제") {
if (!confirm("선택한 게시물을 정말 삭제하시겠습니까?\n\n한번 삭제한 자료는 복구할 수 없습니다"))
return false;
}
return true;
}
</script>
<?php }?>