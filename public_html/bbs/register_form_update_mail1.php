<?php
// 회원가입축하 메일 (회원님께 발송)
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>회원가입 축하 메일</title>
</head>

<body>

<!--<div style="margin:30px auto;width:600px;border:10px solid #f7f7f7">-->
<div style="margin:30px auto;width:700px;font-family: MalgunGothic;">
    <div style="border:1px solid #dedede">
        <span style="display:block;padding:10px 30px 30px;text-align:right">
            <a href="<?php echo G5_URL ?>" target="_blank">www.hellomilja.com</a>
        </span>
        <!--<h1 style="padding:30px 30px 0;background:#f7f7f7;color:#555;font-size:1.4em">-->
        <h1 style="padding:30px 30px 0;color:#555;style="font-size:1.4em;"">
            <strong style="font-size:1.8em;">회원가입</strong>을<br> 축하드립니다.
        </h1>
        <span style="display:block;padding:10px 30px 30px;text-align:right">
            <a href="<?php echo G5_URL ?>" target="_blank"><img src ="<?php echo G5_URL ?>/img/mailimg_join.png"> </a>
        </span>
        <p style="line-height: 22px;margin:20px 0 0;padding:30px 30px 50px;min-height:200px;height:auto !important;font-size: 13px;color: rgb(85, 85, 85);height:200px;border-bottom:1px solid #eee">
            <b><?php echo $mb_name ?></b> 님의 회원가입을 진심으로 축하드립니다.<br>
            예쁘고 따뜻한 제품들과 함께 언제나 즐거운 쇼핑이 될 수 있도록 <br>
            항상 최선을 다하는 헬로밀자가 되기 위해 더욱 더 열심히 노력하겠습니다.<br><br>

            방문 하실 때 마다 새롭고 마음에 꼭 드는 소식과 이벤트로<br>
            고객님의 알찬 쇼핑을 도와드리고자 고민하는 모습, 변치 않는 헬로밀자가 되겠습니다.<br><br>

            <?php if ($config['cf_use_email_certify']) { ?>아래의 <strong>메일인증</strong>을 클릭하시면 회원가입이 완료됩니다.<br><?php } ?>
            감사합니다.
        </p><br><br>


        <div style="color: rgb(68, 68, 68); font-size: 15px; font-weight: bold; margin-bottom: 10px;margin-left: 10px;">회원가입정보</div>

        <table class="__se_tbl_ext" style="width: inherit; border-top-color: rgb(181, 181, 181); border-top-width: 1px; border-top-style: solid; border-collapse: collapse;">
            <tbody><tr><td style="height: 43px; color: rgb(136, 136, 136); padding-right: 20px; padding-left: 20px; font-size: 13px; border-bottom-color: rgb(229, 229, 229); border-bottom-width: 1px; border-bottom-style: solid; background-color: rgb(247, 247, 247);">가입아이디</td>
                <td style="width: 80%; height: 43px; color: rgb(51, 51, 51); padding-left: 20px; font-size: 13px; border-bottom-color: rgb(229, 229, 229); border-bottom-width: 1px; border-bottom-style: solid;"><?php echo $mb_id ?></td></tr></tbody>
        </table>

        <div style="margin: 40px 0px 10px; color: rgb(68, 68, 68); font-size: 15px; font-weight: bold;margin-left: 10px;">광고성 정보 수신동의 상태</div>

        <table class="__se_tbl_ext" style="width: inherit; border-top-color: rgb(181, 181, 181); border-top-width: 1px; border-top-style: solid; border-collapse: collapse;">
            <tbody><tr><td style="height: 43px; color: rgb(136, 136, 136); padding-right: 20px; padding-left: 20px; font-size: 13px; border-bottom-color: rgb(229, 229, 229); border-bottom-width: 1px; border-bottom-style: solid; background-color: rgb(247, 247, 247);">SMS</td>

                <td style="width: 80%; height: 43px; color: rgb(51, 51, 51); padding-left: 20px; font-size: 13px; border-bottom-color: rgb(229, 229, 229); border-bottom-width: 1px; border-bottom-style: solid;"><?php if($mb_sms==1) echo "수신동의"; else echo "수신거부"; ?></td>

            <tr><td style="height: 43px; color: rgb(136, 136, 136); padding-right: 20px; padding-left: 20px; font-size: 13px; border-bottom-color: rgb(229, 229, 229); border-bottom-width: 1px; border-bottom-style: solid; background-color: rgb(247, 247, 247);">이메일</td>

                <td style="width: 80%; height: 43px; color: rgb(51, 51, 51); padding-left: 20px; font-size: 13px; border-bottom-color: rgb(229, 229, 229); border-bottom-width: 1px; border-bottom-style: solid;"><?php if($mb_mailling==1) echo "수신동의"; else echo "수신거부"; ?></td></tbody>
        </table>
        <br><br><br>

        <?php if ($config['cf_use_email_certify']) { ?>
        <a href="<?php echo $certify_href ?>" target="_blank" style="display:block;padding:30px 0;background:#484848;color:#fff;text-decoration:none;text-align:center">메일인증</a>
        <?php } else { ?>
        <a href="<?php echo G5_URL ?>" target="_blank" style="display:block;padding:30px 0;background:#484848;color:#fff;text-decoration:none;text-align:center">사이트바로가기</a>
        <?php } ?>
    </div>
</div>

</body>
</html>
