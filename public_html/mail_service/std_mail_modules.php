<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-01-11
 * Time: ���� 1:39
 */

namespace stdmailmodules;


class std_mail_modules
{

    private $mailgun_sending_limit = 10000;

    private $selected_mail_service = '';

    public function addUnsubscribe($selected_service,$type,$mb_id,$mb_md5){

        $unsubscribe_text_exam = "본 메일은 회원님의 수신동의 여부를 확인한 결과 회원님께서 수신 동의를 하셨기에 발송되었습니다.
                                메일 수신을 원치 않으시면, <{코드}>를 클릭하십시오.
                                IF you don't want to receive this mail anymore, click , <{코드}>.
                            ​소재지: 서울특별시 강서구 등촌1동 647-26
                            ​​사업자 등록번호: 306-12-82499
                            TEL: 010-2891-8288
                            Email: bro@brosiidea.com";

        $unsubscribe_html_exam = '<div style="background:#e8e8e8; font-family:Dotum,Arial; padding:10px 15px; text-align:left; font-size:12px; color:#555555; line-height:1.4em;">
                                    <div>본 메일은 회원님의 수신동의 여부를 확인한 결과 회원님께서 수신 동의를 하셨기에 발송되었습니다.<br>
                                    메일 수신을 원치 않으시면,<a href="{코드}">[수신거부]</a>를 클릭하십시오.<br>
                                    IF you do not want to receive this mail anymore, click , <a href="{코드}">unsubscribe</a></div>
                                    <!-- 여백 -->
                                    <div style="clear:both;height:15px;"></div>
                                    <div><strong>소재지: </strong> 서울특별시 강서구 등촌1동 647-26 <strong>​​사업자 등록번호:</strong> 306-12-82499 <br>
                                         <strong>TEL:</strong> 010-2891-8288  <strong>E-Mail</strong> bro@brosiidea.com <br>
                                         Copyright ⓒ brosiidea Communication Inc. All Rights Reserved.
                                    </div>
                                </div>';

        $unsubscribe_string ="";

        if($selected_service=="mailgun"){
            $unsubscribe_string = "%unsubscribe_url%";
        }elseif($selected_service=="sendgrid"){
            $unsubscribe_string = "%sendgrid_unsub_url%";
        }else{
            $unsubscribe_string = G5_BBS_URL."/email_stop.php?mb_id=".$mb_id."&amp;mb_md5=".$mb_md5;
        }

        if ($type == 1) { //type 0 = text, 1 = html
            $tail = preg_replace("/{코드}/", $unsubscribe_string, $unsubscribe_html_exam);
        }else{
            $tail = preg_replace("/{코드}/", $unsubscribe_string, $unsubscribe_text_exam);
        }

        return $tail;

    }

    public function convertTimeForMailgun($date){

        //$delivery_time = "Tue, 9 Jan 2018 23:10:00 +0900";
        $yoil = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
        $month = array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
        $date_info = "";
        $month_info = "";
        $day_info = "";
        $week_info = "";

        if(strtotime($date)>(time()+(3*24*60*60)))
            return "error";

        $temp1 = explode(' ', $date);
        $temp2 = explode('-', $temp1[0]);

        $week_info=$yoil[date('w', strtotime($temp1[0]))];

        if($temp2[2][0]=='0')
            $day_info=$temp2[2][1];
        else
            $day_info=$temp2[2];

        $time=$temp1[1].":00 +0900";

        if($temp2[1][0]=='0')
            $month_info=$month[$temp2[1][1]];
        else
            $month_info=$month[$temp2[1]];

        $date_info = $week_info.", ".$day_info." ".$month_info." ".$temp2[0]." ".$time;


        return $date_info;
    }

    public function convertTimeForSendgrid($date){
        $timestamp = strtotime($date);
        $timelimit = time()+(3*24*60*60);
        if($timestamp>$timelimit)
            return "error";

        return $timestamp;
    }

    public function getMailgunSendingLimit()
    {
        return $this->mailgun_sending_limit;
    }

    public function CheckServiceOptionsState($send_count, $over_send_set, $use_continue_set, $selected_mail_service)
    {
        if ($send_count == $this->mailgun_sending_limit && $over_send_set == 0) {

            return 1;
        }

        if ($send_count == $this->mailgun_sending_limit && $over_send_set == 1 && $use_continue_set == 0 && $selected_mail_service == "mailgun") {

            return 2;
        }

        if ($send_count == $this->mailgun_sending_limit && $over_send_set == 1 && $use_continue_set == 1 && $selected_mail_service == "mailgun"){

            return 3;
        }

        return 0;
    }

    public function getFirstDayinfo(){
        $today = date("Y/m/d");
        $today = explode('/', $today);
        $result =$today[0].'-'.$today[1].'-01';

        return $result;
    }

    public function getLastDayinfo(){
        $today = date("Y/m/d");
        $today = explode('/', $today);

        switch ($today[1]){
            case 1 : $result =$today[0].'-'.$today[1].'-31';
                      return $result;
            case 2 :
                if(($today[0] % 4) == 0 && ($today[0] % 100) !=0 || ($today[0] % 400) == 0) $result =$today[0].'-'.$today[1].'-29';
                else $result =$today[0].'-'.$today[1].'-28';
                return $result;
            case 3 :$result =$today[0].'-'.$today[1].'-31';
                return $result;
            case 4 :$result =$today[0].'-'.$today[1].'-30';
                return $result;
            case 5 :$result =$today[0].'-'.$today[1].'-31';
                return $result;
            case 6 :$result =$today[0].'-'.$today[1].'-30';
                return $result;
            case 7 :$result =$today[0].'-'.$today[1].'-31';
                return $result;
            case 8 :$result =$today[0].'-'.$today[1].'-31';
                return $result;
            case 9 :$result =$today[0].'-'.$today[1].'-30';
                return $result;
            case 10 :$result =$today[0].'-'.$today[1].'-31';
                return $result;
            case 11 :$result =$today[0].'-'.$today[1].'-30';
                return $result;
            case 12 :$result =$today[0].'-'.$today[1].'-31';
                return $result;
        }
    }

}