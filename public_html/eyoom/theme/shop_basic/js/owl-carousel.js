var OwlCarousel = function () {

    return {

        //Owl Carousel
        initOwlCarousel: function () {
		    jQuery(document).ready(function() {
		        //Owl Slider
		        jQuery(document).ready(function() {
		        var owl = jQuery(".owl-slider-shop");
		            owl.owlCarousel({
			            autoPlay : 4000,
		            	items: [4],
		                itemsDesktop : [1200,3],
		                itemsTablet: [600,2],
		                itemsMobile: [479,2],
		                slideSpeed: 400
		            });

		            // Custom Navigation Events
		            jQuery(".next-s").click(function(){
		                owl.trigger('owl.next');
		            })
		            jQuery(".prev-s").click(function(){
		                owl.trigger('owl.prev');
		            })
		        });
		    });

            //Owl Slider Partner
            jQuery(document).ready(function() {
            var owl = jQuery(".owl-slider-partner");
                owl.owlCarousel({
	                autoPlay : 4000,
                    items:4,
                    itemsDesktop : [1000,4],
                    itemsDesktopSmall : [979,3],
                    itemsTablet: [600,2],
                });
            });

		}
    };

}();

/* Main Banner 10 Skin */
$(document).ready(function() {

    var time = 7; // time in seconds

	var $progressBar,
	    $bar,
	    $elem,
	    isPause,
	    tick,
	    percentTime;

	//Init the carousel
	$("#owl-shop").owlCarousel({
		slideSpeed : 500,
		paginationSpeed : 500,
		singleItem : true,
		afterInit : progressBar,
		afterMove : moved,
		startDragging : pauseOnDragging
	});

	//Init progressBar where elem is $("#owl-demo")
	function progressBar(elem){
	$elem = elem;
		//build progress bar elements
		buildProgressBar();
		//start counting
		start();
	}

	//create div#progressBar and div#bar then prepend to $("#owl-demo")
	function buildProgressBar(){
		$progressBar = $("<div>",{
			id:"progressBar"
		});
		$bar = $("<div>",{
			id:"bar"
		});
		$progressBar.append($bar).prependTo($elem);
	}

	function start() {
		//reset timer
		percentTime = 0;
		isPause = false;
		//run interval every 0.01 second
		tick = setInterval(interval, 10);
	};

	function interval() {
		if(isPause === false){
			percentTime += 1 / time;
			$bar.css({
				width: percentTime+"%"
			});
			//if percentTime is equal or greater than 100
			if(percentTime >= 100){
				//slide to next item
				$elem.trigger('owl.next')
			}
		}
	}

	//pause while dragging
	function pauseOnDragging(){
		isPause = true;
	}

	//moved callback
	function moved(){
		//clear interval
		clearTimeout(tick);
		//start again
		start();
	}

	//uncomment this to make pause on mouseover
	// $elem.on('mouseover',function(){
	//   isPause = true;
	// })
	// $elem.on('mouseout',function(){
	//   isPause = false;
	// })

});