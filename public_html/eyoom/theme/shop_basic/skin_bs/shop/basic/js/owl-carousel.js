var OwlCarousel = function () {
    return {
        //Owl Carousel
        initOwlCarousel: function () {
		    jQuery(document).ready(function() {
		        //Owl Slider shop-1 (main.20.skin)
		        jQuery(document).ready(function() {
		        var owl = jQuery(".owl-slider-shop1");
		            owl.owlCarousel({
			            autoPlay : 4000,
		            	items: [3],
		                itemsDesktop : [1000,3],
		                itemsDesktopSmall : [979,2],
		                itemsTablet: [600,1],
		                slideSpeed: 400
		            });
		            // Custom Navigation Events
		            jQuery(".next-s1").click(function(){
		                owl.trigger('owl.next');
		            })
		            jQuery(".prev-s1").click(function(){
		                owl.trigger('owl.prev');
		            })
		        });

		        //Owl Slider shop-2 (main.40.skin)
		        jQuery(document).ready(function() {
		        var owl = jQuery(".owl-slider-shop2");
		            owl.owlCarousel({
			            autoPlay : 4000,
		            	items: [4],
		                itemsDesktop : [1200,3],
		                itemsTablet: [600,2],
		                itemsMobile: [479,2],
		                slideSpeed: 400
		            });
		            // Custom Navigation Events
		            jQuery(".next-s2").click(function(){
		                owl.trigger('owl.next');
		            })
		            jQuery(".prev-s2").click(function(){
		                owl.trigger('owl.prev');
		            })
		        });

		        //Owl Slider shop-3 (relation.10.skin)
		        jQuery(document).ready(function() {
		        var owl = jQuery(".owl-slider-shop3");
		            owl.owlCarousel({
			            autoPlay : 4000,
		            	items: [4],
		                itemsDesktop : [1200,3],
		                itemsTablet: [600,2],
		                itemsMobile: [479,2],
		                slideSpeed: 400
		            });
		            // Custom Navigation Events
		            jQuery(".next-s3").click(function(){
		                owl.trigger('owl.next');
		            })
		            jQuery(".prev-s3").click(function(){
		                owl.trigger('owl.prev');
		            })
		        });
		    });
		}
    };
}();

/* Main Banner 10 Skin */
$(document).ready(function() {

    var time = 7; // time in seconds

	var $progressBar,
	    $bar,
	    $elem,
	    isPause,
	    tick,
	    percentTime;

	//Init the carousel
	$(".owl-mainbanner-10").owlCarousel({
		navigation : true,
		navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		//pagination : true,
		//paginationNumbers: true,
		slideSpeed : 500,
		paginationSpeed : 500,
		singleItem : true,
		afterInit : progressBar,
		afterMove : moved,
		startDragging : pauseOnDragging
	});

	//Init progressBar where elem is $("#owl-demo")
	function progressBar(elem){
	$elem = elem;
		//build progress bar elements
		buildProgressBar();
		//start counting
		start();
	}

	//create div#progressBar and div#bar then prepend to $("#owl-demo")
	function buildProgressBar(){
		$progressBar = $("<div>",{
			id:"progressBar"
		});
		$bar = $("<div>",{
			id:"bar"
		});
		$progressBar.append($bar).prependTo($elem);
	}

	function start() {
		//reset timer
		percentTime = 0;
		isPause = false;
		//run interval every 0.01 second
		tick = setInterval(interval, 10);
	};

	function interval() {
		if(isPause === false){
			percentTime += 1 / time;
			$bar.css({
				width: percentTime+"%"
			});
			//if percentTime is equal or greater than 100
			if(percentTime >= 100){
				//slide to next item
				$elem.trigger('owl.next')
			}
		}
	}

	//pause while dragging
	function pauseOnDragging(){
		isPause = true;
	}

	//moved callback
	function moved(){
		//clear interval
		clearTimeout(tick);
		//start again
		start();
	}

	//uncomment this to make pause on mouseover
	//$elem.on('mouseover',function(){
	//	isPause = true;
	//})
	//$elem.on('mouseout',function(){
	//	isPause = false;
	//})

});