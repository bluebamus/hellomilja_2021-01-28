<?php
	if (!defined('_GNUBOARD_')) exit;
	
	
	define("_INDEX_", true);
	
	if (G5_IS_MOBILE && $eyoom['use_shop_mobile'] == 'y') {
		include_once(EYOOM_MSHOP_PATH.'/index.php');
		return;
	}
	
	// 그누 헤더정보 출력
	@include_once(G5_PATH.'/head.sub.php');
	
	// 팝업창
	@include_once(EYOOM_CORE_PATH.'/newwin/newwin.inc.php');

	// 이윰 헤더 디자인 출력
	@include_once(EYOOM_SHOP_PATH.'/shop.head.php');
	
	// 히트상품
	$hit_goods = new item_list();
	$hit_goods->tpl_name = $tpl_name;
	$hit_goods->theme = $theme;
	$hit_goods->eyoom = $eyoom;
	$hit_goods->set_order_by("it_time desc");
	/*$hit_goods->set_order_by("it_time asc"); 에서 it_time 은 등록시간이며
	it_update_time, it_price 등으로도 가능하며 asc는 오름차순 desc는 내림차순입니다.*/
	$hit_goods->set_list_skin(EYOOM_SHOP_PATH.'/'.$default['de_type1_list_skin']);
	$hit_goods->set_type(1);
	
	// 추천상품
	$recommend_goods = new item_list();
	$recommend_goods->tpl_name = $tpl_name;
	$recommend_goods->theme = $theme;
	$recommend_goods->eyoom = $eyoom;
	$recommend_goods->set_order_by("it_time desc");
	/*$hit_goods->set_order_by("it_time asc"); 에서 it_time 은 등록시간이며
	it_update_time, it_price 등으로도 가능하며 asc는 오름차순 desc는 내림차순입니다.*/
	$recommend_goods->set_list_skin(EYOOM_SHOP_PATH.'/'.$default['de_type2_list_skin']);
	$recommend_goods->set_type(2);
	
	// 최신상품
	$new_goods = new item_list();
	$new_goods->tpl_name = $tpl_name;
	$new_goods->theme = $theme;
	$new_goods->eyoom = $eyoom;
	$new_goods->set_order_by("it_time desc");
	/*$hit_goods->set_order_by("it_update_time asc"); 에서 it_time 은 등록시간이며
	it_update_time, it_price 등으로도 가능하며 asc 는 오름차순 desc 는 내림차순입니다.*/
	$new_goods->set_list_skin(EYOOM_SHOP_PATH.'/'.$default['de_type3_list_skin']);
	$new_goods->set_type(3);
	
	// 인기상품
	$popular_goods = new item_list();
	$popular_goods->tpl_name = $tpl_name;
	$popular_goods->theme = $theme;
	$popular_goods->eyoom = $eyoom;
	$popular_goods->set_order_by("it_time desc");
	/*$hit_goods->set_order_by("it_time asc"); 에서 it_time 은 등록시간이며
	it_update_time, it_price 등으로도 가능하며 asc는 오름차순 desc는 내림차순입니다.*/
	$popular_goods->set_list_skin(EYOOM_SHOP_PATH.'/'.$default['de_type4_list_skin']);
	$popular_goods->set_type(4);
	
	// 할인상품
	$sale_goods = new item_list();
	$sale_goods->tpl_name = $tpl_name;
	$sale_goods->theme = $theme;
	$sale_goods->eyoom = $eyoom;
	$sale_goods->set_order_by("it_time desc");
	/*$hit_goods->set_order_by("it_time asc"); 에서 it_time 은 등록시간이며
	it_update_time, it_price 등으로도 가능하며 asc는 오름차순 desc는 내림차순입니다.*/
	$sale_goods->set_list_skin(EYOOM_SHOP_PATH.'/'.$default['de_type5_list_skin']);
	$sale_goods->set_type(5);

	// 사용자 프로그램
	@include_once(EYOOM_USER_PATH.'/shop/index.php');

	$tpl->assign(array(
		'shop' => $shop,
		'hit_goods' => $hit_goods,
		'recommend_goods' => $recommend_goods,
		'new_goods' => $new_goods,
		'popular_goods' => $popular_goods,
		'sale_goods' => $sale_goods,
	));
	
	// Template define
	$tpl->define_template('shop',$eyoom['shop_skin'],'index.skin.html');
	$tpl->print_($tpl_name);
	
	// 이윰 테일 디자인 출력
	@include_once(EYOOM_SHOP_PATH.'/shop.tail.php');
	
?>