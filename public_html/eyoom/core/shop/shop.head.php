<?php
	if (!defined('_GNUBOARD_')) exit;

	@include_once(EYOOM_SHOP_PATH.'/shop.lib.php');

	if(!defined('_EYOOM_')) @include EYOOM_PATH.'/common.php';

	// PC/모바일 링크 생성
	$href = $thema->get_href($tpl_name);
	
	if($is_member) {
		// 읽지 않은 쪽지
		$memo_not_read = $eb->get_memo($member['mb_id']);

		// 내글 반응
		$respond = $eyoomer['respond'];
		if($respond < 0) {
			$respond = 0;
			sql_query("update {$g5['eyoom_member']} set respond = 0 where mb_id='{$member['mb_id']}'");
		}
	}

	// 메뉴설정
	if($eyoom['use_eyoom_shopmenu'] == 'y') {
		$me_shop = 1;
		$menu = $thema->menu_create('eyoom');
	} else {
		$menu = $shop->menu_create();
	}

	// 서브페이지 타이틀 및 Path 정보
	$subinfo = $thema->subpage_info($menu);

	// 접속자 정보
	$connect = $eb->get_connect();

	//최근 본 상품
	if(defined('_SHOP_')) @include EYOOM_SHOP_PATH.'/boxtodayview.php';

	// 사용자 프로그램
	@include_once(EYOOM_USER_PATH.'/shop/shop.head.php');

	// 템플릿에 변수 할당
	@include EYOOM_INC_PATH.'/tpl.assign.php';

	$tpl->define(array(
		"shop_side" => "skin_bs/shop/".$eyoom['shop_skin']."/side.skin.html",
	));

	// Template define
	$tpl->define_template('shop',$eyoom['shop_skin'],'head.skin.html');

	$tpl->print_($tpl_name);

?>