<?php
$sub_menu = "200300";
include_once('./_common.php');

if (!$config['cf_email_use'])
    alert('환경설정에서 \'메일발송 사용\'에 체크하셔야 메일을 발송할 수 있습니다.');

$selected_mail_service= $_GET['selected_mail_service'];
$send_count= $_GET['cnt'];
$over_send_set_state= $_GET['over_set'];
$use_con_set_state= $_GET['use_con_set'];
$sendgrid_contn_send_set=0;

require_once(G5_PATH.'/mail_service/std_mail_modules.php');
$str_mail_modules= new stdmailmodules\std_mail_modules();

$result = $str_mail_modules->CheckServiceOptionsState($send_count, $over_send_set_state, $use_con_set_state, $selected_mail_service);

switch($result){
    case 1 : alert("mailgun의 무료 전송량을 모두 사용하였습니다.");
        return;
    case 2 : alert("sendgrid의 자동으로 이어서 전송하는 설정을 확인해 주세요.");
        return;
    case 3 : $sendgrid_contn_send_set=1;
        break;
    default : break;
}

auth_check($auth[$sub_menu], 'w');

check_demo();

$g5['title'] = '회원메일 테스트';

$name = get_text($member['mb_name']);
$nick = $member['mb_nick'];
$mb_id = $member['mb_id'];
$email = $member['mb_email'];

$sql = "select ma_subject, ma_content from {$g5['mail_table']} where ma_id = '{$ma_id}' ";
$ma = sql_fetch($sql);

$subject = $ma['ma_subject'];

$content = $ma['ma_content'];
$content = preg_replace("/{이름}/", $name, $content);
$content = preg_replace("/{닉네임}/", $nick, $content);
$content = preg_replace("/{회원아이디}/", $mb_id, $content);
$content = preg_replace("/{이메일}/", $email, $content);

$mb_md5 = md5($member['mb_id'].$member['mb_email'].$member['mb_datetime']);

$content = $content . '<p>더 이상 정보 수신을 원치 않으시면 [<a href="'.G5_BBS_URL.'/email_stop.php?mb_id='.$mb_id.'&amp;mb_md5='.$mb_md5.'" target="_blank">수신거부</a>] 해 주십시오.</p>';

if($selected_mail_service=='mailgun'&&$sendgrid_contn_send_set==0){

    require_once(G5_PATH.'/mail_service/mailgun/Mailgun_send.php');

    //mail service list information
    $sql = " select cf_4 from {$g5['config_table']} ";
    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        $mail_service_info[$i] = $row;
    }

    $api_key=$mail_service_info[0]['cf_4'];
    $domain = "brosiidea.com";
    $from_name = "admin";
    $from = $member['mb_email'];
    $to_name = "admin";
    $to = $member['mb_email'];
    $subject = $subject;
    $text = $content;

    $mailgun = new MailgunMailer();
    $response=$mailgun->mailer($api_key,$domain,$from_name,$from,$to_name,$to,$subject,0,$text,'',$cc='',$bcc='','',$delivery_time='');

    //alert($response);

    if($response=="Queued. Thank you.")
        alert($member['mb_nick'].'('.$member['mb_email'].')님께 테스트 메일을 발송하였습니다. 확인하여 주십시오.');
    else
        alert($member['mb_nick'].'('.$member['mb_email'].')님께 테스트 메일 발송이 실패하였습니다!!! 확인하여 주십시오!');

}elseif($selected_mail_service=='sendgrid'||$sendgrid_contn_send_set==1){

    require_once(G5_PATH.'/mail_service/sendgrid/sendgrid-php/sendgrid-php.php');

    $sql = " select cf_8 from {$g5['config_table']} ";
    $result = sql_query($sql);
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        $mail_service_info[$i] = $row;
    }

    $api_key=trim($mail_service_info[0]['cf_8']);
    $from_name = "admin";
    $from = $member['mb_email'];
    $from = new SendGrid\Email($from_name, $from);
    $to_name = "admin";
    $to = $member['mb_email'];
    $to = new SendGrid\Email($to_name, $to);
    $subject = $subject;
    $content = new SendGrid\Content("text/plain", $content);

    $mail = new SendGrid\Mail($from, $subject, $to, $content);

    $sg = new \SendGrid($api_key);
    $response = $sg->client->mail()->send()->post($mail);

    if($response->statusCode()== "202")
        alert($member['mb_nick'].'('.$member['mb_email'].')님께 테스트 메일을 발송하였습니다. 확인하여 주십시오.');
    else
        alert($member['mb_nick'].'('.$member['mb_email'].')님께 테스트 메일 발송이 실패하였습니다!!! 확인하여 주십시오!');

}else //local_server
{
    include_once(G5_LIB_PATH.'/mailer.lib.php');

    mailer($config['cf_title'], $member['mb_email'], $member['mb_email'], $subject, $content, 1);

    alert($member['mb_nick'].'('.$member['mb_email'].')님께 테스트 메일을 발송하였습니다. 확인하여 주십시오.');

}
?>
