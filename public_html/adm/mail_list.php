<?php
$sub_menu = '200300';
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

//mail service lib include
require_once(G5_PATH.'/mail_service/mailgun/Mailgun.php');
require_once(G5_PATH.'/mail_service/sendgrid/sendgrid-php/sendgrid-php.php');
require_once(G5_PATH.'/mail_service/std_mail_modules.php');



// note : mail service list information
// dev : lim dohyun
// 2018/02/02 update

$sql = " select cf_4,cf_8 from {$g5['config_table']} ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++) {
    $mail_service_info[$i] = $row;
}

//mail sending count checking
$mailgun_api_key=$mail_service_info[0]['cf_4'];
$sendgrid_api_key=$mail_service_info[0]['cf_8'];
$domain=  "hellomilja.com";

//get mailgun sending count
$mailgun = new Mailgun($mailgun_api_key,$domain);

$messages = $mailgun->get("/$domain/stats/total", array(
    'event' => array('accepted', 'delivered', 'failed'),
    'duration' => '1m'
));

//get first and last days info
$str_mail_modules= new stdmailmodules\std_mail_modules();

$first_day =  $str_mail_modules->getFirstDayinfo();
$last_day = $str_mail_modules->getLastDayinfo();

//get sendgrid sending count information from server
$sendgrid = new \SendGrid($sendgrid_api_key);
$sendgrid_query_params = json_decode('{"aggregated_by": "month", "limit": 1, "start_date": "'.$first_day.'", "end_date": "'.$last_day.'", "offset": 1}');
$response = $sendgrid->client->stats()->get(null, $sendgrid_query_params);
$result =json_decode($response->body(), true);

//update sendgrid mailgun sending count information in DB table
$sql = " update {$g5['config_table']} set cf_3 = '{$messages->stats[0]->delivered->total}', cf_7 = '{$result[0]['stats'][0]['metrics']['delivered']}'";
sql_query($sql);

//mail service list information
$sql = " select cf_1,cf_2,cf_3,cf_5,cf_6,cf_7,cf_9,cf_10 from {$g5['config_table']} ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++) {
    $mail_service_info[$i] = $row;
}

//set send count and state to use it in html code
$mailgun_sending_count = $mail_service_info[0]['cf_3'];
$over_send_set_state = $mail_service_info[0]['cf_5'];
$sendgrid_sending_count = $mail_service_info[0]['cf_7'];
$set_use_continue = $mail_service_info[0]['cf_9'];

$sending_error_msg= $_GET['error_msg'];

if($mail_service_info[0]['cf_1']==1)
    $mail_service_state='local_server';
elseif($mail_service_info[0]['cf_2']==1)
    $mail_service_state='mailgun';
else
    $mail_service_state='sendgrid';

// end of mail service code

$sql_common = " from {$g5['mail_table']} ";

// 테이블의 전체 레코드수만 얻음
$sql = " select COUNT(*) as cnt {$sql_common} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$page = 1;

$sql = " select * {$sql_common} order by ma_id desc ";
$result = sql_query($sql);

$g5['title'] = '회원메일발송';
include_once('./admin.head.php');

$colspan = 7;

?>

<div class="local_desc01 local_desc">
    <p>
        <b>테스트</b>는 등록된 최고관리자의 이메일로 테스트 메일을 발송합니다.<br>
        현재 등록된 메일은 총 <?php echo $total_count ?>건입니다.<br>
        <strong>주의) 수신자가 동의하지 않은 대량 메일 발송에는 적합하지 않습니다. 수십건 단위로 발송해 주십시오.</strong>
    </p>
</div>

<div style="margin:0;padding:0 0 10px 21px;">
    <div class="mail_service_info">
        <div>
            [local_server info] <br>
            local_server set status : <?php echo $mail_service_info[0]['cf_1']; ?><br><br>
        </div>
        <div>
            [mailgun info]<br>
            <a href ="https://mailgun.com">mailgun site</a>
            mailgun set status : <?php echo $mail_service_info[0]['cf_2']; ?>,   mailgun sending count : <?php echo $mail_service_info[0]['cf_3']; ?>,   set_use_over_free_cnt : <?php echo $mail_service_info[0]['cf_5']; ?><br><br>
        </div>
        <div>
            [sendgrid info]<br>
            <a href ="https://sendgrid.com">sendgrid site</a>
            sendgrid set status : <?php echo $mail_service_info[0]['cf_6']; ?>,   sendgrid sending count : <?php echo $mail_service_info[0]['cf_7']; ?>,   set_use_continue : <?php echo $mail_service_info[0]['cf_9']; ?><br><br>
        </div>
        <div>
            [마지막 성공적으로 전송된 사용자 id]<br>
            last_sending_success_id : <?php echo $mail_service_info[0]['cf_10']; ?><br><br>
        </div>
        <div>
            [에러 메시지]<br>
            error : <?php echo $sending_error_msg; ?>
        </div>
    </div>

    <div style="margin-top: 20px">
        [메일 서비스 리스트]
        <input type="radio" name="mail_service" value="local_server"  style="margin-left: 10px"> local_server
        <input type="radio" name="mail_service" value="mailgun " style="margin-left: 10px"> mailgun
        <input type="radio" name="mail_service" value="sendgrid" style="margin-left: 10px"> sendgrid
        <input type="button" value="서비스 변경" onclick="mail_service_select();" style="margin-left: 15px ">
    </div>

    <div style="margin-top: 20px">
        [mailgun 무료 전송 수가 넘어도 계속 사용할 것인지 여부]
        <input type="radio" name="set_use_over_free_cnt" value="0"  style="margin-left: 10px"> no
        <input type="radio" name="set_use_over_free_cnt" value="1" style="margin-left: 10px"> yes
        <input type="button" value="확인" onclick="mailgun_free_set();" style="margin-left: 15px ">
    </div>

    <div style="margin-top: 20px">
        [mailgun의 무료 전송수가 넘으면 sendgrid의 서비스로 자동 전환할 것인지 여부 ]
        <input type="radio" name="set_use_continue" value="0"  style="margin-left: 10px"> no
        <input type="radio" name="set_use_continue" value="1 " style="margin-left: 10px"> yes
        <input type="button" value="확인" onclick="sendgrid_auto_use_set();" style="margin-left: 15px ">
    </div>
</div>


<div class="btn_add01 btn_add">
    <a href="./mail_form.php" id="mail_add">메일내용추가</a>
</div>

<form name="fmaillist" id="fmaillist" action="./mail_delete.php" method="post">
<div class="tbl_head01 tbl_wrap">
    <table>
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col"><input type="checkbox" name="chkall" value="1" id="chkall" title="현재 페이지 목록 전체선택" onclick="check_all(this.form)"></th>
        <th scope="col">번호</th>
        <th scope="col">제목</th>
        <th scope="col">작성일시</th>
        <th scope="col">테스트</th>
        <th scope="col">보내기</th>
        <th scope="col">미리보기</th>
    </tr>
    </thead>
    <tbody>
    <?php
    for ($i=0; $row=sql_fetch_array($result); $i++) {
        $s_vie = '<a href="./mail_preview.php?ma_id='.$row['ma_id'].'" target="_blank">미리보기</a>';

        $num = number_format($total_count - ($page - 1) * $config['cf_page_rows'] - $i);

        $bg = 'bg'.($i%2);
    ?>

    <tr class="<?php echo $bg; ?>">
        <td class="td_chk">
            <label for="chk_<?php echo $i; ?>" class="sound_only"><?php echo $row['ma_subject']; ?> 메일</label>
            <input type="checkbox" id="chk_<?php echo $i ?>" name="chk[]" value="<?php echo $row['ma_id'] ?>">
        </td>
        <td class="td_num"><?php echo $num ?></td>
        <td><a href="./mail_form.php?w=u&amp;ma_id=<?php echo $row['ma_id'] ?>"><?php echo $row['ma_subject'] ?></a></td>
        <td class="td_datetime"><?php echo $row['ma_time'] ?></td>
        <td class="td_test"><a href="./mail_test.php?ma_id=<?php echo $row['ma_id'] ?>&selected_mail_service=<?php $mail_service_state ?>&cnt=<?php $mailgun_sending_count ?>&over_set=<?php $over_send_set_state ?>&use_con_set=<?php $set_use_continue ?>">테스트</a></td>
        <td class="td_send"><a href="./mail_select_form.php?ma_id=<?php echo $row['ma_id'] ?>">보내기</a></td>
        <td class="td_mngsmall"><?php echo $s_vie ?></td>
    </tr>

    <?php
    }
    if (!$i)
        echo "<tr><td colspan=\"".$colspan."\" class=\"empty_table\">자료가 없습니다.</td></tr>";
    ?>
    </tbody>
    </table>
</div>

<div class="btn_list01 btn_list">
    <input type="submit" value="선택삭제">
</div>
</form>

<script>

function mail_service_select(){

    var mail_info = document.getElementsByName("mail_service");

    for(var i = 0; i < mail_info.length; i++) {
        if(mail_info[i].checked==true) {
            switch(i){
                case 1: window.location.href="<?php echo G5_URL; ?>/mail_service/update_mail_service.php?set_cond=0&mail_service=mailgun";
                    break;
                case 2: window.location.href="<?php echo G5_URL; ?>/mail_service/update_mail_service.php?set_cond=0&mail_service=sendgrid";
                    break;
                default: window.location.href="<?php echo G5_URL; ?>/mail_service/update_mail_service.php?set_cond=0&mail_service=local_server";
            }
            break;
        }
    }
}

function mailgun_free_set(){

    var mail_info = document.getElementsByName("set_use_over_free_cnt");

    for(var i = 0; i < mail_info.length; i++) {
        if(mail_info[i].checked==true) {
            switch(i){
                case 1: window.location.href="<?php echo G5_URL; ?>/mail_service/update_mail_service.php?set_cond=1&mailgun_free_set=yes";
                    break;
                default: window.location.href="<?php echo G5_URL; ?>/mail_service/update_mail_service.php?set_cond=1&mailgun_free_set=no";
            }
            break;
        }
    }
}

function sendgrid_auto_use_set(){

    var mail_info = document.getElementsByName("set_use_continue");

    for(var i = 0; i < mail_info.length; i++) {
        if(mail_info[i].checked==true) {
            switch(i){
                case 1: window.location.href="<?php echo G5_URL; ?>/mail_service/update_mail_service.php?set_cond=2&set_use_continue=yes";
                    break;
                default: window.location.href="<?php echo G5_URL; ?>/mail_service/update_mail_service.php?set_cond=2&set_use_continue=no";
            }
            break;
        }
    }
}

$(function() {
    $('#fmaillist').submit(function() {
        if(confirm("한번 삭제한 자료는 복구할 방법이 없습니다.\n\n정말 삭제하시겠습니까?")) {
            if (!is_checked("chk[]")) {
                alert("선택삭제 하실 항목을 하나 이상 선택하세요.");
                return false;
            }

            return true;
        } else {
            return false;
        }
    });
});
</script>

<?php
include_once ('./admin.tail.php');
?>